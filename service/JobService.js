import api from "./SecuredAPI";

const JOB_API_URL = "/api/jobs";

class JobService {
  retrieveAllJobs() {
    return api.get(`${JOB_API_URL}/`);
  }

  findMyJobs() {
    return api.get(`${JOB_API_URL}/findMyJobs`);
  }

  deleteJob(id) {
    return api.delete(`${JOB_API_URL}/${id}`);
  }

  getJobById(id) {
    return api.get(`${JOB_API_URL}/${id}`);
  }

  sentToArchiveByUser(id) {
    return api.post(`${JOB_API_URL}/sentToArchiveByUser/${id}`);
  }

  pickCompanyForJobByUser(jobId, companyId) {
    return api.put(`${JOB_API_URL}/pickCompany/${jobId}/${companyId}`);
  }

  setJobPriceByMaster(jobPrice) {
    return api.put(`${JOB_API_URL}/setPriceByCompanyId`, jobPrice);
  }

  updateJob(id, job) {
    return api.put(`${JOB_API_URL}/${id}`, job);
  }

  createJob(job) {
    return api.post(`${JOB_API_URL}`, job);
  }

  getReadableStatus(status) {
    if (status == "NEW_JOB") {
      return "Новая работа";
    }
    if (status == "INDICATE_PRICE") {
      return "В процессе";
    }
    if (status == "COMPLETED") {
      return "Завершена";
    }
  }
}

export default new JobService();
