import api from "./SecuredAPI";
import openAPI from "./UnsecuredAPI";

const HIERARCHY_API_URL = "/api/topCategories/allHierarchy";
const BASE_API_URL = "/api/baseCategories";
const SUBJOBS_API_URL = "/api/tasks/jobTemplates";

class CategoryService {
  getFullHierarchy() {
    return openAPI.get(`${HIERARCHY_API_URL}`);
  }

  getBaseCategories() {
    return api.get(`${BASE_API_URL}`);
  }

  saveBaseCategory(category) {
    return api.post(`${BASE_API_URL}`, category);
  }

  deleteBaseCategoryById(id) {
    return api.delete(`${BASE_API_URL}${id}`);
  }

  getTasksByJobId(id) {
    return api.get(`${SUBJOBS_API_URL}/${id}`);
  }

  getJobWithJobTemplates(id) {
    return api.get(`${BASE_API_URL}/withJobTemplates/${id}`);
  }
}

export default new CategoryService();
