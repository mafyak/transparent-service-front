import api from "./SecuredAPI";

const TICKET_API_URL = "/api/support";

class TicketService {
  retrieveAllTickets() {
    return api.get(`${TICKET_API_URL}/`);
  }

  retrieveMyTickets() {
    return api.get(`${TICKET_API_URL}/myTickets/`);
  }

  deleteTicket(id) {
    return api.delete(`${TICKET_API_URL}/${id}`);
  }

  closeTicket(id) {
    return api.get(`${TICKET_API_URL}/closeTicket/${id}`);
  }

  getTicketById(id) {
    return api.get(`${TICKET_API_URL}/ticket/${id}`);
  }

  updateTicket(ticket) {
    return api.put(`${TICKET_API_URL}`, ticket);
  }

  createTicket(ticket) {
    return api.post(`${TICKET_API_URL}/addTicket`, ticket);
  }

  addComment(id, comment) {
    return api.post(`${TICKET_API_URL}/ticket/${id}/addComment`, comment);
  }
}

export default new TicketService();
