import api from "./SecuredAPI";

const NOTES_API_URL = "/api/adminService/companyNotes";

class CompanyNotesService {
  retrieveAllTickets() {
    // return api.get(`${TICKET_API_URL}/`);
  }

  retrieveMyTickets() {
    // return api.get(`${TICKET_API_URL}/myTickets/`);
  }

  deleteTicket(id) {
    // return api.delete(`${NOTES_API_URL}/${id}`);
  }

  getNotesByCompanyId(id) {
    return api.get(`${NOTES_API_URL}/${id}`);
  }

  updateTicket(ticket) {
    // return api.put(`${TICKET_API_URL}`, ticket);
  }

  createTicket(ticket) {
    // return api.post(`${TICKET_API_URL}/addTicket`, ticket);
  }

  addComment(id, comment) {
    return api.post(`${NOTES_API_URL}/addComment/${id}`, comment);
  }
}

export default new CompanyNotesService();
