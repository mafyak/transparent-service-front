import api from "./SecuredAPI";
import openAPI from "./UnsecuredAPI";

const USER_API_URL = "/api/users";
const USER_API_URL_GET_INFO = "/api/users/userInfo";

class UserService {
  registerUser(user) {
    return api.post(`${USER_API_URL}`, user);
  }

  updateUser(user) {
    return api.put(`${USER_API_URL}`, user);
  }

  getUserInfo() {
    return api.get(USER_API_URL_GET_INFO);
  }

  activateUser(email, code) {
    return api.get(`${USER_API_URL}/activate/${email}/${code}`);
  }

  getUserPublicInfo(id) {
    return api.get(`${USER_API_URL}/publicUserInfo/` + id);
  }

  getUserPublicInfoByName(name) {
    return api.get(`${USER_API_URL}/publicUserInfoByName/` + name);
  }

  updatePassword(request) {
    return api.post(`${USER_API_URL}/updatePassword`, request);
  }

  getAllUsersForAdmin() {
    return api.get(`${USER_API_URL}/publicUserInfo`);
  }

  sendRecoveryEmail(emailAddress) {
    return openAPI.post(`${USER_API_URL}/recoverPassword`, emailAddress);
  }
}

export default new UserService();
