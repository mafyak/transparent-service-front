import api from "./SecuredAPI";

const BANNER_API_URL = "/api/banner";

class BannerService {
  getBanner(id) {
    return api.get(`${BANNER_API_URL}/${id}`);
  }
  getLatest50BannersForAdmin() {
    return api.get(`${BANNER_API_URL}/latest50admin`);
  }
  retrieveBannersForAdmin() {
    // todo all logic here
    return [];
  }

  getBannersForJobTemplateName(jobTemplateName) {
    return api.get(`${BANNER_API_URL}/template/${jobTemplateName}`);
  }

  getSponsorBannersForJobTemplateName(jobTemplateName) {
    return api.get(`${BANNER_API_URL}/sponsor/template/${jobTemplateName}`);
  }

  saveBanner(banner) {
    return api.post(`${BANNER_API_URL}`, banner);
  }

  updateBanner(banner) {
    return api.put(`${BANNER_API_URL}/`, banner);
  }

  deleteBanner(id) {
    return api.delete(`${BANNER_API_URL}/${id}`);
  }
}

export default new BannerService();
