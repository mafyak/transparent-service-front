import api from "./SecuredAPI";
import openAPI from "./UnsecuredAPI";

const ARTICLE_API_URL = "/api/article";
const COMMENT_API_URL = "/api/blogComment";

class ArticleService {

  saveCommentByGuest(article) {
    return openAPI.post(`${COMMENT_API_URL}`, article);
  }

  saveCommentByUser(article) {
    return api.post(`${COMMENT_API_URL}`, article);
  }

  getAll() {
    return openAPI.get(`${ARTICLE_API_URL}`);
  }

  getAllCategories() {
    return openAPI.get(`${ARTICLE_API_URL}/categories`);
  }

  saveArticle(post) {
    return openAPI.post(`${ARTICLE_API_URL}`, post);
  }

  deleteArticleById(id) {
    return api.delete(`${ARTICLE_API_URL}${id}`);
  }

  getArticleByUrl(category, articleName) {
    return openAPI.get(`${ARTICLE_API_URL}/${category}/${articleName}`);
  }

  getArticleById(id) {
    return openAPI.get(`${ARTICLE_API_URL}/id/${id}`);
  }

  getArticleByAuthorId(id) {
    return openAPI.get(`${ARTICLE_API_URL}/author-id/${id}`);
  }

  getArticleByAuthorName(name) {
    return openAPI.get(`${ARTICLE_API_URL}/author-name/${name}`);
  }

  getBlogSidebar() {
    return openAPI.get(`${ARTICLE_API_URL}/sidebar`);
  }

  getBlogSidebarForAdmin() {
    return api.get(`${ARTICLE_API_URL}/admin/sidebar`);
  }

  getArticleMetadata(articleId) {
    return api.get(`${ARTICLE_API_URL}/metadata/${articleId}`)
  }
}

export default new ArticleService();
