import api from "./SecuredAPI";
import openAPI from "./UnsecuredAPI";

const CITY_API_URL = "/api/cities";

class CityService {
  retrieveAllCities() {
    return openAPI.get(`${CITY_API_URL}`);
  }
  deleteCity(id) {
    return api.delete(`${CITY_API_URL}/${id}`);
  }
  addCity(city) {
    return api.post(`${CITY_API_URL}`, city);
  }
}

export default new CityService();
