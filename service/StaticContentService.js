import openAPI from "./UnsecuredAPI";

const STATIC_API_URL = "/api/static";

class StaticContentService {

  getSiteName() {
    return openAPI.get(`${STATIC_API_URL}/site`);
  }
}

export default new StaticContentService();
