import axios from "axios";

const openAPI = axios.create({
  baseURL: process.env.BASE_URL,
});

export default openAPI;
