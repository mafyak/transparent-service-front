class MasterService {
  static checkIfMaster() {
    if (typeof window !== "undefined") {
      var type = localStorage.getItem("type");
      if (type) {
        const bcrypt = require("bcryptjs");
        // @todo change to Async processing.
        return bcrypt.compareSync("MASTER", type);
      }
    } else return false;
  }

  static printSubscription(type) {
    if (type == "ZERO") {
      return "Начальный";
    }
    if (type == "LEVEL_ONE") {
      return "Базовый";
    }
    if (type == "TOP_FIVE") {
      return "Профессионал";
    }
    if (type == "TOP_THREE") {
      return "VIP";
    }
  }
}

export default MasterService;
