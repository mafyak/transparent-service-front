import api from "./SecuredAPI";
import openAPI from "./UnsecuredAPI";

const COMPANY_API_URL = "/api/companies";

class CompanyService {
  retrieveAllCompanies() {
    return openAPI.get(`${COMPANY_API_URL}`);
  }

  retrieveCompaniesForAdmin(page, size, companyName, phone) {
    return api.get(`${COMPANY_API_URL}/getCompanyDetailsForAdmin?page=` + page + `&size=` + size + `&companyName=` + companyName + `&phone=` + phone);
  }

  retrieveFilteredByNameCompanies(page, size, companyName) {
    return openAPI.get(`${COMPANY_API_URL}?page=` + page + `&size=` + size + `&companyName=` + companyName);
  }

  retrieveFilteredPaginableCompanies(page, size, coveredCityId, sortByRating, currentJobTemplateId) {
    return openAPI.get(
      `${COMPANY_API_URL}?page=` +
        page +
        `&size=` +
        size +
        `&coveredCityId=` +
        coveredCityId +
        `&sort=` +
        sortByRating +
        `&jobTemplateId=` +
        currentJobTemplateId +
        `&sort=subscription.type,desc`
    );
  }

  deleteCompany(id) {
    return api.delete(`${COMPANY_API_URL}/${id}`);
  }

  getCompanyById(id) {
    return openAPI.get(`${COMPANY_API_URL}/${id}`);
  }

  getCompaniesBasedOnCategoryAndCity(category, city) {
    return api.get(`${COMPANY_API_URL}/${category}/${city}`);
  }

  getMyCompany() {
    return api.get(`${COMPANY_API_URL}/getMyCompany`);
  }

  updateCompany(company) {
    return api.put(`${COMPANY_API_URL}`, company);
  }

  changeSubscription(subscription) {
    return api.post(`${COMPANY_API_URL}/changeSubscription`, subscription);
  }

  deactivateCompany(id) {
    return api.put(`${COMPANY_API_URL}/deactivateCompany/${id}`);
  }

  activateCompany(id) {
    return api.put(`${COMPANY_API_URL}/activateCompany/${id}`);
  }

  createCompany(company) {
    return api.post(`${COMPANY_API_URL}`, company);
  }
}

export default new CompanyService();
