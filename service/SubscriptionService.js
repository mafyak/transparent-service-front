import api from "./SecuredAPI";
import openAPI from "./UnsecuredAPI";

const SUBSCRIPTION_API_URL = "/api/subscriptionPackage";

class SubscriptionService {
  retrieveAllSubscriptions() {
    return openAPI.get(`${SUBSCRIPTION_API_URL}/findAll`);
  }
  getSubscriptionById(id) {
    return api.get(`${SUBSCRIPTION_API_URL}/${id}`);
  }
  updateSubscription(subscription) {
    return api.put(`${SUBSCRIPTION_API_URL}`, subscription);
  }
  addSubscription(subscription) {
    return api.post(`${SUBSCRIPTION_API_URL}`, subscription);
  }
  deleteSubscriptions(id) {
    return api.delete(`${SUBSCRIPTION_API_URL}/${id}`);
  }
}

export default new SubscriptionService();
