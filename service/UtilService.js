class UtilService {
  static printDate(date) {
    if (date) {
      return new Date(date).toLocaleString("en", {
        year: "numeric",
        month: "long",
        day: "numeric",
      });
    }
  }

  static printRuDate(date) {
    if (date) {
      return new Date(date).toLocaleString("ru", {
        year: "numeric",
        month: "long",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
      });
    }
  }

  static buildArticleUrl(article) {
    if (article.url) {
      return "/article/" + article.url;
    } else {
      return "/blog";
    }
  }
}

export default UtilService;