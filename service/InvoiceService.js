import api from "./SecuredAPI";

const INVOICE_API_URL = "/api/invoices";

class InvoiceService {
  getInvoice(id) {
    return api.get(`${INVOICE_API_URL}/${id}`);
  }

  getMyInvoices() {
    return api.get(`${INVOICE_API_URL}/company`);
  }

  getTop50InvoicesForAdmin() {
    return api.get(`${INVOICE_API_URL}/admin/top50/`);
  }

  retrieveInvoicesForAdmin(companyName, dateBefore, dateAfter) {
    return api.get(`${INVOICE_API_URL}/admin/getInvoices?companyName=` + companyName + `&dateBefore=` + dateBefore + `&dateAfter=` + dateAfter);
  }

  saveInvoice(subscription) {
    return api.post(`${INVOICE_API_URL}`, subscription);
  }

  updateInvoice(invoice) {
    return api.put(`${INVOICE_API_URL}/`, invoice);
  }

  updateInvoiceSignature(id, signature) {
    return api.put(`${INVOICE_API_URL}/signature?id=${id}&signature=${signature}`);
  }

  deleteInvoice(id) {
    return api.delete(`${INVOICE_API_URL}/${id}`);
  }

  downloadInvoice(id) {
    return api.get(`${INVOICE_API_URL}/download/${id}`);
  }
}

export default new InvoiceService();
