import api from "./SecuredAPI";
import openAPI from "./UnsecuredAPI";

const BLOG_API_URL = "/api/blog";
const COMMENT_API_URL = "/api/blogComment";

class BlogService {

  saveCommentByGuess(article) {
    return openAPI.post(`${COMMENT_API_URL}`, article);
  }

  saveCommentByUser(article) {
    return api.post(`${COMMENT_API_URL}`, article);
  }

  getAll() {
    return openAPI.get(`${BLOG_API_URL}`);
  }

  saveArticle(post) {
    return openAPI.post(`${BLOG_API_URL}`, post);
  }

  deleteArticleById(id) {
    return api.delete(`${BLOG_API_URL}${id}`);
  }

  getBlogByCategoryOrTagName(tagOrCategory, tagOrCategoryName) {
    return openAPI.get(`${BLOG_API_URL}/${tagOrCategory}/${tagOrCategoryName}`);
  }

  getBlogByCategoryOrTagNamePageable(tagOrCategory, tagOrCategoryName, page, size) {
    return openAPI.get(`${BLOG_API_URL}/${tagOrCategory}/${tagOrCategoryName}?page=` + page + `&size=` + size);
  }

  getBlogForAdmin() {
    return api.get(`${BLOG_API_URL}/admin`);
  }

  getSitemap() {
    return openAPI.get(`${BLOG_API_URL}/sitemap`);
  }

  getArticleById(id) {
    return openAPI.get(`${BLOG_API_URL}/id/${id}`);
  }

  getBlogSearch(q) {
    return openAPI.get(`${BLOG_API_URL}/search?q=${q}`);
  }

  getRandomArticlesByCount(count) {
    return openAPI.get(`${BLOG_API_URL}/random/${count}`);
  }

  getBlogSidebar() {
    return openAPI.get(`${BLOG_API_URL}/sidebar`);
  }

  getTagCloud(tagOrCategory, tagOrCategoryName) {
    return openAPI.get(`${BLOG_API_URL}/tag-cloud/${tagOrCategory}/${tagOrCategoryName}`);
  }
}

export default new BlogService();
