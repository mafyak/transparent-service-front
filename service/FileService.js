import api from "./SecuredAPI";

const FILES_API_URL = "/api/uploadFiles";

class FileService {
  saveImage(image) {
    return api.post(`${FILES_API_URL}/uploadImage`, image);
  }
  saveImages(images) {
    return api.post(`${FILES_API_URL}/uploadImages`, images);
  }
  updateArticleImage(image, id) {
    return api.post(`${FILES_API_URL}/updateArticleImage/${id}`, image);
  }
}

export default new FileService();
