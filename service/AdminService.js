import api from "./SecuredAPI";

const ADMIN_API_URL = "/api/adminService/reports";
const ADMIN_AI_API_URL = "/api/adminService/ai";
const ADMIN_ARTICLE_URL = "/api/article/admin";
const ADMIN_AI_RECOMMENDATION_API_URL = "/api/ai-recommendation";
const ADMIN_AI_SERVICE_URL = "/api/ai";
const ADMIN_CONFIG_API_URL = "/api/configuration";

class AdminService {
  static checkIfAdmin() {
    if (typeof window !== "undefined") {
      var type = localStorage.getItem("type");
      if (type) {
        const bcrypt = require("bcryptjs");
        // @todo change to Async processing.
        return bcrypt.compareSync("ADMIN", type);
      }
    } else return false;
  }

  static generateReport() {
    return api.get(`${ADMIN_API_URL}/generateCategoryToCityCoverageExcel`);
  }

  static getConfiguration() {
    return api.get(`${ADMIN_CONFIG_API_URL}`);
  }

  static updateConfiguration(config) {
    return api.put(`${ADMIN_CONFIG_API_URL}`, config);
  }

  static approveArticle(article) {
    return api.put(`${ADMIN_ARTICLE_URL}/approve-article`, article);
  }

  static deleteArticle(article) {
    return api.delete(`${ADMIN_ARTICLE_URL}/delete-article/${article.id}`);
  }

  static sendToArticleModeration(article) {
    return api.put(`${ADMIN_ARTICLE_URL}/needs-changes/${article.id}`);
  }

  static getCategoryToImageCount() {
    return api.get(`${ADMIN_ARTICLE_URL}/get-image-breakdown`);
  }

  static addToPinterest(article) {
    return api.post(`${ADMIN_ARTICLE_URL}/add-article-to-pinterest/${article.id}`);
  }

  static savePinterestCode(code) {
    return api.post(`${ADMIN_AI_SERVICE_URL}/pinterest/code/${code}`);
  }

  static generateArticle() {
    return api.post(`${ADMIN_AI_API_URL}/run-generator`);
  }

  static generateSchema() {
    return api.post(`${ADMIN_AI_API_URL}/run-generate-schema`);
  }

  static generateRelatedArticles() {
    return api.post(`${ADMIN_AI_API_URL}/run-generate-related-articles`);
  }

  static addAiArticleRecommendation(payload) {
    return api.post(`${ADMIN_AI_RECOMMENDATION_API_URL}`, payload);
  }

  static getAiArticleRecommendation() {
    return api.get(`${ADMIN_AI_RECOMMENDATION_API_URL}`);
  }

  static deleteRecommendation(id) {
    return api.delete(`${ADMIN_AI_RECOMMENDATION_API_URL}/${id}`);
  }
}

export default AdminService;
