import openAPI from "./UnsecuredAPI";

const AUTH_API_URL = "/oauth/token";

class AuthService {
  getAccessToken() {
    return localStorage.getItem("token");
  }

  getRefreshToken() {
    return localStorage.getItem("refresh_token");
  }

  refreshToken() {
    var token = this.getRefreshToken();
    const config = {
      headers: {
        Authorization: "Basic b3VyVHJ1c3RlZENsaWVudDpjbGllbnRTZWNyZXQ=",
        "Content-Type": "application/x-www-form-urlencoded",
      },
    };
    var payload = {
      grant_type: "refresh_token",
      refresh_token: token,
    };
    const qs = require("querystring");
    var stringPayload = qs.stringify(payload);
    return this.getRefreshTokenValue(stringPayload, config);
  }

  getRefreshTokenValue(stringPayload, config) {
    return openAPI.post(`${AUTH_API_URL}`, stringPayload, config).then((response) => {
      localStorage.setItem("token", response.data.access_token);
      localStorage.setItem("refresh_token", response.data.refresh_token);
      return true;
    });
  }
}

export default new AuthService();
