import api from "./SecuredAPI";

const JOB_TEMPLATE_API_URL = "/api/jobTemplates";
const JOB_TEMPLATE_TASKS_API_URL = "/api/tasks/jobTemplates";

class JobTemplateService {
  getTemplatesByBaseCategoryId(id) {
    return api.get(`${JOB_TEMPLATE_API_URL}/baseCategory/${id}`);
  }

  retrieveAllTemplates() {
    return api.get(`${JOB_TEMPLATE_API_URL}`);
  }

  deleteTemplate(id) {
    return api.delete(`${JOB_TEMPLATE_API_URL}/${id}`);
  }

  getTemplateById(id) {
    return api.get(`${JOB_TEMPLATE_API_URL}/${id}`);
  }

  retrieveTemplateTasks(id) {
    return api.get(`${JOB_TEMPLATE_TASKS_API_URL}/${id}`);
  }

  retrieveTemplateWithTasks(id) {
    return api.get(`${JOB_TEMPLATE_API_URL}/withTasks/${id}`);
  }

  updateTemplate(template) {
    return api.put(`${JOB_TEMPLATE_API_URL}/`, template);
  }

  saveTemplate(template) {
    return api.post(`${JOB_TEMPLATE_API_URL}/`, template);
  }
}

export default new JobTemplateService();
