import api from "./SecuredAPI";

const REVIEW_API_URL = "/api/reviews";

class ReviewService {
  retrieveAllReviews() {
    return api.get(`${REVIEW_API_URL}/`);
  }

  deleteReview(id) {
    return api.delete(`${REVIEW_API_URL}/${id}`);
  }

  getReviewById(id) {
    return api.get(`${REVIEW_API_URL}/${id}`);
  }

  updateReview(review) {
    return api.put(`${REVIEW_API_URL}`, review);
  }

  createReview(review, companyId) {
    return api.post(`${REVIEW_API_URL}/${companyId}`, review);
  }
}

export default new ReviewService();
