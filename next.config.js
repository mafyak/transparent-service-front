const withFonts = require("next-fonts");
const withImages = require('next-images');

module.exports = withFonts(
  withImages({
    images: {
      disableStaticImages: true,
    },
    webpack(config, options) {
      config.module.rules.push({
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
        use: {
          loader: "url-loader",
          options: {
            name: "[name].[ext]",
            limit: 100000,
            esModule: false,
          },
        },
      });
      return config;
    }
  })
);

// const path = require("path");
// const HtmlWebpackPlugin = require("html-webpack-plugin");

// module.exports = {
//   entry: "./src/index.js",
//   output: {
//     path: path.join(__dirname, "/dist"),
//     filename: "index-bundle.js",
//     publicPath: "/",
//   },
//   module: {
//     rules: [
//       { test: /\.js$/, exclude: /node_modules/, use: "babel-loader" },
//       {
//         test: /\.(css|scss)$/,
//         use: ["style-loader", "css-loader", "sass-loader"],
//       },
//       {
//         test: /\.(png|jpe?g|gif)$/,
//         use: [
//           {
//             loader: "file-loader",
//             options: {
//               name: "images/[name].[ext]",
//             },
//           },
//         ],
//       },
//       {
//         test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
//         use: [
//           {
//             loader: "file-loader",
//             options: {
//               name: "[name].[ext]",
//               outputPath: "fonts/",
//             },
//           },
//         ],
//       },
//     ],
//   },
//   devServer: {
//     historyApiFallback: true,
//   },
//   plugins: [
//     new HtmlWebpackPlugin({
//       template: "./src/index.html",
//       favicon: "./src/assets/images/favicon.ico",
//     }),
//   ],
// };
