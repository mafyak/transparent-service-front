import BlogTopSection from "../components/blog/blogTopSection";
import React, {useEffect, useState} from "react";
import BlogMiniArticles from "../components/blog/blogMiniArticles";
import Pagination from "../components/blog/blogPagination";
import {useRouter} from "next/router";
import ArticleService from "../../service/ArticleService";
import BlogService from "../../service/BlogService";
import BlogMiniArticlesSearch from "../components/blog/blogMiniArticlesSearch";

const BlogSearch = () => {

  const router = useRouter();
  const {q} = router.query;
  const [blog, setBlog] = useState(null);
  const [sidebar, setSidebar] = useState(null);
  const [paginator, setPaginator] = useState({
    totalPages: 1,
    first: true,
    last: true,
    number: 1,
    size: 100,
    pageNumber: 1
  });

  useEffect(() => {

    BlogService.getBlogSearch(q).then(res => {
      setBlog(res.data.content);
    });
    ArticleService.getBlogSidebar().then(res => setSidebar(res.data));

  }, [q]);

  return (
    <>

      <BlogTopSection tagOrCategory={q} tagOrCategoryName={null}/>

      <div className="position-relative" key="div1">
        <div className="shape overflow-hidden text-color-white">
          <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"/>
          </svg>
        </div>
      </div>

      <section className="section">
        <div className="container">
          <div className="row">
            <div className="col-lg-8 col-md-6">
              <div className="row">
                <BlogMiniArticlesSearch blog={blog} paginator={paginator} prefixUrl={"/article/"}/>

                {/*<Pagination totalPages={1} size={1} pageNumber={0}*/}
                {/*            onPageChange={page => {*/}
                {/*              setPaginator((prevState) => ({...prevState, pageNumber: page}))*/}
                {/*            }}*/}
                {/*/>*/}
              </div>
            </div>

          </div>
        </div>
      </section>

    </>
  )
}

export default BlogSearch;