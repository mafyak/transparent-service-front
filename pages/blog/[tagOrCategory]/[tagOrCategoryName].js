import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import BlogService from "../../../service/BlogService";
import ArticleService from "../../../service/ArticleService";
import Head from "next/head";
import BlogMiniArticles from "../../components/blog/blogMiniArticles";
import BlogSidebar from "../../components/blog/blogSidebar";
import BlogTopSection from "../../components/blog/blogTopSection";
import Pagination from "../../components/blog/blogPagination";

const Blog = ({blogServer, sidebarServer}) => {

    const router = useRouter();
    const { tagOrCategory, tagOrCategoryName } = router.query;
    const [sidebar, setSidebar] = useState(sidebarServer);
    const [secondaryLoad, setSecondaryLoad] = useState(false);
    const [blog, setBlog] = useState(blogServer.content);
    const [paginator, setPaginator] = useState({
        totalPages: blogServer.totalPages,
        first: blogServer.first,
        last: blogServer.last,
        number: blogServer.number,
        size: blogServer.size,
        pageNumber: 1
      }
    )

    useEffect(() => {

        if (secondaryLoad) {
            ArticleService.getBlogSidebar()
                .then(res => setSidebar(res.data));
        } else {
            setSecondaryLoad(true);
        }
    }, [router]);

    console.log("render");
    return (
        <>
            <Head>
                <title>{"A blog about " + tagOrCategoryName}</title>
                <meta property="og:title" content={"A blog about " + tagOrCategoryName}/>
                <meta name="description" content={"A blog about " + tagOrCategoryName}/>
                <meta property="og:description" content={"A blog about " + tagOrCategoryName}/>
            </Head>
            <BlogTopSection tagOrCategory={tagOrCategory} tagOrCategoryName={tagOrCategoryName}/>

            <div className="position-relative" key="div1">
                <div className="shape overflow-hidden text-color-white">
                    <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"/>
                    </svg>
                </div>
            </div>

            <section className="section">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 col-md-6" >
                            <div className="row">
                                <BlogMiniArticles tagOrCategory={tagOrCategory} tagOrCategoryName={tagOrCategoryName}
                                                  blog={blog} paginator={paginator} prefixUrl={"/article/"} />

                                <Pagination totalPages={paginator.totalPages} size={paginator.size} pageNumber={paginator.pageNumber}
                                            onPageChange={page => {setPaginator((prevState) => ({...prevState, pageNumber: page}))}}
                                />
                            </div>

                            {/* <!-- PAGINATION START --> */}

                            {/*<Pagination totalPages={totalPages} first={first} last={last} number={number} size={size} pageNumber={pageNumber}/>*/}
                            {/*<div className="col-12">*/}
                            {/*    <ul className="pagination justify-content-center mb-0">*/}
                            {/*        <li className="page-item"><Link className="page-link" href="javascript:void(0)" aria-label="Previous">Prev</Link></li>*/}
                            {/*        <li className="page-item active"><Link className="page-link" href="javascript:void(0)">1</Link></li>*/}
                            {/*        <li className="page-item"><Link className="page-link" href="javascript:void(0)">2</Link></li>*/}
                            {/*        <li className="page-item"><Link className="page-link" href="javascript:void(0)">3</Link></li>*/}
                            {/*        <li className="page-item"><Link className="page-link" href="javascript:void(0)" aria-label="Next">Next</Link></li>*/}
                            {/*    </ul>*/}
                            {/*</div>*/}
                            {/* <!-- PAGINATION END --> */}
                        </div>

                        <BlogSidebar sidebar={sidebar} tagOrCategoryName={tagOrCategoryName} tagOrCategory={tagOrCategory} isAdmin={false} blog={blog}/>

                    </div>
                </div>
            </section>
            {/* <!-- Back to top --> */}
            {/* <a href="#" onClick="topFunction()" id="back-to-top" className="back-to-top fs-5"><i data-feather="arrow-up" className="fea icon-sm icons align-middle"></i></a> */}
        </>
    )
}

export default Blog;

export async function getServerSideProps({res, query, req}) {
    if (!req) {
        return {
            blogServer: {},
            sidebarServer: {}
        };
    }

    let blogServer = {};
    let sidebarServer = {};

    try {
        const [blogResponse, sidebarResponse] = await Promise.all([
            BlogService.getBlogByCategoryOrTagNamePageable(query.tagOrCategory, query.tagOrCategoryName, 0, 10),
            ArticleService.getBlogSidebar()
        ]);
        if (blogResponse) {
            blogServer = await blogResponse.data;
        }
        if (sidebarResponse) {
            sidebarServer = await sidebarResponse.data;
        }

    } catch (error) {
        res.setHeader("location", "/404");
        res.statusCode = 302;
        res.end();
    }

    return {
        props: {blogServer, sidebarServer},
    };
}