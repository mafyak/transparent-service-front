import React from "react";
import Link from "next/link";
import ReactGA from "react-ga4";

const SiteTerms = () => {
  return (
    <>
      <div className="bg-half bg-light d-table w-100">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-12 text-center">
              <div className="page-next-level">
                <h4 className="title">Условия сайта </h4>
                <div className="page-next">
                  <nav aria-label="breadcrumb" className="d-inline-block">
                    <ul className="breadcrumb bg-white rounded shadow mb-0">
                      <li className="breadcrumb-item">
                        <Link href="/">
                          <a onClick={() => ReactGA.send({ hitType: "pageview", page: "/", title: "Home" })}>Главная</a>
                        </Link>
                      </li>
                      <li className="breadcrumb-item">
                        <Link href="/register">
                          <a onClick={() => ReactGA.pageview("/register")}>Регистрация</a>
                        </Link>
                      </li>
                      <li className="breadcrumb-item active" aria-current="page">
                        Условия сайта
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="position-relative">
        <div className="shape overflow-hidden text-white">
          <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"/>
          </svg>
        </div>
      </div>
      <section className="section">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-12">
              <div className="card shadow border-0 rounded">
                <div className="card-body">
                  <h1 className="card-title">Пользовательское соглашение об использовании ресурса RMT.by</h1>

                  <p className="text-muted">(далее по тексту — «Соглашение»)</p>
                  <p className="text-muted">Редакция от 01 октября 2020 г.</p>

                  <h3 className="card-title">Термины, используемые в Соглашении:</h3>

                  <p className="text-muted">
                    <b className="text-primary">Компания</b> — ИП Вершина И. Г.
                  </p>
                  <p className="text-muted">
                    <b className="text-primary">Сайт Компании</b> — ресурс (сайт), размещенный в сети Интернет по адресу «https://rmt.by»
                  </p>
                  <p className="text-muted">
                    {" "}
                    <b className="text-primary">Администрация</b> — сотрудники Компании, а также лица, уполномоченные надлежащим образом Компанией на
                    управление Ресурсом и предоставление Услуг Компании Посетителям, в рамках использования Сайта последними.
                  </p>
                  <p className="text-muted">
                    <b className="text-primary">Сервисы</b> — совокупность программ для ЭВМ, баз данных, обеспечивающих функционирование Сайта, а
                    также совокупность Услуг, предоставляемых Пользователям при использовании Сайта.
                  </p>
                  <p className="text-muted">
                    <b className="text-primary">Пользователь</b> — любое физическое лицо, использующее Ресурс.
                  </p>
                  <p className="text-muted">
                    <b className="text-primary">Акцепт оферты</b> — полное и безоговорочное принятие Посетителем Соглашения об использовании Ресурса
                    Rmt.by, путем осуществления действий по использованию Сайта Rmt.by.
                  </p>

                  <h3 className="card-title">Область применения (предмет, принятие, изменение):</h3>
                  <p className="text-muted">
                    Компания предлагает Вам услуги Сайта на условиях, являющихся предметом настоящего Соглашения об использовании Ресурса Rmt.by. В
                    связи с этим Вам необходимо внимательно ознакомиться с условиями настоящего Соглашения, которые рассматриваются Компанией как
                    публичная оферта.
                  </p>

                  <p className="text-muted">
                    С Условиями использования Вам необходимо ознакомиться полностью, поскольку эти положения важны и являются юридическим документом,
                    условия которого будут распространяться на Вас как Пользователя, как только Вы его примете.
                  </p>

                  <p className="text-muted">
                    C даты начала использования сайта Rmt.by Пользователь считается принявшим Условия использования в полном объёме, без всяких
                    оговорок и исключений.
                  </p>

                  <p className="text-muted">
                    В случае несогласия Пользователя с какими-либо из положений Условий использования, Пользователь в праве отказаться от
                    использования сайта Rmt.by.
                  </p>

                  <p className="text-muted">
                    В случае внесения Компанией изменений в Условия использования или если Пользователь начал использовать сайт Компании, но с
                    Условиями не согласен, последний обязан прекратить использование сайта Компании. При этом к отношениям, возникшим в период
                    использования Пользователем сайта Компании, применяются действующие положения Условий использования, если иное не установлено
                    дополнительно. Компания оставляет за собой право пересматривать Условия использования в любое время без предварительного
                    уведомления.
                  </p>

                  <p className="text-muted">
                    Компания оставляет за собой право вносить любые изменения в работу сайта Компании в любое время по собственному усмотрению, без
                    любых (предварительных и последующих) уведомлений Пользователей.
                  </p>

                  <p className="text-muted">
                    Пользователь ресурса соглашается регулярно отслеживать информацию об изменениях Условий использования. Любые изменения в текстах
                    правовых документов вступают в силу немедленно.
                  </p>

                  <h5 className="card-title">Передача прав и обязанностей:</h5>

                  <p className="text-muted">
                    Компания в любое время по собственному усмотрению и без предварительного уведомления Пользователя вправе передать полностью или
                    частично свои права и обязанности, любому третьему лицу.
                  </p>

                  <p className="text-muted">
                    Использование Ресурса Компании означает, что Вы соглашаетесь с тем, что у Компании имеется право, предусмотренное абзацем выше.
                  </p>

                  <p className="text-muted">
                    Компания вправе хранить данные Пользователя и платежей между Компанией и Пользователем в течении 3-х лет.
                  </p>

                  <h5 className="card-title">Ограничение ответственности:</h5>

                  <p className="text-muted">
                    Компания, предоставляя Ресурс, предпринимает все возможные меры для ожидаемого уровня функциональности и поддержки. Однако
                    Компания обращает ваше внимание на действующее ограничение ответственности.
                  </p>

                  <p className="text-muted">
                    Вы соглашаетесь и осознаете, что пользуетесь Ресурсом Компании исключительно на свой риск: это означает, что Ресурс Компании, в
                    том числе материалы и информация, предоставляются «как есть», без гарантий. Компания не предоставляет косвенных, подразумеваемых,
                    обусловленных законодательством или иных гарантий, ручательств, в том числе гарантий относительно возможности коммерческого
                    использования результатов услуг, пригодности их для каких-либо определенных целей и ненарушения чьих-либо имущественных прав и/или
                    прав на результаты интеллектуальной деятельности.
                  </p>

                  <h3 className="card-title">Компания не отвечает:</h3>

                  <ul className="list-unstyled feature-list text-muted">
                    <li>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-arrow-right fea icon-sm mr-2"
                      >
                        <line x1="5" y1="12" x2="19" y2="12"></line>
                        <polyline points="12 5 19 12 12 19"></polyline>
                      </svg>
                      за несоответствие Ресурса Компании истинным целям Пользователя;
                    </li>{" "}
                    <li>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-arrow-right fea icon-sm mr-2"
                      >
                        <line x1="5" y1="12" x2="19" y2="12"></line>
                        <polyline points="12 5 19 12 12 19"></polyline>
                      </svg>
                      за любой ущерб, связанный с использованием Ресурса Компании;
                    </li>{" "}
                    <li>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-arrow-right fea icon-sm mr-2"
                      >
                        <line x1="5" y1="12" x2="19" y2="12"></line>
                        <polyline points="12 5 19 12 12 19"></polyline>
                      </svg>
                      за достоверность предоставленной информации и документов третьими лицами.
                    </li>{" "}
                    <li>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-arrow-right fea icon-sm mr-2"
                      >
                        <line x1="5" y1="12" x2="19" y2="12"></line>
                        <polyline points="12 5 19 12 12 19"></polyline>
                      </svg>
                      за действия других Пользователей Ресурса Компании, в том числе в случае несоблюдения Пользователями Компании настоящих Условий
                      использования;
                    </li>{" "}
                    <li>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-arrow-right fea icon-sm mr-2"
                      >
                        <line x1="5" y1="12" x2="19" y2="12"></line>
                        <polyline points="12 5 19 12 12 19"></polyline>
                      </svg>
                      за качество оказываемой услуги третьими лицами;
                    </li>{" "}
                    <li>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-arrow-right fea icon-sm mr-2"
                      >
                        <line x1="5" y1="12" x2="19" y2="12"></line>
                        <polyline points="12 5 19 12 12 19"></polyline>
                      </svg>
                      за не предоставление услуг, информации или имущества от иных Пользователей Компании и/или третьих лиц.
                    </li>
                  </ul>

                  <h3 className="card-title">Пользователь обязуется:</h3>

                  <ul className="list-unstyled feature-list text-muted">
                    <li>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-arrow-right fea icon-sm mr-2"
                      >
                        <line x1="5" y1="12" x2="19" y2="12"></line>
                        <polyline points="12 5 19 12 12 19"></polyline>
                      </svg>
                      потребовать от третьих лиц составление договора об оказании услуг, а также ознакомиться с документами, подтверждающими
                      законность осуществление данных услуг.
                    </li>{" "}
                    <li>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-arrow-right fea icon-sm mr-2"
                      >
                        <line x1="5" y1="12" x2="19" y2="12"></line>
                        <polyline points="12 5 19 12 12 19"></polyline>
                      </svg>
                      не размещать материалы, содержащих информацию, распространение которой запрещено законодательством Республики Беларусь (в
                      соответствии с постановлением Совета Министров Республики Беларусь (от 23.11.2018 N 850).
                    </li>
                  </ul>

                  <p className="text-muted">
                    Компания не обещает и не гарантирует, что информация, полученная посредством использования Ресурса, будет во всех случаях
                    правильной, новейшей или полной. Ресурс может содержать технические неточности и типографские опечатки.
                  </p>

                  <p className="text-muted">
                    Компания не дает никаких заверений или гарантий относительно сроков устранения каких-либо проблем и/или их последствий, отмеченных
                    Пользователями.
                  </p>

                  <h3 className="card-title">Использование материалов Компании:</h3>

                  <p className="text-muted">
                    Запрещается копирование, воспроизведение, переиздание, выгрузка, размещение, передача, распространение любой информации,
                    находящегося на сайте компании, а также их использование для создания производных Ресурсов без получения предварительного
                    письменного согласия от Компании.
                  </p>

                  <p className="text-muted">
                    Все ссылки на сайт Компании должны быть утверждены Компанией. В качестве условия получения разрешения на создание ссылок на данный
                    сайт вы должны согласиться с тем, что Компания может в любой момент исключительно по своему усмотрению аннулировать разрешение на
                    создание ссылок на данный веб-сайт. При подобных обстоятельствах вы обязуетесь незамедлительно удалить все ссылки на данный сайт и
                    прекратить использование любых товарных знаков Компании.
                  </p>

                  <h3 className="card-title">Конфиденциальная информация:</h3>

                  <p className="text-muted">
                    Вы осознаете, подтверждаете и соглашаетесь с тем, что техническая обработка и передача информации Ресурсом Компании, в том числе
                    предоставленной Вами информации, может включать в себя передачу данных по различным сетям, в том числе по незашифрованным каналам
                    связи сети Интернет, которая никогда не является полностью конфиденциальной и безопасной; соглашаетесь с такой передачей в рамках
                    сотрудничества с Компанией. Вы также понимаете, что любое сообщение и/ или информация, отправленные посредством Ресурса Компании,
                    могут быть несанкционированно прочитаны и/или перехвачены третьими лицами.
                  </p>

                  <h4 className="card-title">Какие данные собираются:</h4>

                  <p className="text-muted">Используя Ресурс Компании, вы осознаете и соглашаетесь с тем, что:</p>

                  <ul className="list-unstyled feature-list text-muted">
                    <li>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-arrow-right fea icon-sm mr-2"
                      >
                        <line x1="5" y1="12" x2="19" y2="12"></line>
                        <polyline points="12 5 19 12 12 19"></polyline>
                      </svg>
                      Компания собирает автоматически определённые сведения о Пользователях, не ограничиваясь например, сведениями о Вашем компьютере;
                    </li>{" "}
                    <li>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-arrow-right fea icon-sm mr-2"
                      >
                        <line x1="5" y1="12" x2="19" y2="12"></line>
                        <polyline points="12 5 19 12 12 19"></polyline>
                      </svg>
                      Компания имеет право размещать файлы cookie на вашем компьютере с целью сбора таких сведений.
                    </li>
                  </ul>

                  <h4 className="card-title">В каких случаях Компания разглашает данные о Пользователях третьим лицам?</h4>

                  <p className="text-muted">
                    Используя Ресурс Компании, вы осознаете и соглашаетесь с тем, что Компания вправе разглашать Ваши данные третьим лицам в случаях:
                  </p>

                  <ul className="list-unstyled feature-list text-muted">
                    <li>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-arrow-right fea icon-sm mr-2"
                      >
                        <line x1="5" y1="12" x2="19" y2="12"></line>
                        <polyline points="12 5 19 12 12 19"></polyline>
                      </svg>
                      законных требований и запросов правоохранительных и/или судебных органов, и/или иных государственных органов в соответствии с
                      применимым к сторонам законодательством;
                    </li>{" "}
                    <li>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-arrow-right fea icon-sm mr-2"
                      >
                        <line x1="5" y1="12" x2="19" y2="12"></line>
                        <polyline points="12 5 19 12 12 19"></polyline>
                      </svg>
                      если предоставляемые сведения могут иметь отношение к предполагаемой/возможной подготовке к совершению преступления /
                      совершенному преступлению / иному правонарушению или подготовке к гражданскому процессу / в ходе гражданского процесса и
                      запрашиваются в соответствии с обычными процедурами, используемыми применимым законодательством и/или установленными Компанией;
                    </li>
                    <li>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-arrow-right fea icon-sm mr-2"
                      >
                        <line x1="5" y1="12" x2="19" y2="12"></line>
                        <polyline points="12 5 19 12 12 19"></polyline>
                      </svg>
                      если требуется защита интересов Компании в случае угрозы нарушения прав и законных интересов, и/ или фактического нарушения прав
                      и законных интересов Компании, и/или Пользователей, и/или подрядчиков Компании.
                    </li>
                  </ul>

                  <p className="text-muted">
                    Обращаем ваше внимание на то, что Компания может быть обязана предоставлять запрашиваемую информацию и в иных случаях, не
                    оговоренных в настоящем документе, но полномочным в рамках применимого к сторонам законодательства.
                  </p>

                  <h4 className="card-title">Резервное копирование, неполадки в работе Ресурса:</h4>

                  <p className="text-muted">
                    Используя Ресурс Компании, Вы осознаете и соглашаетесь с тем, что Ресурс может быть подвержен сбоям и простоям, вследствие которых
                    Пользователям могут быть нанесены убытки, Компания не несет ответственности за какие-либо сбои и последующую потерю данных и иные
                    последствия в случае сбоев и простоев Ресурса Компании.
                  </p>

                  <p className="text-muted">
                    Компания предпринимает все возможные меры по недопущению сбоев и простоев Ресурса, тем не менее Вам следует регулярно создавать
                    резервные копии содержимого, хранящегося на сайте, которые, возможно, позволят предотвратить потерю содержимого.
                  </p>

                  <h4 className="card-title">Ссылки на Сайты третьих лиц:</h4>

                  <p className="text-muted">
                    На Сайте Компании могут содержаться ссылки на сайты, которые не имеют отношения к Компании. Компания не делает никаких заявлений,
                    не дает никаких гарантий и не берет на себя никаких иных обязательств какого бы то ни было рода относительно сайтов других
                    компаний (не Компании) или ресурсов, принадлежащих третьим сторонам.
                  </p>

                  <p className="text-muted">
                    Ссылка на сайты другой компании (не Компании) не означает, что Компания одобряет содержимое или рекомендует использование этих
                    сайтов, либо поддерживает его владельца. Кроме того, Компания не будет являться стороной никаких сделок и не будет отвечать ни за
                    какие сделки, которые Вы можете заключить с третьими сторонами, даже если вы узнали о таких сторонах (или воспользовались ссылкой
                    на такие стороны) на сайте Компании. Соответственно, Вы признаете и согласны с тем, что Компания не отвечает за доступность таких
                    внешних сайтов или ресурсов и не несет никаких обязательств ни за какой контент, услуги, Продукты и иные материалы, которые
                    находятся на таких сайтах или в составе таких ресурсов, или которые можно оттуда получить.
                  </p>

                  <p className="text-muted">
                    Перейдя на сайты третьих лиц (не Компании), даже если на них содержится логотип Компании, пожалуйста, учитывайте, что эти сайты
                    существуют независимо от Компании и Компания никак не контролирует содержимое таких сайтов.
                  </p>

                  <p className="text-muted">
                    Вы сами должны принимать меры предосторожности, которые Вы считаете необходимыми, чтобы обеспечить защиту вашей информации от
                    элементов деструктивного характера.
                  </p>

                  <h3 className="card-title">Заключительные положения:</h3>

                  <p className="text-muted">
                    Недействительность отдельных условий настоящих Условий использования, применимых к правоотношениям сторон, не влияет на
                    действительность других условий правовых документов в целом.
                  </p>

                  <p className="text-muted">
                    Если какое-либо условие настоящих Условий использования, применимых к правоотношениям сторон, или их часть становятся
                    недействительными полностью или частично в соответствии с каким-либо нормативным актом или положением закона, такое условие или их
                    часть не будут считаться частью настоящих Условий использования, а также иных правовых документов, применимых к правоотношениям
                    сторон, и при этом не затронут юридической силы остальной части правовых документов, применимых к правоотношениям сторон.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default SiteTerms;
