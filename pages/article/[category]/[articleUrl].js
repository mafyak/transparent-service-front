import ArticleService from "../../../service/ArticleService";
import DefaultPic from "../../../assets/images/company.jpg";
import React, {useState, useEffect} from "react";
import {useRouter} from "next/router";
import Link from "next/link";
import ReactGA from "react-ga4";
import {Search} from "react-feather";
import bcrypt from "bcryptjs";
import {withAlert} from "react-alert";
import Head from 'next/head';
import RelatedArticlesBlock from "../../components/blog/relatedArticlesBlock";
import Tags from "../../components/blog/tags";
import PageHead from "../../components/blog/pageHead";
import ParagraphsContent from "../../components/blog/paragraphsContent";
import Sidebar from "../../components/blog/sidebar";
import {UtilService} from "../../../service";
import Image from "next/image";

const Article = ({alert, articleServer, sidebarServer}) => {
  const router = useRouter();
  const {category, articleUrl} = router.query;
  const [article, setArticle] = useState(articleServer);
  const [sidebar, setSidebar] = useState(sidebarServer);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [commentText, setCommentText] = useState([]);
  const [commentName, setCommentName] = useState([]);

  useEffect(() => {
    setIsLoggedIn(localStorage.getItem("logged in"));
    ArticleService.getArticleByUrl(category, articleUrl)
    .then(res => setArticle(res.data));
    ArticleService.getBlogSidebar()
    .then(res => setSidebar(res.data));
  }, [articleUrl]);

  console.log("render");
  if (article) {
    return (
      <>
        <Head>
          <title>{article.topic}</title>
          <meta name="description" content={article.topic}/>
          <meta property="og:description" content={article.topic}/>
          <meta property="og:title" content={article.topic}/>
          <meta property="og:url" content={"/article/" + article.url}/>
          <meta property="og:image" content={article.topicImage} />
          <meta property="og:type" content="article" />
          {article.schema &&
            <script
              type="application/ld+json"
              dangerouslySetInnerHTML={{__html: article.schema}}
              key="product-jsonld"
            />}
        </Head>

        <PageHead article={article} category={category}/>

        <section className="section section-override">
          <div className="container">
            <div className="row">
              <div className="col-lg-8 col-md-6">
                <div className="card blog blog-detail border-0 shadow rounded">
                  <PinterestButton isLoggedIn={isLoggedIn}/>
                  <img src={article.topicImage} className="img-fluid rounded-top" alt={article.topic}/>
                  <div className="card-body content">
                    <Tags tags={article.tags}/>
                    <h1 className="back-end-text">{article.topic}</h1>
                    <ParagraphsContent paragraphs={article.content}/>
                    {/* <LikesAndCommentsCount article={article} /> */}
                  </div>
                </div>

                <RelatedArticlesBlock relatedArticles={article.relatedArticles}/>

                {/* <CommentsSection comments={article.comments} commentText={commentText} commentName={commentName} /> */}

              </div>
              <Sidebar sidebar={sidebar} article={article}/>
            </div>
          </div>
        </section>
      </>
    );
  } else {
    return <></>
  }
}

const PinterestButton = ({isLoggedIn}) => {
  if (isLoggedIn) {
    let userType = localStorage.getItem("type");
    const bcrypt = require("bcryptjs");
    if (bcrypt.compareSync("PINTEREST", userType)) {
      return (
        <div className="pointer-cursor">
          <a onClick={() => console.log("add to Pinterest")}>
            <Image src="/img/pinterest-icon.jpg" alt="pinterest" width={42} height={42}/>
          </a>
        </div>
      )
    }
  }
  return (<></>)
}

// const LikesAndCommentsCount = ({article}) => {
//   return (
//     <div className="post-meta mt-3">
//       <ul className="list-unstyled mb-0">
//         <li className="list-inline-item mr-2">
//           <a href="javascript:void(0)" className="text-muted like">
//             <i className="mdi mdi-heart-outline mr-1"></i>
//             {article.likes}
//           </a>
//         </li>
//         <li className="list-inline-item">
//           <a href="#comments" className="text-muted comments">
//             <i className="mdi mdi-comment-outline mr-1"></i>
//             {article.comments ? article.comments.length : 0}
//           </a>
//         </li>
//       </ul>
//     </div>
//   )
// }

// const SearchSection = () => {
//   return (
//     <div className="widget mb-4 pb-2">
//       <h4 className="widget-title">Search</h4>
//       <div id="search2" className="widget-search mt-4 mb-0">
//         <form role="search" method="get" id="searchform" className="searchform">
//           <div>
//             <input type="text" className="border rounded" name="s" id="s" placeholder="Enter your request..."/>
//             <input type="submit" id="searchsubmit" value="Search"/>
//           </div>
//         </form>
//       </div>
//     </div>
//   )
// }

// const Comments = ({comments}) => {
//   if (comments) {
//     return comments.map((comment) => (
//       <li className="mt-4">
//         <div className="d-flex justify-content-between">
//           <div className="media align-items-center">
//
//             <Link href={"/user/" + comment.user.id}>
//               <a onClick={() => {
//                 ReactGA.send({ hitType: "pageview", page: "/user/" + comment.user.id, title: "User page: " + comment.user.fullName });
//               }} className="pr-3">
//                 <img src={DefaultPic} className="img-fluid avatar avatar-md-sm rounded-circle shadow" alt="img"/>
//               </a>
//             </Link>
//             <div className="commentor-detail">
//               <h6 className="mb-0">
//                 <Link href={"/user/" + comment.user.id}>
//                   <a onClick={() => {
//                     ReactGA.send({ hitType: "pageview", page: "/user/" + comment.user.id, title: "User page: " + comment.user.fullName });
//                   }} className="text-dark media-heading">
//                     {comment.user.fullName}
//                   </a>
//                 </Link>
//               </h6>
//               <small className="text-muted">{printDate(comment.creationDate)}</small>
//             </div>
//           </div>
//           <Link href="#" className="text-muted">
//             <i className="mdi mdi-reply"></i> Reply
//           </Link>
//         </div>
//         <div className="mt-3">
//           <p className="text-muted font-italic p-3 bg-light rounded">" {comment.content} "</p>
//         </div>
//       </li>
//     ));
//   }
// }

// const CommentsSection = ({comments}) => {
//   return (
//     <>
//       <div className="card shadow rounded border-0 mt-4" id="comments">
//         <div className="card-body">
//           <h5 className="card-title mb-0">Comments :</h5>
//           <ul className="media-list list-unstyled mb-0">
//             <Comments comments={comments}/>
//           </ul>
//         </div>
//       </div>
//       <div className="card shadow rounded border-0 mt-4">
//         <div className="card-body">
//           <h5 className="card-title mb-0">Leave a comment :</h5>
//
//           <form className="mt-3">
//             <div className="row">
//               <div className="col-md-12">
//                 <div className="form-group position-relative">
//                   <label>Your comment</label>
//                   <i data-feather="message-circle" className="fea icon-sm icons"></i>
//                   <textarea
//                     id="message"
//                     placeholder="Leave your comment here..."
//                     rows="5"
//                     value={commentText}
//                     onChange={handleCommentText}
//                     name="message"
//                     className="form-control pl-5"
//                     required=""
//                   ></textarea>
//                 </div>
//               </div>
//
//               <div className="col-lg-6">
//                 <div className="form-group position-relative">
//                   <input
//                     id="name"
//                     name="name"
//                     type="text"
//                     placeholder="Your name"
//                     value={commentName}
//                     onChange={handleCommentName}
//                     className="form-control pl-5"
//                     required=""
//                   />
//                 </div>
//               </div>
//               <div className="col-lg-6">
//                 <div className="form-group position-relative">
//                   <div className="send">
//                     <button type="submit" onClick={addComment} className="btn btn-primary btn-block">
//                       Leave a comment
//                     </button>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </form>
//         </div>
//       </div>
//     </>
//   );
// }

// function printFollowBlock() {
//     return <div className="widget">
//     <h4 className="widget-title">Follow us ;)</h4>
//     <ul className="list-unstyled social-icon mb-0 mt-4">
//       <li className="list-inline-item">
//         <a href="javascript:void(0)" className="rounded">
//           <i data-feather="facebook" className="fea icon-sm fea-social"></i>
//         </a>
//       </li>
//       <li className="list-inline-item">
//         <a href="javascript:void(0)" className="rounded">
//           <i data-feather="instagram" className="fea icon-sm fea-social"></i>
//         </a>
//       </li>
//       <li className="list-inline-item">
//         <a href="javascript:void(0)" className="rounded">
//           <i data-feather="twitter" className="fea icon-sm fea-social"></i>
//         </a>
//       </li>
//       <li className="list-inline-item">
//         <a href="javascript:void(0)" className="rounded">
//           <i data-feather="linkedin" className="fea icon-sm fea-social"></i>
//         </a>
//       </li>
//       <li className="list-inline-item">
//         <a href="javascript:void(0)" className="rounded">
//           <i data-feather="github" className="fea icon-sm fea-social"></i>
//         </a>
//       </li>
//       <li className="list-inline-item">
//         <a href="javascript:void(0)" className="rounded">
//           <i data-feather="youtube" className="fea icon-sm fea-social"></i>
//         </a>
//       </li>
//       <li className="list-inline-item">
//         <a href="javascript:void(0)" className="rounded">
//           <i data-feather="gitlab" className="fea icon-sm fea-social"></i>
//         </a>
//       </li>
//     </ul>
//   </div>
// }

// function printAuthorName(author) {
//   return (
//     <li className="list-inline-item h6 user text-muted mr-2">
//       <i className="mdi mdi-account"></i> {author ? author.fullName : " "}
//     </li>
//   );
// }

// function handleCommentText({target}) {
//   setCommentText(target.value);
// }
//
// function handleCommentName({target}) {
//   setCommentName(target.value);
// }
//
// function addComment(e) {
//   e.preventDefault();
//   var blogComment = {};
//   blogComment.content = commentText;
//   var article = {};
//   article.id = article.id;
//   blogComment.article = article;
//   if (!localStorage.getItem("logged in")) {
//     blogComment.guessName = commentName;
//     ArticleService.saveCommentByGuest(blogComment).then(({data}) => setArticle(data));
//   } else {
//     ArticleService.saveCommentByUser(blogComment).then(({data}) => setArticle(data));
//   }
//   setCommentText("");
//   setCommentName("");
// }

export default withAlert()(Article);

export async function getServerSideProps({res, query, req}) {
  if (!req) {
    return {
      articleServer: null,
      sidebarServer: null
    };
  }

  let articleServer = {};
  let sidebarServer = {};

  try {
    const [articleResponse, sidebarResponse] = await Promise.all([
      ArticleService.getArticleByUrl(query.category, query.articleUrl),
      ArticleService.getBlogSidebar()
    ]);
    if (articleResponse) {
      articleServer = await articleResponse.data;
    }
    if (sidebarResponse) {
      sidebarServer = await sidebarResponse.data;
    }

  } catch (error) {
    res.setHeader("location", "/404");
    res.statusCode = 302;
    res.end();
  }

  return {
    props: {articleServer, sidebarServer},
  };
}