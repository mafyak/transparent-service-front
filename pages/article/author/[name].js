import React, { useState, useEffect } from "react";
import { UserService, ArticleService, AdminService } from "@services";
import { useRouter } from "next/router";
import Link from "next/link";
import ReactGA from "react-ga4";
import Head from "next/head";

const AuthorPage = ({authorServer, articlesServer}) => {
    const router = useRouter();
    const { name } = router.query;
    const [author, setAuthor] = useState(authorServer);
    const [articles, setArticles] = useState(articlesServer);

    useEffect(() => {
        async function loadAuthor() {
            if (name) {
                UserService.getUserPublicInfoByName(name)
                    .then(({ data }) => {
                        setAuthor(data);
                    })
                    .then(ArticleService.getArticleByAuthorName(name)
                        .then(({ data }) => {
                            setArticles(data);
                        }));
            }
        }

        if (!author || !author.fullName) {
            loadAuthor();
        }
    }, [router]);

    return (<>
        <Head>
            <title>{"Page of our blog author " + author.fullName}</title>
            <meta name="description" content={"Page of our blog author " + author.fullName}/>
            <meta property="og:description" content={"Page of our blog author " + author.fullName}/>
            <meta property="og:title" content={"Page of our blog author " + author.fullName}/>
        </Head>
        <section className="bg-profile d-table w-100 bg-primary">
            {/* <section className="bg-profile d-table w-100 bg-primary" style={{background: url('assets/images/account/bg.png') center center}}> */}
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card public-profile border-0 rounded shadow">
                            {/* <div className="card public-profile border-0 rounded shadow" style={{z-index: 1;}}> */}
                            <div className="card-body">
                                <div className="row align-items-center">
                                    <div className="col-lg-2 col-md-3 text-md-start text-center">
                                        <img src={author.avatar} className="avatar avatar-large rounded-circle shadow d-block mx-auto" alt="" />
                                    </div>

                                    <div className="col-lg-10 col-md-9">
                                        <div className="row align-items-end">
                                            <div className="col-md-7 text-md-start text-center mt-4 mt-sm-0">
                                                <h3 className="title mb-0">{author.fullName}</h3>
                                                <small className="text-muted h6 me-2">Blogger</small>
                                                <ul className="list-inline mb-0 mt-3">
                                                    {/* <li className="list-inline-item me-2"><a href="javascript:void(0)" className="text-muted" title="Instagram"><i data-feather="instagram" className="fea icon-sm me-2"></i>krista_joseph</a></li> */}
                                                    {/* <li className="list-inline-item"><a href="javascript:void(0)" className="text-muted" title="Linkedin"><i data-feather="linkedin" className="fea icon-sm me-2"></i>Krista Joseph</a></li> */}
                                                </ul>
                                            </div>
                                            {/* <div className="col-md-5 text-md-end text-center">
                                                <ul className="list-unstyled social-icon social mb-0 mt-4">
                                                    <li className="list-inline-item"><a href="javascript:void(0)" className="rounded" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Add Friend"><i className="uil uil-user-plus align-middle"></i></a></li>
                                                    <li className="list-inline-item"><a href="javascript:void(0)" className="rounded" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Messages"><i className="uil uil-comment align-middle"></i></a></li>
                                                    <li className="list-inline-item"><a href="javascript:void(0)" className="rounded" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Notifications"><i className="uil uil-bell align-middle"></i></a></li>
                                                    <li className="list-inline-item"><a href="account-setting.html" className="rounded" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Settings"><i className="uil uil-cog align-middle"></i></a></li>
                                                </ul>
                                            </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section className="section mt-60">
            <div className="container mt-lg-3">
                <div className="row">
                    <div className="col-lg-4 col-md-6 col-12 d-lg-block d-none">
                        <div className="sidebar sticky-bar p-4 rounded shadow">
                            <div className="widget">
                                <h5 className="widget-title">About author</h5>
                                <div className="row mt-4">
                                    <div className="d-flex align-items-center mt-3">
                                        <i data-feather="bookmark" className="fea icon-ex-md text-muted me-3"/>
                                        <div className="flex-1">
                                            <h6 className="text-primary mb-0 medium-font">Most popular topics</h6>
                                            <div className="flexing-wrapping">
                                                <AuthorCategories articles={articles}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="d-flex align-items-center mt-3">
                                        <i data-feather="italic" className="fea icon-ex-md text-muted me-3"/>
                                        <div className="flex-1">
                                            <h6 className="text-primary mb-0 medium-font">Language:</h6>
                                            <span className="text-muted">English</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-8 col-12">
                        <div className="rounded shadow p-4">
                            <h5 className="mb-0 large-font">Articles by {author.fullName}: </h5>

                            <div className="row projects-wrapper">
                                <ArticlesByAuthor articles={articles} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </>)
}

const AuthorCategories = ({articles}) => {

  function onlyUnique(value, index, array) {
    return array.indexOf(value) === index;
  }

  const uniqueCategories = articles.map(article => article.category)
                                     .flat()
                                     .filter(onlyUnique);

  if (uniqueCategories && uniqueCategories.length > 0) {
    return uniqueCategories.map((category, index) =>
      <Link href={"/blog/category/" + category}>
          <a key={"author-categories-" + index} className="text-muted mr-10" onClick={() => {
              ReactGA.send({hitType: "pageview", page: "/blog/category/" + category, title: "Category: " + category})}} >
            {category}
          </a>
      </Link>
    )
  } else {
    return <></>
  }
}

const ArticlesByAuthor = ({ articles }) => {

    return articles.map((article) => (
        <div className="col-md-6 col-12 mt-4 pt-2">
            <div className="card border-0 work-container work-primary work-classic">
                <div className="card-body p-0">

                    <Link href={"/article/" + article.url}>
                        <a onClick={() => {
                            ReactGA.send({ hitType: "pageview", page: "/article/" + article.url, title: "Article: " + article.url });
                        }}><img src={article.topicImage} className="img-fluid rounded work-image" alt="" /></a>
                    </Link>
                    <div className="content pt-3">
                        <h5 className="mb-0">
                            <Link onClick={() => {
                                ReactGA.send({ hitType: "pageview", page: "/article/" + article.url, title: "Article: " + article.url });
                            }} href={"/article/" + article.url} className="text-dark title">
                                {article.topic}
                            </Link>
                        </h5>
                        <h6 className="text-muted tag mb-0">{article.category}</h6>
                    </div>
                </div>
            </div>
        </div>
    ))

}
export default AuthorPage;

export async function getServerSideProps({res, query, req}) {
    if (!req) {
        return {
            author: {},
            articles: []
        };
    }

    let authorServer = {};
    let articlesServer = [];

    try {
        const [authorResponse, articlesResponse] = await Promise.all([
            UserService.getUserPublicInfoByName(query.name),
            ArticleService.getArticleByAuthorName(query.name)
        ]);
        if (authorResponse) {
            authorServer = await authorResponse.data;
        }
        if (articlesResponse) {
            articlesServer = await articlesResponse.data;
        }

    } catch (error) {
        res.setHeader("location", "/404");
        res.statusCode = 302;
        res.end();
    }

    return {
        props: {authorServer, articlesServer},
    };
}