import BlogService from "../service/BlogService";

function SiteMap() {
  // getServerSideProps does the heavy lifting
}

export async function getServerSideProps({res}) {

  const sitemap = await BlogService.getSitemap();

  res.setHeader('Content-Type', 'text/xml');
  res.write(sitemap.data);
  res.end();

  return {
    props: {},
  };
}

export default SiteMap;