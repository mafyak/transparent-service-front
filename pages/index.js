import React from "react";
import InfoBlockHome1 from "./components/Home/InfoBlockHome1";
import InfoBlockHome2 from "./components/Home/InfoBlockHome2";
import LatestArticlesHome from "./components/Home/LatestArticlesHome";
import TopBlockHome from "./components/Home/TopBlockHome";
import StaticContentService from "../service/StaticContentService";
import Head from "next/head";

const Home = (props) => {

  return (
    <>
      <Head>
        <title>{process.env.NEXT_PUBLIC_SITE_META_TITLE}</title>
        <meta property="og:title" content={process.env.NEXT_PUBLIC_SITE_META_TITLE}/>
        <meta name="description" content={process.env.NEXT_PUBLIC_SITE_META_DESC} />
        <meta property="og:description" content={process.env.NEXT_PUBLIC_SITE_META_DESC}/>
      </Head>

      <TopBlockHome history={props.history} textData={props.textData.topBlockHome}/>
      <InfoBlockHome2 textData={props.textData.infoBlockHome2}/>
      <LatestArticlesHome textData={props.textData.latestArticles}/>
      <InfoBlockHome1 textData={props.textData.infoBlockHome1}/>
    </>
  );
};

export async function getStaticProps(context) {

  const siteName = await StaticContentService.getSiteName();

  const staticContent =
    {
      Bazinga: {
        infoBlockHome1: {
          topbox: {
            header: "Million of advises at ReNailedIt",
            body: "We cover all type of home renovation and construction topics.\n" +
              "              No matter if you're doing construction on your own or want to get an independent opinion before picking up\n" +
              "              a contractor company, this is the place for you!",
            points: ["We help DIYers to avoid mistakes",
              "Not sure if your contractor is trying to fool you? Our blog can help!",
              "Not sure which hardwood option to choose? Our hardwood blog can help!",
              "Save your time and money by making right decisions. We can provide you with knowledge that can help you for free!"]
          },
        },
        infoBlockHome2: {
          header: "We're a team of independent contractors answering the most common questions",
          body: "100+ years of combined experience in flooring, roofing, siding, renovation, landscaping and other home and\n" +
            "              construction related niches in one place.\n" +
            "              We have collected the most interesting questions from our clients and now we're answering them in this\n" +
            "              blog."
        },
        latestArticles: {
          header: "Popular Articles",
          body: "In our blog you can find answers to a log ot construction related topics. Check out few of these articles here:"
        },
        topBlockHome: {
          header: "Home DIY portal. Find affordable contractor helper.",
          body: "Home construction and renovation portal. Home advisor for renovation, remodel. Hardwood flooring services advises. Find affordable contractor help."
        },
        header: {
          url: "ReNailedIt",
        },
        footer: {
          url: "ReNailedIt",
          slogan: "Home DIY portal. Find affordable contractor helper.",
          siteCopyrights: "2023 ReNailedIt.com"
        },
        metadata: {
          title: "Home DIY portal. Home tips for renovation, remodel. Hardwood flooring services advises",
          description: "Home DIY portal. Home tips for renovation, remodel. Hardwood flooring services advises",

        },
        analyticsTag: "G-111Y4WJ8EF"
      },
      reNailedIt: {
        infoBlockHome1: {
          topbox: {
            header: "Million of advises at ReNailedIt",
            body: "We cover all type of home renovation and construction topics.\n" +
              "              No matter if you're doing construction on your own or want to get an independent opinion before picking up\n" +
              "              a contractor company, this is the place for you!",
            points: ["We help DIYers to avoid mistakes",
              "Not sure if your contractor is trying to fool you? Our blog can help!",
              "Not sure which hardwood option to choose? Our hardwood blog can help!",
              "Save your time and money by making right decisions. We can provide you with knowledge that can help you for free!"]
          },
        },
        infoBlockHome2: {
          header: "We're a team of independent contractors answering the most common questions",
          body: "100+ years of combined experience in flooring, roofing, siding, renovation, landscaping and other home and\n" +
            "              construction related niches in one place.\n" +
            "              We have collected the most interesting questions from our clients and now we're answering them in this\n" +
            "              blog."
        },
        latestArticles: {
          header: "Popular Articles",
          body: "In our blog you can find answers to a log ot construction related topics. Check out few of these articles here:"
        },
        topBlockHome: {
          header: "Home DIY portal. Find affordable contractor helper.",
          body: "Home construction and renovation portal. Home advisor for renovation, remodel. Hardwood flooring services advises. Find affordable contractor help."
        },
        header: {
          url: "ReNailedIt",
        },
        footer: {
          url: "ReNailedIt",
          slogan: "Home DIY portal. Find affordable contractor helper.",
          siteCopyrights: "2023 ReNailedIt.com"
        },
        metadata: {
          title: "Home DIY portal. Home tips for renovation, remodel. Hardwood flooring services advises",
          description: "Home DIY portal. Home tips for renovation, remodel. Hardwood flooring services advises",

        },
        analyticsTag: "G-6RFY4WJ8EF"
      },
      cookMeSlowly: {
        infoBlockHome1: {
          topbox: {
            header: "Million of cocktail and food recipes at CookMeSlowly",
            body: "We have tons of great food and drink recipes. At CookMeSlowly you will find mouth-watering food and drink recipes from around the globe! " +
              " Set your inner chef free and embark on a flavorful journey with us.",
            points: ["CookMeSlowly helps people to cook new interesting meals",
              "Your online gateway to a world of delicious food",
              "Join our vibrant community of food and drink lovers",
              "Get inspired to create beautiful dishes"]
          },
        },
        infoBlockHome2: {
          header: "Quick, Easy and Delicious Recipes",
          body: "No more sifting through endless cookbooks or wandering around the web searching for reliable recipes. " +
            "We've simplified your search with an organized collection of recipes that are not just simple to make but also big on flavor. " +
            "From 30-minute meals to decadent desserts, nourishing breakfasts to delightful drinks, discover a plethora of recipes to suit every taste and occasion."
        },
        latestArticles: {
          header: "Popular Articles",
          body: "In our website you will find hundreds of amazing recipes. Check out few of these articles here:"
        },
        topBlockHome: {
          header: "Place to Cook!",
          body: "Cook Me Slowly is created by people for the people. Find interesting recipes and share them with your friends!"
        },
        header: {
          url: "CookMeSlowly",
        },
        footer: {
          url: "CookMeSlowly",
          slogan: "Home DIY portal. Find affordable contractor helper.",
          siteCopyrights: "2023 ReNailedIt.com"
        },
        metadata: {
          title: "Home DIY portal. Home tips for renovation, remodel. Hardwood flooring services advises",
          description: "Home DIY portal. Home tips for renovation, remodel. Hardwood flooring services advises",

        },
        analyticsTag: "G-X0GLQWCWH6"
      },
      MamaExpecting: {
        topBlockHome: {
          header: "We are a team of people who are passionate about supporting expectant parents",
          body: "You will find easy-to-understand and up-to-date information to guide you of the way through your pregnancy journey. We're here to help you and your growing family."
        },
        infoBlockHome2: {
          header: "Your companion throughout the wonderful adventure of pregnancy.",
          body: "We recognize that each pregnancy is a distinctive voyage brimming with thrill, expectation, and perhaps a touch of nervousness. From the moment of conception to the point of childbirth, we are committed to offering extensive resources and guidance to facilitate this journey, making it as effortless and delightful for you as possible."
        },
        infoBlockHome1: {
          topbox: {
            header: "By moms for moms",
            body: "Please remember that we  provide the most accurate and new information but our content should not replace professional medical advice, diagnosis, or treatment. Ask a healthcare professional for any suggestion about your health",
            points: ["Variety of topics ranging from morning sickness remedies to preparing for your newborn",
              "Guiding you through the journey of pregnancy",
              "Easy-to-read, easy-to-understand content",
              "Discussions on pregnancy and parenting"]
          },
        },
        latestArticles: {
          header: "Popular Articles",
          body: "In our website you will find hundreds of amazing articles. Check out few right here:"
        },
        header: {
          url: "MamaExpecting",
        },
        footer: {
          url: "MamaExpecting",
          slogan: "Mama Expecting. Nurturing Life: A Journey Through Baby Pregnancy",
          siteCopyrights: "2023 MamaExpecting.com"
        },
        metadata: {
          title: "Mama Expecting. Nurturing Life: A Journey Through Baby Pregnancy",
          description: "Mama Expecting. Nurturing Life: A Journey Through Baby Pregnancys. Your reliable companion throughout the wonderful adventure of pregnancy",
        },
        analyticsTag: "G-0NPWJN4KSF"
      },
      InsuranceSimplyExplained: {
        infoBlockHome1: {
          topbox: {
            header: "",
            body: "",
            points: []
          },
        },
        infoBlockHome2: {
          header: "",
          body: ""
        },
        latestArticles: {
          header: "Popular Articles",
          body: "On our website you will find hundreds of useful articles about all kind of insurances. Check out few of these articles here:"
        },
        topBlockHome: {
          header: "",
          body: ""
        },
        header: {
          url: "InsuranceSimplyExplained",
        },
        footer: {
          url: "InsuranceSimplyExplained",
          slogan: "",
          siteCopyrights: "2023 InsuranceSimplyExplained.com"
        },
        metadata: {
          title: "",
          description: "",

        },
        analyticsTag: "G-1111111111"
      },
      NewSite: {
        infoBlockHome1: {
          topbox: {
            header: "",
            body: "",
            points: []
          },
        },
        infoBlockHome2: {
          header: "",
          body: ""
        },
        latestArticles: {
          header: "Popular Articles",
          body: "On our website you will find hundreds of useful articles. Check out few of these articles here:"
        },
        topBlockHome: {
          header: "",
          body: ""
        },
        header: {
          url: "WEBSITE",
        },
        footer: {
          url: "WEBSITE",
          slogan: "",
          siteCopyrights: "2023 WEBSITE.com"
        },
        metadata: {
          title: "",
          description: "",

        },
        analyticsTag: "G-1111111111"
      },
    };

  return {
    props: {textData: staticContent[siteName.data]},
    revalidate: 86400, // this means that this data will be refreshed once 86400 secs(once a day). Page refresh DOESN'T trigger refresh.
  };
}

export default Home;
