import React, { useState, useEffect } from "react";
import Router from "next/router";
import { useRouter } from "next/router";
import { Home, User, Key } from "react-feather";
import { UserService, ValidationService, openAPI } from "@services";
import vkLogo from "../assets/images/vk-logo.png";
import { withAlert } from "react-alert";
import ArticleService from "../service/ArticleService";
import Image from "next/image";
import Head from "next/head";

const Login = ({ alert, job: serverJob }) => {
  // const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActivated, setIsActivated] = useState(false);
  const [isRedirected, setIsRedirected] = useState(false);
  const router = useRouter();
  const { emailAddress, code } = router.query;

  useEffect(() => {
    if (emailAddress && code && !isActivated) {
      setIsActivated(true);
      UserService.activateUser(emailAddress, code)
        .then((response) => {
          if (response.status == 200) {
            callSuccess("Thank you. Your account is verified!");
          }
        })
        .catch((e) => {
          callError("Can't verify your email. Try again or contact support");
        });
    }
    if (typeof window !== "undefined" && !isRedirected) {
      let loggedIn = localStorage.getItem("logged in");
      let fullName = localStorage.getItem("fullName");
      if (loggedIn && fullName) {
        callSuccess("You're already authenticated");
        setIsRedirected(true);
        Router.push({
          pathname: "/",
        });
      }
    }
  }, [router]);

  function onSelectChangeLogin({ target }) {
    setEmail(target.value);
  }

  function onSelectChangePassword({ target }) {
    setPassword(target.value);
  }

  function onLoginSetSecret(type, token, refresh_token) {
    localStorage.setItem("logged in", "true");
    localStorage.setItem("token", token);
    localStorage.setItem("refresh_token", refresh_token);

    UserService.getUserInfo().then((user) => {
      const bcrypt = require("bcryptjs");
      const salt = bcrypt.genSaltSync(10);
      const encodedType = bcrypt.hashSync(user.data.type, salt);
      localStorage.setItem("type", encodedType);
      localStorage.setItem("fullName", user.data.fullName);
      localStorage.setItem("id", user.data.id);
      if (localStorage.getItem("masterFirstRegistration") === "Y") {
        Router.push("/introPage/");
      } else {
        Router.push({
          pathname: "/",
          query: { loggedIn: JSON.stringify(true) },
        });
      }
    });
  }

  function callError(message) {
    alert.error(message);
  }

  function callSuccess(message) {
    alert.success(message);
  }

  function handleLogin(event) {
    event.preventDefault();
    if (ValidationService.validateEmail(email)) {
      var apiBaseUrl = "/oauth/token";
      var payload = {
        username: email,
        password: password,
        grant_type: "password",
      };
      const config = {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Basic b3VyVHJ1c3RlZENsaWVudDpjbGllbnRTZWNyZXQ=",
        },
      };
      const qs = require("querystring");
      var stringPayload = qs.stringify(payload);
      openAPI
        .post(apiBaseUrl, stringPayload, config)
        .then((response) => {
          if (response != null && response.status === 200) {
            callSuccess("Welcome to " + process.env.NEXT_PUBLIC_SITE_NAME);
            onLoginSetSecret(response.data.token_type, response.data.access_token, response.data.refresh_token);
          }
        })
        .catch((error) => {
          callError("Wrong credentials");
        });
    } else {
      callError("Check your e-mail");
    }
  }

  return (
    <>
    <Head>
      <title>Login page</title>
      <meta property="og:title" content="Login page"/>
      <meta name="description" content="Login page"/>
      <meta property="og:description" content="Login page"/>
    </Head>
    <section className="bg-home  image-backgr d-flex align-items-center">
      <div className="back-to-home rounded d-none d-sm-block">
        <a href="/" className="text-white title-dark rounded d-inline-block text-center">
          <Home className="fea icon-sm" />
        </a>
      </div>
      <div className="container">
        <div>
          <div className="row align-items-center">
            <div className="col-lg-5 col-md-6 mx-5 mx-md-0 mt-4 mt-sm-0 pt-2 pt-sm-0">
              <div className="card login-page bg-white shadow rounded border-0">
                <div className="card-body">
                  <h3 className="card-title text-center text-primary">Log in</h3>
                  <form className="login-form mt-4" onSubmit={handleLogin}>
                    <div className="row">
                      <div className="col-lg-12">
                        <div className="form-group position-relative">
                          <label>
                            Enter your credentials <span className="text-danger"> * </span>
                          </label>
                          <User className="fea icon-sm icons" />
                          <input
                            type="email"
                            className="form-control pl-5 pad-left-40"
                            // pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                            placeholder="e-mail"
                            required
                            name="email"
                            onChange={onSelectChangeLogin.bind(this)}
                          />
                        </div>
                      </div>
                      <div className="col-lg-12">
                        <div className="form-group position-relative">
                          <label>
                            Password <span className="text-danger"> * </span>
                          </label>
                          <Key className="fea icon-sm icons" />
                          <input
                            type="password"
                            className="form-control pl-5 pad-left-40"
                            placeholder="password"
                            required
                            onChange={onSelectChangePassword.bind(this)}
                          />
                        </div>
                      </div>
                      <div className="col-lg-12">
                        <div className="d-flex justify-content-end">
                          <p className="forgot-pass mb-2">
                            <a href="/recoverPassword" className="text-dark font-weight-bold">
                              Forgot password?
                            </a>
                          </p>
                        </div>
                      </div>
                      <div className="col-lg-12 mb-0 flexing-space-between">
                        <button className="btn btn-primary btn-block"> Log in </button>
                        <div className="pointer-cursor">
                          <a href="https://www.pinterest.com/oauth/?client_id=1487391&redirect_uri=https://renailedit.com/feoauth/pinterest/&response_type=code&scope=boards:read,boards:write,pins:read,pins:write">
                            <Image src="/img/pinterest-icon.jpg" alt="pinterest" width={42} height={42}/>
                          </a>
                        </div>
                      </div>
                      {/*<div className="col-lg-12 mt-4 text-center">*/}
                      {/*  <h6> Войти с помощью : </h6>*/}
                      {/*  <ul className="list-unstyled social-icon mb-0 mt-3">*/}
                      {/*    <li className="list-inline-item">*/}
                      {/*      <a*/}
                      {/*        href="https://oauth.vk.com/authorize?client_id=7508029&display=page&redirect_uri=https://rmt.by/socialLogin/&scope=email&response_type=code&v=5.110"*/}
                      {/*        className="rounded"*/}
                      {/*      >*/}
                      {/*        <img src={vkLogo} className="img-fluid d-block mx-auto" alt="vkLogo" />*/}
                      {/*        /!* <Icon path={mdiVk} title="Vkontakte" size={1.2} /> *!/*/}
                      {/*      </a>*/}
                      {/*    </li>*/}
                      {/*    /!* @todo add odnoklasniki login*/}
                      {/*      <li className="list-inline-item">*/}
                      {/*        <a href="#" className="rounded">*/}
                      {/*          <Icon path={mdiOdnoklassniki} title="Odnoklassniki" size={1.2} />*/}
                      {/*        </a>*/}
                      {/*      </li> *!/*/}
                      {/*  </ul>*/}
                      {/*</div>*/}
                      <div className="col-12 text-center mt-2 mb-2">
                        <p className="mb-0 mt-3">
                          <small className="text-dark mr-2"> No account ? </small>
                          <a href="/register" className="text-dark font-weight-bold">
                            Register
                          </a>
                        </p>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div className="col-lg-7 col-md-6">
              <div className="d-none d-md-block align-image-center">
                  <Image src="/img/login-page.png" className="img-fluid d-block mx-auto" alt="loginPic" width={360} height={352}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    </>
  );
};

export default withAlert()(Login);
