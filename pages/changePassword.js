import React, { useState } from "react";
import { Home } from "react-feather";
import DefaultPic from "../assets/images/recovery-small.png";
import { withAlert } from "react-alert";
import UserService from "../service/UserService";
import Router, { useRouter } from "next/router";

const ChangePassword = ({ alert }) => {
  const router = useRouter();
  const { token } = router.query;
  const [pass, setPass] = useState("");
  const [rePass, setRePass] = useState("");

  function changePass({ target }) {
    setPass(target.value);
  }

  function changeRePass({ target }) {
    setRePass(target.value);
  }

  function callSuccess(message) {
    alert.success(message);
  }

  function callError(message) {
    alert.error(message);
  }

  function saveNewPass(e) {
    e.preventDefault();
    if (!pass.match("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}")) {
      callError("Пароль должен содержать как минимум одну цифру, одну заглавную и одну строчную латинскую букву. Длина пароля: 8 или более символов");
      return;
    }
    if (pass == rePass) {
      if (token) {
        const bcrypt = require("bcryptjs");
        const salt = bcrypt.genSaltSync(10);
        const encodedPass = bcrypt.hashSync(pass, salt);
        let request = {
          token: token,
          newPass: encodedPass,
        };
        UserService.updatePassword(request).then((response) => {
          if (response.status == 200) {
            callSuccess("Пароль успешно изменён.");
            Router.push("/login");
          } else {
            // todo add better error message(in case token is expired for example)
            callError("Произошла ошибка. Проверьте данные или обратитесь в поддержку.");
          }
        });
      }
    } else {
      callError("Пароли должны совпадать");
    }
  }

  return (
    <section className="bg-home d-flex align-items-center">
      <div className="back-to-home rounded d-none d-sm-block">
        <a href="./" className="text-white title-dark rounded d-inline-block text-center">
          <Home className="fea icon-sm" />
        </a>
      </div>
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-7 col-md-6">
            <div className="mr-lg-5">
              <img src={DefaultPic} className="img-fluid d-block mx-auto" alt="" />
            </div>
          </div>
          <div className="col-lg-5 col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
            <div className="card login_page shadow rounded border-0">
              <div className="card-body">
                <h4 className="card-title text-center">Создайте новый пароль</h4>

                <form className="login-form mt-4">
                  <div className="row">
                    <div className="col-lg-12">
                      <p className="text-muted">Пожалуйста, введите новый пароль для вашего профиля.</p>
                      <div className="form-group position-relative">
                        <label>
                          Пароль <span className="text-danger">*</span>
                        </label>
                        <i data-feather="mail" className="fea icon-sm icons"></i>
                        {/* todo this required regex doesn't work */}
                        <input
                          type="password"
                          className="form-control pl-5"
                          placeholder="Введите ваш пароль"
                          name="password"
                          pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                          value={pass}
                          onChange={changePass}
                          title="Пароль должен содержать как минимум одну цифру, одну заглавную и одну строчную латинскую букву. Длина пароля: 8 или более символов"
                          required
                        />
                      </div>{" "}
                      <div className="form-group position-relative">
                        <label>
                          Повторите пароль <span className="text-danger">*</span>
                        </label>
                        <i data-feather="mail" className="fea icon-sm icons"></i>
                        <input
                          type="password"
                          className="form-control pl-5"
                          placeholder="Введите ваш пароль"
                          name="rePassword"
                          value={rePass}
                          onChange={changeRePass}
                          required
                        />
                      </div>
                    </div>
                    <div className="col-lg-12">
                      <button
                        className="btn btn-primary btn-block"
                        onClick={(e) => {
                          saveNewPass(e);
                        }}
                      >
                        Отправить
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default withAlert()(ChangePassword);
