import React, { Component } from "react";
import UserService from "../../service/UserService";
import AdminService from "../../service/AdminService";

class AdminUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      totalPages: "",
      first: "",
      last: "",
      number: "",
      size: "",
    };
  }

  componentDidMount() {
    this.refreshData(20, 0, 0, 0);
  }

  refreshData(size, first, last, number) {
    UserService.getAllUsersForAdmin().then((response) => {
      if (response) {
        this.setState({
          users: response.data.content,
        });
      }
    });
  }

  getPaginableUsers(page, size) {
    const { coveredCityId, sortByRating, currentJobTemplateId } = this.state;
    CompanyService.retrieveFilteredPaginableCompanies(page, size, coveredCityId, sortByRating, currentJobTemplateId).then(({ data }) => {
      this.setState({
        companies: data.content,
        totalPages: data.totalPages,
        first: data.first,
        last: data.last,
        number: data.number,
        size: data.size,
      });
    });
  }

  printPagination() {
    const { totalPages, first, last, number, size } = this.state;
    if (totalPages) {
      return (
        <div className="d-flex p-4 justify-content-center">
          <ul className="pagination mb-0">
            <li className="page-item">
              <button
                className="page-link"
                key="prev"
                aria-label="Previous"
                onClick={() => {
                  if (!first) this.getPaginableUsers(number - 1, size);
                }}
              >
                Назад
              </button>
            </li>
            {Array.from(Array(totalPages), (e, i) => (
              <li className={"page-item" + (number == i ? " active" : "")} key={"page" + i}>
                <button className="page-link" onClick={() => this.getPaginableUsers(i, size)}>
                  {i + 1}
                </button>
              </li>
            ))}
            <li className="page-item">
              <button
                className="page-link"
                key="next"
                aria-label="Next"
                onClick={() => {
                  if (!last) this.getPaginableUsers(number + 1, size);
                }}
              >
                Вперед
              </button>
            </li>
          </ul>
        </div>
      );
    }
  }

  printTableRows() {
    if (this.state.users) {
      return this.state.users.map((user) => (
        <tr key={user.id} value={user.id}>
          <td scope="row">{user.id}</td>
          <th>
            <div className="d-flex align-items-center">
              <a href={"/user/" + user.id} target="_blank" className="mb-0 font-weight-normal h5">
                {user.fullName}
              </a>
            </div>
          </th>
          <td>{user.type}</td>
          <td>{user.jobCount}</td>
          <td>{user.averageRate ? user.averageRate.toFixed(2) : user.averageRate}</td>
          <td>{user.registrationDate}</td>
          <td>
            <div className="btn-group dropdown-primary">
              <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Действие
              </button>
              <div className="dropdown-menu">
                <a className="dropdown-item" href={"/user/" + user.id}>
                  Открыть
                </a>
              </div>
            </div>
          </td>
        </tr>
      ));
    }
  }

  printHeader() {
    return (
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col" style={{ minWidth: "250px" }}>
            Имя
          </th>
          <th scope="col">Тип</th>
          <th scope="col">JobsCount</th>
          <th scope="col">AverageRating</th>
          <th scope="col">RegistrationDate</th>
          <th scope="col" style={{ width: "100px" }}>
            Действие
          </th>
        </tr>
      </thead>
    );
  }

  render() {
    if (AdminService.checkIfAdmin()) {
      return (
        <>
          <div className="bg-half-170 bg-light d-table w-100">
            <div className="container">
              <h2>Список пользователей</h2>
              <p>Список пользователей зарегистрированых на платформе. Разрешённые действия к обсуждению.</p>
            </div>
          </div>
          <div className="section border-top">
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-lg-12">
                  <div className="table-responsive crypto-table bg-white shadow rounded">
                    <table className="table mb-0 table-center">
                      {this.printHeader()}
                      <tbody>{this.printTableRows()}</tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return <></>;
    }
  }
}
export default AdminUsers;
