import React, {useEffect, useState} from "react";
import AdminService from "../../service/AdminService";
import ArticleService from "../../service/ArticleService";
import {withAlert} from "react-alert";

const AdminAi = ({alert}) => {

  const [newArticleName, setNewArticleName] = useState("");
  const [newCategoryName, setNewCategoryName] = useState("");
  const [newPriority, setNewPriority] = useState("");
  const [recommendations, setRecommendations] = useState([]);
  const [categories, setCategories] = useState([]);
  const [perPage, setPerPage] = useState(25);
  // todo add filtering logic here

  useEffect(() => {
    getAiArticleRecommendation();
    getCategories();
  }, []);

  function updateArticleName({target}) {
    setNewArticleName(target.value);
  }

  function updateCategoryName({target}) {
    setNewCategoryName(target.value);
  }

  function updateCategoryNameSimple(newValue) {
    setNewCategoryName(newValue);
  }

  function updatePriority({target}) {
    setNewPriority(target.value);
  }

  function getAiArticleRecommendation() {
    AdminService.getAiArticleRecommendation()
    .then((result) => {
      setRecommendations(result.data);
    })
  }

  function getCategories() {
    ArticleService.getAllCategories()
    .then((result) => {
      setCategories(result.data);
    })
  }

  function addAiRecommendation() {
    const aiRecommendation = {};
    aiRecommendation.topic = newArticleName;
    aiRecommendation.category = newCategoryName;
    aiRecommendation.priority = newPriority;
    AdminService.addAiArticleRecommendation(aiRecommendation)
    .then((response) => {
      printAlert(response, "Done!", "Can't add recommendation =(");
      setNewArticleName("");
    });
  }

  function generateArticle() {
    AdminService.generateArticle()
    .then((response) => printAlert(response, "Done!", "Can't generate =("));
  }

  function generateSchema() {
    AdminService.generateSchema()
    .then((response) => printAlert(response, response.data + " articles updated.", "Can't generate =("));
  }

  function generateRelatedArticles() {
    AdminService.generateRelatedArticles()
    .then((response) => printAlert(response, response.data + " articles updated.", "Can't generate =("));
  }

  function printAlert(response, successMessage, failMessage) {
    if (response && (response.status === 200 || response.status === 201)) {
      callSuccess(successMessage);
    } else {
      callError(failMessage);
    }
  }

  function callSuccess(message) {
    alert.success(message);
  }

  function callError(message) {
    alert.error(message);
  }

  if (AdminService.checkIfAdmin()) {
    return (
      <>
        <section className="bg-half-170 bg-light d-table w-100">
          <div className="mt-4 pt-2 container" id="forms">
            <div className="component-wrapper rounded shadow bg-white">
              <div className="p-4 border-bottom">
                <h4 className="title mb-0">Generate article</h4>
                <p className="mb-0">Article generation tool</p>
              </div>
              <div className="p-4">
                <form>
                  <div className="row">
                    <div className="col-md-2">
                      <div className="form-group position-relative">
                        <a type="submit" className="btn btn-success" onClick={generateArticle}>
                          Generate article
                        </a>
                      </div>
                    </div>
                    <div className="col-md-2">
                      <div className="form-group position-relative">
                        <a type="submit" className="btn btn-success" onClick={generateSchema}>
                          Generate schema
                        </a>
                      </div>
                    </div>
                    <div className="col-md-2">
                      <div className="form-group position-relative">
                        <a type="submit" className="btn btn-success" onClick={generateRelatedArticles}>
                          Generate related articles
                        </a>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div className="mt-4 pt-2 container" id="forms">
            <div className="component-wrapper rounded shadow bg-white">
              <div className="p-4 border-bottom">
                <h4 className="title mb-0">Add ai recommendation</h4>
                <p className="mb-0">Add topic, category and priority for ai recommendations</p>

              </div>
              <div className="p-4">
                <form>
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group position-relative">
                        <input
                          name="name"
                          id="name"
                          type="text"
                          value={newArticleName}
                          className="form-control pl-3"
                          placeholder="Article name"
                          onChange={updateArticleName}
                        />
                      </div>
                      <p className="mb-0">Current categories(click to select):
                        {
                          categories.map((r, index) =>
                            <span onClick={() => updateCategoryNameSimple(r)} key={"reco_" + index}>
                              {" " + r}
                            </span>)
                        }
                      </p>
                      <div className="form-group position-relative">
                        <input
                          name="name"
                          id="name"
                          type="text"
                          value={newCategoryName}
                          className="form-control pl-3"
                          placeholder="Category name"
                          onChange={updateCategoryName}
                        />
                      </div>
                      <div className="form-group position-relative">
                        <input
                          name="name"
                          id="name"
                          type="text"
                          value={newPriority}
                          className="form-control pl-3"
                          placeholder="Priority (number from 0 to 100)"
                          onChange={updatePriority}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-2">
                      <div className="form-group position-relative">
                        <a type="submit" className="btn btn-success" onClick={addAiRecommendation}>
                          Save
                        </a>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <AiRecommendationsTable key="new_rec" title="New recommendations" status="NOT_USED" recomms={recommendations}
                                  printAlert={printAlert} getAiArticleRecommendation={getAiArticleRecommendation}/>

          <AiRecommendationsTable key="bl_rec" title="Blacklisted recommendations" status="BLACK_LISTED"
                                  recomms={recommendations} printAlert={printAlert}
                                  getAiArticleRecommendation={getAiArticleRecommendation}/>

          <AiRecommendationsTable key="used_rec" title="Used recommendations" status="USED" recomms={recommendations}
                                  printAlert={printAlert} getAiArticleRecommendation={getAiArticleRecommendation}/>

        </section>
      </>
    );
  } else {
    return <></>;
  }
}

const AiRecommendationsTable = ({title, status, recomms, printAlert, getAiArticleRecommendation}) => {

  function deleteRecommendation(recommendationId) {
    if (recommendationId) {
      AdminService.deleteRecommendation(recommendationId)
      .then((removeResponse) => {
        printAlert(removeResponse, "Recommendation removed", "Can't remove recommendation");
        getAiArticleRecommendation();
      });
    }
  }

  if (!recomms) {
    return (<></>)
  }

  return (
    <div className="mt-4 pt-2 container" id="forms">
      <div className="component-wrapper rounded shadow bg-white">
        <div className="p-4 border-bottom">
          <h4 className="title mb-0">{title}</h4>
          <p className="mb-0">List of recommendations sorted by priority</p>
          {"# of recommendations: " + recomms.filter(recommendation => recommendation.status === status).length}
        </div>
        <div className="table-responsive p-4">
          <table className="table table-center">
            <thead>
            <tr>
              <th scope="col">
                ID
              </th>
              <th scope="col" className="w-25">
                Article
              </th>
              <th scope="col" className="w-25">
                Category
              </th>
              <th scope="col" className="w-25">
                Source
              </th>
              <th scope="col" className="w-25">
                Priority
              </th>
              <th scope="col">
                Delete
              </th>
            </tr>
            </thead>
            <tbody>
            {recomms
            .filter(recommendation => recommendation.status === status)
            .map((recommendation) => (
              <tr key={recommendation.id}>
                <td>{recommendation.id}</td>
                <td>
                  <div className="d-flex align-items-center">
                    <p className="mb-0 font-weight-normal h5">{recommendation.topic}</p>
                  </div>
                </td>
                <td>
                  <div className="d-flex align-items-center">
                    <p className="mb-0 font-weight-normal h5">{recommendation.category}</p>
                  </div>
                </td>
                <td>
                  <div className="d-flex align-items-center">
                    <p className="mb-0 font-weight-normal h5">{recommendation.source}</p>
                  </div>
                </td>
                <td>
                  <div className="d-flex align-items-center">
                    <p className="mb-0 font-weight-normal h5">{recommendation.priority}</p>
                  </div>
                </td>
                <td>
                  <div className="btn-group dropdown-primary d-flex justify-content-end">
                    <button type="button" className="btn btn-success"
                            onClick={() => deleteRecommendation(recommendation.id)}>
                      Delete
                    </button>
                  </div>
                </td>
              </tr>
            ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default withAlert()(AdminAi);
