import React, {useState, useEffect} from "react";
import {useRouter} from "next/router";
import {AdminService, FileService, ArticleService} from "@services";
import {withAlert} from "react-alert";
import Head from 'next/head';
import PageHead from "../../../../components/blog/pageHead";
import Tags from "../../../../components/blog/tags";
import ParagraphsContent from "../../../../components/blog/paragraphsContent";
import RelatedArticlesBlock from "../../../../components/blog/relatedArticlesBlock";
import Sidebar from "../../../../components/blog/sidebar";
import {UtilService} from "../../../../../service";

const AdminArticle = ({alert}) => {

  const router = useRouter();
  const {category, articleUrl} = router.query;
  const [article, setArticle] = useState(null);
  const [sidebar, setSidebar] = useState(null);
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    async function load() {
      const [articleResponse, sidebarResponse] = await Promise.all([
        ArticleService.getArticleByUrl(category, articleUrl),
        ArticleService.getBlogSidebar()
      ]);
      if (articleResponse) {
        setArticle(articleResponse.data);
      }
      if (sidebarResponse) {
        setSidebar(sidebarResponse.data);
      }
    }

    if (!article && articleUrl) {
      load();
    }
    setIsLoggedIn(localStorage.getItem("logged in"));
  }, [articleUrl]);

  console.log("render");
  if (article) {
    return (
      <>
        <Head>
          <title>{article.topic}</title>
          <meta property="og:title" content={article.topic}/>
          <meta name="description" content={article.topic}/>
          <meta property="og:description" content={article.topic}/>
        </Head>

        <PageHead article={article} category={category}/>

        <section className="section section-override">
          <div className="container">
            <div className="row">
              <div className="col-lg-8 col-md-6">
                <div className="card blog blog-detail border-0 shadow rounded">
                  <AdminMetadata article={article}/>
                  <AdminButtons isLoggedIn={isLoggedIn} article={article} alert={alert}/>
                  <img src={article.topicImage} className="img-fluid rounded-top" alt=""/>
                  <div className="card-body content">
                    <Tags tags={article.tags}/>
                    <ParagraphsContent paragraphs={article.content}/>
                  </div>
                </div>

                <RelatedArticlesBlock relatedArticles={article.relatedArticles}/>

              </div>
              <Sidebar sidebar={sidebar} article={article}/>
            </div>
          </div>
        </section>
      </>
    );
  } else {
    return <></>
  }
}

const AdminMetadata = ({article}) => {

  const [metadata, setMetadata] = useState(null);

  useEffect(() => {
    async function load() {
      ArticleService.getArticleMetadata(article.id)
      .then(resp => setMetadata(resp.data));
    }

    if (!metadata) {
      load();
    }
  }, [article]);

  if (metadata) {
    return <div className="title-block">
      <div className="title">Metadata</div>
      <div className="metadata">
        <div><span className="bold_text">Timestamp :</span> {UtilService.printDate(metadata.timestamp)}</div>
        <div><span className="bold_text">HumanFactor :</span> {metadata.humanFactor}</div>
        <div><span className="bold_text">FakePercentage :</span> {metadata.fakePercentage}</div>
        <div><span className="bold_text">GenerationAttempts :</span> {metadata.generationAttempts}</div>
        <div><span className="bold_text">GenerationRequest :</span> {metadata.generationRequest}</div>
        <div><span className="bold_text">ParaphraseRequest :</span> {metadata.paraphraseRequest}</div>
        <div><span className="bold_text">ParaphraseSingleRequest :</span> {metadata.paraphraseSingleRequest}</div>
        <div><span className="bold_text">AiModelUsed :</span> {metadata.aiModelUsed}</div>
        <div><span className="bold_text">Midjourney :</span> {article.topic + " --v 5"}</div>
      </div>
    </div>
  } else {
    return <></>;
  }
}

const AdminButtons = ({isLoggedIn, article, alert}) => {

  const [imageIsLoading, setImageIsLoading] = useState(false);
  const [imageError, setImageError] = useState(false);

  function isType(aType) {
    const type = localStorage.getItem("type");
    const bcrypt = require("bcryptjs");
    return type && bcrypt.compareSync(aType, type);
  }

  function approveArticle() {
    AdminService.approveArticle(article)
    .then(() => callSuccess("Article approved"));
  }

  function deleteArticle() {
    AdminService.deleteArticle(article)
    .then(() => callSuccess("Article removed"));
  }

  function sendToArticleModeration() {
    AdminService.sendToArticleModeration(article)
    .then(() => callSuccess("Article sent to moderation"));
  }

  function addToPinterest() {
    AdminService.addToPinterest(article)
    .then(() => callSuccess("Article added to Pinterest"));
  }

  function callSuccess(message) {
    alert.success(message);
  }

  function selectLogoFile(e, id) {
    setImageIsLoading(true);
    setImageError(false);
    saveImage(e.target.files[0], id);
  }

  function saveImage(file, id) {
    const image = new FormData();
    image.append("image", file);

    if (file.size < 524288000) {
      FileService.updateArticleImage(image, id).then((res) => {
        console.log("data: " + JSON.stringify(res));
        if (res && res.status === 200) {
          alert.success("Image is uploaded!");
          setImageIsLoading(false);
          // todo add hook here to reload page
        } else {
          alert.error("Can't upload file");
          setImageError(true);
        }
      });
    } else {
      alert.error("Can't upload file >500 Mb");
    }
  }

  if (isLoggedIn && isType("ADMIN")) {
    return (
      <div>
        <button type="submit" onClick={() => approveArticle()} className="btn btn-primary btn-block mr-3">
          Approve article
        </button>
        <button type="submit" onClick={() => addToPinterest()} className="btn btn-primary btn-block mr-3">
          Add to Pinterest
        </button>
        <button type="submit" onClick={() => sendToArticleModeration()} className="btn btn-primary btn-block mr-3">
          Send to moderation
        </button>
        <button type="submit" onClick={() => deleteArticle()} className="btn btn-primary btn-block mr-3">
          Delete article
        </button>
        <div>
          Change image:
          {!imageIsLoading && !imageError
            ? <span className="text-white"/>
            : <span>Loading...</span>
          }
          <input
            className="form-control-file mb-4 ms-2"
            name="articleImage"
            type="file"
            onChange={(e) => selectLogoFile(e, article.id)}
          />
        </div>

      </div>
    )
  } else {
    return <></>
  }
}

export default withAlert()(AdminArticle);