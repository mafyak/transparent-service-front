import React, {useState, useEffect} from "react";
import BlogService from "../../../service/BlogService";
import ArticleService from "../../../service/ArticleService";
import Head from "next/head";
import BlogMiniArticles from "../../components/blog/blogMiniArticles";
import BlogSidebar from "../../components/blog/blogSidebar";
import BlogTopSection from "../../components/blog/blogTopSection";

const AdminBlog = () => {

  const tagOrCategory = "category";
  const tagOrCategoryName = "flooring";
  const [sidebar, setSidebar] = useState(null);
  const [blog, setBlog] = useState(null);
  // todo complete
  const [paginator, setPaginator] = useState({
    totalPages: 1,
    first: true,
    last: true,
    number: 1,
    size: 100,
    pageNumber: 1
  });

  useEffect(() => {
    async function load() {
      BlogService.getBlogForAdmin().then(response => {
        setBlog(response.data);
      });
      ArticleService.getBlogSidebarForAdmin().then((response) => {
        setSidebar(response.data);
      })
    }

    if (!blog) {
      load();
    }
  }, []);

  if (blog) {
    return (
      <>
        <BlogTopSection tagOrCategory={tagOrCategory} tagOrCategoryName={tagOrCategoryName}/>

        <div className="position-relative" key="div1">
          <div className="shape overflow-hidden text-color-white">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"/>
            </svg>
          </div>
        </div>

        <section className="section">
          <div className="container">
            <div className="row">
              <div className="col-lg-8 col-md-6">
                <div className="row">
                  <h1>Articles to be reviewed</h1>
                  <BlogMiniArticles tagOrCategoryName={tagOrCategoryName} tagOrCategory={tagOrCategory}
                                    blog={blog.filter((b) => "IN_MODERATION" === b.status)}
                                    prefixUrl={"/admin/blog/article/"} paginator={paginator}/>

                  <h1>Articles that need changes</h1>
                  <BlogMiniArticles tagOrCategoryName={tagOrCategoryName} tagOrCategory={tagOrCategory}
                                    blog={blog.filter((b) => "NEEDS_CHANGES" === b.status)}
                                    prefixUrl={"/admin/blog/article/"} paginator={paginator}/>
                </div>
              </div>

              <BlogSidebar sidebar={sidebar} tagOrCategoryName={tagOrCategoryName}
                           tagOrCategory={tagOrCategory} isAdmin={true} blog={blog}/>
            </div>
          </div>
        </section>
      </>
    )
  } else {
    return <></>
  }
}

export default AdminBlog;