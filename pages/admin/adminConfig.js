import React, { Component } from "react";
import AdminService from "../../service/AdminService";
import Modal from "react-awesome-modal";
import { withAlert } from "react-alert";

class AdminConfig extends Component {
  constructor(props) {
    super(props);
    this.state = {
      configs: [],
      currentConfig: { id: "", key: "", value: "" },
      visibleModal: false,
    };
    this.closeModal = this.closeModal.bind(this);
    this.openModal = this.openModal.bind(this);
    this.updateCurrentConfigValue = this.updateCurrentConfigValue.bind(this);
  }

  updateCurrentConfigValue({ target }) {
    this.setState((prevState) => ({
      currentConfig: {
        ...prevState.currentConfig,
        value: target.value,
      },
    }));
  }

  saveConfig() {
    AdminService.updateConfiguration(this.state.currentConfig).then((response) => {
      if (response && response.status == 200) {
        this.callSuccess("Конфигурация изменена");
      }
      this.closeModal();
      this.refreshData();
    });
  }

  openModal(config) {
    this.setState({
      currentConfig: config,
      visibleModal: true,
    });
  }

  closeModal() {
    this.setState({
      visibleModal: false,
    });
  }

  refreshData() {
    AdminService.getConfiguration().then((response) => {
      this.setState({
        configs: response.data,
      });
    });
  }

  componentDidMount() {
    this.refreshData();
  }

  callSuccess(message) {
    alert = withAlert();
    this.props.alert.success(message);
  }

  callError(message) {
    alert = withAlert();
    this.props.alert.error(message);
  }

  printTableRows() {
    if (this.state.configs) {
      return this.state.configs.map((config) => (
        <tr key={config.id} value={config.id}>
          <td className="text-center p-3">{config.id}</td>
          <th className="p-3 admin-table-cell">{config.key}</th>
          <td className="p-3 admin-table-cell">{config.value}</td>
          <td className="text-center">
            <button type="button" className="btn btn-primary m-3" onClick={() => this.openModal(config)}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
                className="feather feather-settings"
              >
                <circle cx="12" cy="12" r="3" />
                <path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z" />
              </svg>
            </button>
          </td>
        </tr>
      ));
    }
  }

  render() {
    if (AdminService.checkIfAdmin()) {
      return (
        <>
          <div className="bg-main bg-light w-100 border-top border-bottom">
            <div className="container">
              <h2>Список настроек системы</h2>
              <p>ОСТОРОЖНО!!! Изменение этих данных может привести к некорректной работе всего приложения, ДВАЖДЫ УБЕДИСЬ ЧТО ЗНАЕШЬ ЧТО ДЕЛАЕШЬ!</p>
            </div>
          </div>
          <div className="mt-5 mb-5">
            <div className="container">
              <table className="table mb-0 table-center" style={{ tableLayout: "fixed" }}>
                <thead>
                  <tr className="text-center">
                    <th style={{ width: "7%" }} scope="col">
                      ID
                    </th>
                    <th style={{ width: "30%" }} scope="col">
                      Параметр
                    </th>
                    <th style={{ width: "33%" }} scope="col">
                      Значение
                    </th>
                    <th style={{ width: "30%" }} scope="col">
                      Сохранить
                    </th>
                  </tr>
                </thead>
                <tbody>{this.printTableRows()}</tbody>
              </table>
            </div>
            <Modal visible={this.state.visibleModal} width="480" effect="fadeInUp" onClickAway={this.closeModal}>
              <div className="modalStyle">
                <h1>Изменение конфигурации</h1>
                <p>{this.state.currentConfig.key}</p>
                <input
                  className="form-control pl-4"
                  value={this.state.currentConfig.value}
                  onChange={this.updateCurrentConfigValue}
                  placeholder="Значение конфигурации"
                />
                <button type="button" className="btn btn-primary modalStyle" onClick={() => this.saveConfig()}>
                  Сохранить
                </button>
                <button type="button" className="btn btn-primary modalStyle" onClick={() => this.closeModal()}>
                  Закрыть
                </button>
              </div>
            </Modal>
          </div>
        </>
      );
    } else {
      return <></>;
    }
  }
}

export default withAlert()(AdminConfig);
