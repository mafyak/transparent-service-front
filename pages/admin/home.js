import React from "react";
import JobsImg from "../../assets/images/walls-painting.jpg";
import Ava05 from "../../assets/images/avatars/ava05.jpg";
import Ava06 from "../../assets/images/avatars/ava06.jpg";
import Full from "../../assets/images/full.jpg";
import Invoice from "../../assets/images/invoice.png";
import Banner from "../../assets/images/banner.jpg";
import Cities from "../../assets/images/cities.png";
import AdminService from "../../service/AdminService";
import { withAlert } from "react-alert";
import Router from "next/router";
import Link from "next/link";
import Image from "next/image";

const AdminHome = ({ alert }) => {
  if (AdminService.checkIfAdmin()) {
    return (
      <section className="section border-top">
        <div className="container mt-100 mt-60">
          <div className="row justify-content-center">
            <div className="col-12 text-center">
              <div className="section-title mb-4 pb-2">
                <h4 className="title mb-4">Admin functionality</h4>
                <p className="text-muted para-desc mb-0 mx-auto">
                  Pick one of the available categories available for an admin
                </p>
              </div>
            </div>
          </div>

          <div className="row">

            <div className="col-md-3 col-12 mt-4 pt-2">
              <div className="text-center">
                <div className="rounded p-4 shadow">
                  <Link href="/admin/adminConfig/">
                    <a>
                      <Image src="/img/admin/config.png" className="mx-auto d-block" alt="ConfigPic" width={100} height={100}/>
                    </a>
                  </Link>
                </div>
                <div className="mt-3">
                  <h5>
                    <Link href="/admin/adminConfig/">
                      <a className="text-primary">System config</a>
                    </Link>
                  </h5>
                </div>
              </div>
            </div>

            <div className="col-md-3 col-12 mt-4 pt-2">
              <div className="text-center">
                <div className="rounded p-4 shadow">
                  <Link href="/admin/adminAi/">
                    <a>
                      <Image src="/img/admin/ai_image.jpg" className="mx-auto d-block" alt="ConfigPic" width={100} height={100}/>
                    </a>
                  </Link>
                </div>
                <div className="mt-3">
                  <h5>
                    <Link href="/admin/adminAi/">
                      <a className="text-primary">Ai</a>
                    </Link>
                  </h5>
                </div>
              </div>
            </div>

            <div className="col-md-3 col-12 mt-4 pt-2">
              <div className="text-center">
                <div className="rounded p-4 shadow">
                  <Link href="/admin/blog/blog/">
                    <a>
                      <Image src="/img/admin/blog.jpg" className="mx-auto d-block" alt="ConfigPic" width={100} height={100}/>
                    </a>
                  </Link>
                </div>
                <div className="mt-3">
                  <h5>
                    <Link href="/admin/blog/blog/">
                      <a className="text-primary">Blog</a>
                    </Link>
                  </h5>
                </div>
              </div>
            </div>

          </div>

          <div className="row">
            <div className="col-md-3 col-12 mt-4 pt-2">
              <div className="text-center">
                <div className="rounded p-4 shadow">
                  <Link href="/admin/adminSubscriptions/">
                    <a>
                      <img src={Full} height="100" className="mx-auto d-block" alt="" />
                    </a>
                  </Link>
                </div>

                <div className="mt-3">
                  <h5>
                    <Link href="/admin/adminSubscriptions/">
                      <a className="text-primary">Подписки</a>
                    </Link>
                  </h5>
                  <p className="text-muted mb-0">Добавить\изменить\удалить</p>
                </div>
              </div>
            </div>

            <div className="col-md-3 col-12 mt-4 pt-2">
              <div className="text-center">
                <div className="rounded p-4 shadow">
                  <Link href="/admin/adminCompanies/">
                    <a>
                      <img src={Ava06} height="100" className="mx-auto d-block" alt="" />
                    </a>
                  </Link>
                </div>

                <div className="mt-3">
                  <h5>
                    <Link href="/admin/adminCompanies/">
                      <a className="text-primary">Компании</a>
                    </Link>
                  </h5>
                  <p className="text-muted mb-0">Добавить\изменить\удалить</p>
                </div>
              </div>
            </div>

            <div className="col-md-3 col-12 mt-4 pt-2">
              <div className="text-center">
                <div className="rounded p-4 shadow">
                  <Link href="/admin/adminUsers/">
                    <a>
                      <img src={Ava05} height="100" className="mx-auto d-block" alt="" />
                    </a>
                  </Link>
                </div>

                <div className="mt-3">
                  <h5>
                    <Link href="/admin/adminUsers/">
                      <a className="text-primary">Пользователи</a>
                    </Link>
                  </h5>
                  <p className="text-muted mb-0">Добавить\изменить\удалить</p>
                </div>
              </div>
            </div>

            <div className="col-md-3 col-12 mt-4 pt-2">
              <div className="text-center">
                <div className="rounded p-4 shadow">
                  <Link href="/admin/jobTemplates/">
                    <a>
                      <img src={JobsImg} height="100" className="mx-auto d-block" alt="" />
                    </a>
                  </Link>
                </div>

                <div className="mt-3">
                  <h5>
                    <Link href="/admin/jobTemplates/">
                      <a className="text-primary">Базовые Работы</a>
                    </Link>
                  </h5>
                  <p className="text-muted mb-0">Добавить\изменить\удалить</p>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-md-3 col-12 mt-4 pt-2">
              <div className="text-center">
                <div className="rounded p-4 shadow">
                  <Link href="/admin/adminCities/">
                    <a>
                      <img src={Cities} height="100" className="mx-auto d-block" alt="" />
                    </a>
                  </Link>
                </div>

                <div className="mt-3">
                  <h5>
                    <Link href="/admin/adminCities/">
                      <a className="text-primary">Города</a>
                    </Link>
                  </h5>
                  <p className="text-muted mb-0">Добавить\изменить\удалить</p>
                </div>
              </div>
            </div>

            <div className="col-md-3 col-12 mt-4 pt-2">
              <div className="text-center">
                <div className="rounded p-4 shadow">
                  <Link href="/admin/adminInvoices/">
                    <a>
                      <img src={Invoice} height="100" className="mx-auto d-block" alt="" />
                    </a>
                  </Link>
                </div>

                <div className="mt-3">
                  <h5>
                    <Link href="/admin/adminInvoices/">
                      <a className="text-primary">Счета</a>
                    </Link>
                  </h5>
                  <p className="text-muted mb-0">Посмотреть\удалить</p>
                </div>
              </div>
            </div>

            <div className="col-md-3 col-12 mt-4 pt-2">
              <div className="text-center">
                <div className="rounded p-4 shadow">
                  <Link href="/admin/adminBanners/">
                    <a>
                      <img src={Banner} height="100" width="230" className="mx-auto d-block" alt="" />
                    </a>
                  </Link>
                </div>

                <div className="mt-3">
                  <h5>
                    <Link href="/admin/adminBanners/">
                      <a className="text-primary">Баннеры</a>
                    </Link>
                  </h5>
                  <p className="text-muted mb-0">Посмотреть\удалить</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  } else if (process.browser) {
    Router.push("/404");
    return (
      <div className="section border-top">
        <div className="container mt-100 mt-60">
          <div className="row justify-content-center"></div>
        </div>
      </div>
    );
  } else {
    return (
      <div className="section border-top">
        <div className="container mt-100 mt-60">
          <div className="row justify-content-center"></div>
        </div>
      </div>
    );
  }
};
export default withAlert()(AdminHome);
