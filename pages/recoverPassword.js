import React, { Component } from "react";
import { Home } from "react-feather";
import DefaultPic from "../assets/images/recovery-small.png";
import Router from "next/router";
import { withAlert } from "react-alert";
import { ValidationService, UserService } from "@services";

class RecoverPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
    };
    this.changeEmail = this.changeEmail.bind(this);
  }

  changeEmail({ target }) {
    this.setState({
      email: target.value,
    });
  }

  callSuccess(message) {
    alert = withAlert();
    this.props.alert.success(message);
  }

  callError(message) {
    alert = withAlert();
    this.props.alert.error(message);
  }

  sendEmail(e) {
    e.preventDefault();
    if (ValidationService.validateEmail(this.state.email)) {
      let emailAddress = { emailAddress: this.state.email };
      UserService.sendRecoveryEmail(emailAddress).then((response) => {
        if (response && response.status === 200) {
          this.callSuccess("Password reset steps are sent. Please, check your email.");
          Router.push("/login/");
        } else {
          this.callError("Error. Try again or reach out to support");
        }
      });
    } else {
      this.callError("Please, check email that you're using");
    }
  }

  render() {
    return (
      <section className="bg-home d-flex align-items-center">
        <div className="back-to-home rounded d-none d-sm-block">
          <a href="./" className="text-white title-dark rounded d-inline-block text-center">
            <Home className="fea icon-sm" />
          </a>
        </div>
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-7 col-md-6">
              <div className="mr-lg-5">
                <img src={DefaultPic} className="img-fluid d-block mx-auto" alt="" />
              </div>
            </div>
            <div className="col-lg-5 col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
              <div className="card login_page shadow rounded border-0">
                <div className="card-body">
                  <h4 className="card-title text-center">Password recovery</h4>

                  <form className="login-form mt-4">
                    <div className="row">
                      <div className="col-lg-12">
                        <p className="text-muted">
                          Please, enter email that you used during registrations. We will send you an email with steps to reset your password.
                        </p>
                        <div className="form-group position-relative">
                          <label>
                            Your E-mail <span className="text-danger">*</span>
                          </label>
                          <i data-feather="mail" className="fea icon-sm icons"/>
                          <input
                            type="email"
                            className="form-control pl-5"
                            placeholder="Your email"
                            name="email"
                            required=""
                            onChange={this.changeEmail}
                          />
                        </div>
                      </div>
                      <div className="col-lg-12">
                        <button
                          className="btn btn-primary btn-block"
                          onClick={(e) => {
                            this.sendEmail(e);
                          }}
                        >
                          Send
                        </button>
                      </div>
                      <div className="mx-auto">
                        <p className="mb-0 mt-3">
                          <small className="text-dark mr-2">Remembered your password ?</small>{" "}
                          <a href="/login" className="text-dark font-weight-bold">
                            Log in
                          </a>
                        </p>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default withAlert()(RecoverPassword);
