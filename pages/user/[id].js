import React, { useState, useEffect } from "react";
import { UserService, JobService, AdminService } from "@services";
import DH from "../../assets/images/avatars/default_avatar.png";
import { useRouter } from "next/router";
import {UtilService} from "../../service";

export default function UserPage({ user: serverUser }) {
  const [user, setUser] = useState(serverUser);
  const router = useRouter();
  const { id } = router.query;
  //   const [user, setUser] = useState({});

  useEffect(() => {
    async function load() {
      const response = await UserService.getUserPublicInfo(id);
      const user = await response.data;
      setUser(user);
    }

    if (!serverUser.id) {
      load();
    }
  }, [router]);

  //   function getUser() {
  //     UserService.getUserPublicInfo(id).then((response) => {
  //       setUser(response.data);
  //     });
  //   }

  function printAvatar() {
    if (user.avatar) {
      return <img src={user.avatar} className="avatar avatar-large rounded-circle shadow d-block mx-auto" alt="" />;
    } else return <img src={DH} className="avatar avatar-large rounded-circle shadow d-block mx-auto" alt="" />;
  }

  function printLastReviews() {
    if (user.lastFiveReviews && user.lastFiveReviews.length > 0) {
      return (
        <div className="col-lg-6 mt-4 pt-2 pt-sm-0">
          <h5 className="widget-title">Последние 5 отзывов :</h5>
          {user.lastFiveReviews.map((review) => (
            <div key={review.id} className="media key-feature align-items-center p-3 rounded shadow mt-4">
              <img src={DH} className="avatar avatar-ex-sm" alt="" />
              <div className="media-body content ml-3">
                <h4 className="title mb-0">{review.jobType}</h4>
                <p className="text-muted mb-0">Оценка: {review.average}</p>
                <p className="text-muted mb-0">Дата: {UtilService.printDate(review.creationDate)}</p>
              </div>
            </div>
          ))}
        </div>
      );
    }
  }

  function printLastJob() {
    if (user.lastJob) {
      let job = user.lastJob;
      return (
        <div className="col-lg-6 mt-4">
          <h5 className="widget-title">Последняя заявка :</h5>
          <div className="mt-4">
            <div className="media align-items-center">
              <i data-feather="mail" className="fea icon-ex-md text-muted mr-3"></i>
              <div className="media-body">
                <h5 className="text-primary mb-0">Категория :</h5>
                <p className="text-muted">{job.categoryName}</p>
              </div>
            </div>
            <div className="media align-items-center mt-3">
              <i data-feather="bookmark" className="fea icon-ex-md text-muted mr-3"></i>
              <div className="media-body">
                <h5 className="text-primary mb-0">Город :</h5>
                <p className="text-muted">{job.city.name}</p>
              </div>
            </div>
            <div className="media align-items-center mt-3">
              <i data-feather="italic" className="fea icon-ex-md text-muted mr-3"></i>
              <div className="media-body">
                <h5 className="text-primary mb-0">Дата создания :</h5>
                <p className="text-muted">{UtilService.printDate(job.creationDate)}</p>
              </div>
            </div>
            <div className="media align-items-center mt-3">
              <i data-feather="globe" className="fea icon-ex-md text-muted mr-3"></i>
              <div className="media-body">
                <h5 className="text-primary mb-0">Статус :</h5>
                <p className="text-muted">{JobService.getReadableStatus(job.statusType)}</p>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }

  function printEmailIfAdmin() {
    if (AdminService.checkIfAdmin())
      return (
        <div className="col-6 text-center marginTop20px">
          <i data-feather="user-plus" className="fea icon-ex-md text-primary mb-1"></i>
          <h5 className="mb-0">{user.email}</h5>
          <p className="text-muted mb-0">E-mail</p>
        </div>
      );
  }

  return (
    <>
      <section className="section mt-60">
        <div className="container mt-lg-3">
          <div className="row">
            <div className="col-lg-12">
              <div className="card public-profile border-0 rounded shadow">
                <div className="card-body">
                  <div className="row align-items-center">
                    <div className="col-lg-2 col-md-3 text-md-left text-center">{printAvatar()}</div>
                    <div className="col-lg-10 col-md-9">
                      <div className="row align-items-end">
                        <div className="col-md-7 text-md-left text-center mt-4 mt-sm-0">
                          <h3 className="title mb-0">{user.fullName}</h3>
                          <small className="text-muted mb-0">Пользователь</small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="container mt-lg-3">
            <div className="row">
              <div className="col-lg-4 col-md-5 col-12">
                <div className="sidebar sticky-bar p-4 rounded shadow">
                  <div className="widget mt-4 pt-2">
                    <h5 className="widget-title">Показатели :</h5>
                    <div className="row mt-4">
                      <div className="col-6 text-center">
                        <i data-feather="user-plus" className="fea icon-ex-md text-primary mb-1"></i>
                        <h5 className="mb-0">{user.jobCount}</h5>
                        <p className="text-muted mb-0">Количество заявок</p>
                      </div>
                      <div className="col-6 text-center">
                        <i data-feather="user-plus" className="fea icon-ex-md text-primary mb-1"></i>
                        <h5 className="mb-0">{user.averageRate}</h5>
                        <p className="text-muted mb-0">Средний отзыв</p>
                      </div>
                      {printEmailIfAdmin()}
                      <div className="col-6 text-center marginTop20px">
                        <i data-feather="users" className="fea icon-ex-md text-primary mb-1"></i>
                        <h5 className="mb-0">{UtilService.printDate(user.registrationDate)}</h5>
                        <p className="text-muted mb-0">Дата регистрации</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-8 col-md-7 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <div className="border-bottom pb-4">
                  <div className="row">
                    {printLastJob()}
                    {printLastReviews()}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export async function getServerSideProps({ query, req }) {
  if (!req) {
    return { user: null };
  }

  const response = await UserService.getUserPublicInfo(query.id);
  let user = {};
  if (response) {
    user = await response.data;
  }

  return {
    props: { user },
  };
}
