import React from "react";
import image1 from "../assets/images/404.png";

export default function Error404() {
  return (
    <section className="bg-home d-flex align-items-center">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-8 col-md-12 text-center">
            <img src={image1} className="img-fluid" alt="404" />
            <div className="text-uppercase mt-4 display-3">Ooooppsss</div>
            <div className="text-capitalize text-dark mb-4 error-page">Page not found</div>
            <p className="text-muted para-desc mx-auto"> Something went wrong, this page doesn't exist! </p>
          </div>
        </div>

        <div className="row">
          <div className="col-md-12 text-center">
            <a href="/" className="btn btn-primary mt-4 ml-2">
              Home
            </a>
          </div>
        </div>
      </div>
    </section>
  );
}