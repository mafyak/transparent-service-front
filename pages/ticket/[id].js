import React, { useState, useEffect } from "react";
import { TicketService, AdminService } from "@services";
import AvaAdmin from "../../assets/images/avatars/ava-admin.jpg";
import { useRouter } from "next/router";
import { withAlert } from "react-alert";

const Ticket = ({ alert }) => {
  const router = useRouter();
  const { id } = router.query;
  const [ticket, setTicket] = useState({});
  const [comments, setComments] = useState("");
  const [responseMessage, setResponseMessage] = useState("");

  useEffect(() => {
    async function loadTicket() {
      const response = await TicketService.getTicketById(id);
      if (response) {
        setTicket(response.data);
        setComments(response.data.comments);
      }
    }
    if (!ticket.id && id) {
      loadTicket();
    }
  }, [router]);

  function handleResponseMessage({ target }) {
    setResponseMessage(target.value);
  }

  function printAvatar(user) {
    if (user.type === "ADMIN") {
      return <img src={AvaAdmin} className="img-fluid avatar avatar-small rounded-circle shadow" alt="user avatar" />;
    } else {
      return <img src="https://rmt.by/img/14/default_avatar.png" className="img-fluid avatar avatar-small rounded-circle shadow" alt="user avatar" />;
    }
  }

  function printComments() {
    if (comments && comments.length > 0) {
      return comments.map((comment) => (
        <li key={comment.id} className="mt-4">
          <div className="d-flex justify-content-between">
            <div className="media align-items-center">
              <a className="pr-3" href={"/user/" + comment.author.id}>
                {printAvatar(comment.author)}
              </a>
              <div className="commentor-detail">
                <h6 className="mb-0">
                  <a href={"/user/" + comment.author.id} target="_blank" className="text-dark media-heading bold">
                    {comment.author.fullName}
                  </a>
                </h6>
              </div>
            </div>
            <div className="mt-3 width70p">
              <p className="text-muted font-italic p-3 bg-light rounded">{comment.content}</p>
            </div>
          </div>
        </li>
      ));
    }
  }

  function addResponseMessage() {
    let comment = { content: responseMessage };
    TicketService.addComment(id, comment).then(({ data }) => {
      setTicket(data);
      setComments(data.comments);
      setResponseMessage("");
    });
  }

  function closeTicketButton() {
    if (AdminService.checkIfAdmin()) {
      return (
        <button className="btn btn-info mt-2 mr-2" onClick={() => closeTicket()}>
          Закрыть
        </button>
      );
    }
  }

  function closeTicket() {
    TicketService.closeTicket(ticket.id).then(({ data }) => {
      setTicket(data);
      setComments(data.comments);
    });
  }

  function addComment() {
    return (
      <li className="mt-4">
        <div className="d-flex align-items-center py-2">
          <label className="font-weight-bold">Ответить:</label>
        </div>
        <div className="d-flex flex-wrap">
          <div className="input-group">
            <input
              className="form-control fontsize_15rem"
              placeholder="Сообщение"
              value={responseMessage}
              onChange={handleResponseMessage}
              minLength="5"
            />
            <div className="input-group-append">
              <button className="btn btn-primary" onClick={() => addResponseMessage()}>
                Отправить
              </button>
            </div>
          </div>
        </div>
      </li>
    );
  }

  function printStatus(status) {
    let newStatus = "";
    if (status == "NEW") {
      newStatus = "Новый";
    } else if (status == "PENDING") {
      newStatus = "В обработке";
    } else if (status == "CLOSED") {
      newStatus = "Закрыт";
    }
    return <label>{newStatus}</label>;
  }

  function printTicketId() {
    if (ticket) {
      return <h1 className="heading">{"Вопрос #" + ticket.id}</h1>;
    } else {
      return <h1 className="heading">Вопрос #...</h1>;
    }
  }

  if (typeof window !== "undefined" && localStorage.getItem("logged in") && ticket.id) {
    return (
      <section className="padding150 section">
        <div className="container">
          {printTicketId()}
          <div className="component-wrapper rounded shadow border pt-5 pl-5 pb-5 pr-5">
            <div className="d-flex align-items-center py-2">
              <label className="font-weight-bold pr-3">Тема: </label>
              <label>{ticket.topic}</label>
            </div>
            <div className="d-flex align-items-center py-2">
              <label className="font-weight-bold pr-3">Статус: </label>
              {printStatus(ticket.status)}
            </div>
            <div className="d-flex align-items-center py-2">
              <label className="font-weight-bold pr-3">Дата создания: </label>
              <label>{ticket.creationDate}</label>
            </div>
            <ul className="media-list list-unstyled mb-0">
              {printComments()}
              {addComment()}
              <li className="mt-4">
                {closeTicketButton()}
                <div className="mt-3">
                  <p className="text-muted font-italic p-3 bg-light rounded">
                    " Послание от RMT.by: Среднее время ответа администратора - один день. "
                  </p>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </section>
    );
  } else {
    return <></>;
  }
};

export default withAlert()(Ticket);
