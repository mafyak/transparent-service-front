import {useRouter} from "next/router";
import Router from 'next/router'
import {AdminService} from "../../service";
import { withAlert } from "react-alert";

const PinterestOauth = ({ alert }) => {
  const router = useRouter();
  if (typeof window !== "undefined") {
    let search = window.location.search;
    let params = new URLSearchParams(search);
    let code = params.get('code');
    AdminService.savePinterestCode(code)
      .then((res) => alert.success("Pinterest code saved"));
    console.log("value: " + localStorage.getItem("logged in"))
    if (code && (localStorage.getItem("logged in") === null || "true" !== localStorage.getItem("logged in"))) {
      const bcrypt = require("bcryptjs");
      const salt = bcrypt.genSaltSync(10);
      const encodedType = bcrypt.hashSync("PINTEREST", salt);
      localStorage.setItem("type", encodedType);
      localStorage.setItem("logged in", "true");
      localStorage.setItem("fullName", "Pinterest user");
    }
    Router.push('/');
  }
  return (<></>)
}

export default withAlert()(PinterestOauth);