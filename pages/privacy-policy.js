import React from "react";
import infoPic from "../assets/images/SEO_SVG.svg";
import StaticContentService from "../service/StaticContentService";
import Head from "next/head";

const PrivacyPolicy = ({siteName}) => {

  return (
    <>
    <Head>
      <title>Privacy Policy</title>
      <meta property="og:title" content="Privacy Policy"/>
      <meta name="description" content="Privacy Policy"/>
      <meta property="og:description" content="Privacy Policy"/>
    </Head>
    <section className="section">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-12 text-center">
            <div className="section-title mb-4 pb-2">
              <h4 className="title mb-4">Privacy Policy</h4>
            </div>
          </div>
        </div>

        <BlockOne siteName={siteName}/>

      </div>
    </section>
    </>
  );
}

const BlockOne = ({siteName}) => {
  return (
    <div className="row align-items-center">
      <div className="col-lg-5 col-md-6 mt-4 pt-2">
        <img src={infoPic} alt=""/>
      </div>

      <div className="col-lg-7 col-md-6 mt-4 pt-2">
        <div className="section-title ml-lg-5">
          <p className="text-muted">
            Effective Date: Jan 2023
          </p>
          <p className="text-muted">
            At {siteName}.com, we value your privacy and are committed to protecting your personal information. This
            Privacy Policy outlines how we collect, use, and safeguard the information you provide when you visit our
            construction blog website (the "Website"). Please read this policy carefully to understand our practices
            regarding your personal information and how we will treat it.
          </p>
          <p className="text-muted">
            1. Information We Collect
          </p>
          <p className="text-muted">
            1.1 Personal Information: When you access or use our Website, we may collect certain personal information
            from you, such as your name, email address, and any other information you voluntarily provide when
            submitting comments or contacting us through our contact forms.
          </p>
          <p className="text-muted">
            1.2 Log Data: Like many other websites, we collect information that your browser sends whenever you visit
            our Website. This may include your IP address, browser type, the pages you visit on our Website, the time
            and date of your visit, the time spent on those pages, and other statistics.
          </p>
          <p className="text-muted">
            2. Use of Information
          </p>
          <p className="text-muted">
            2.1 We may use the information we collect from you for various purposes, including:
          </p>
          <ul className="feature-list text-muted">
            <li>
              <i data-feather="check-circle" className="fea icon-sm text-success mr-2"/>
              Providing and maintaining the Website
            </li>
            <li>
              <i data-feather="check-circle" className="fea icon-sm text-success mr-2"/>
              Responding to your inquiries, comments, or requests
            </li>
            <li>
              <i data-feather="check-circle" className="fea icon-sm text-success mr-2"/>
              Improving our content, products, and services
            </li>
            <li>
              <i data-feather="check-circle" className="fea icon-sm text-success mr-2"/>
              Sending you newsletters, updates, or promotional materials related to our Website (with your consent)
            </li>
            <li>
              <i data-feather="check-circle" className="fea icon-sm text-success mr-2"/>
              Monitoring and analyzing usage of the Website
            </li>
            <li>
              <i data-feather="check-circle" className="fea icon-sm text-success mr-2"/>
              Detecting, preventing, and addressing technical issues or security breaches
            </li>
          </ul>

          <p className="text-muted">
            2.2 We will not sell, rent, or lease your personal information to any third parties unless we have obtained
            your consent or are required by law to do so.
          </p>
          <p className="text-muted">
            3. Cookies and Tracking Technologies
          </p>
          <p className="text-muted">
            3.1 Our Website may use cookies and similar tracking technologies to enhance your experience and collect
            information. Cookies are small files stored on your device that help us analyze website traffic and
            recognize your preferences.
          </p>
          <p className="text-muted">
            3.2 You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However,
            if you choose to disable cookies, you may not be able to access certain features of our Website.
          </p>
          <p className="text-muted">
            4. Third-Party Links
          </p>
          <p className="text-muted">
            4.1 Our Website may contain links to third-party websites or services. We are not responsible for the
            privacy practices or the content of such websites. We encourage you to read the privacy policies of those
            third-party websites before providing any personal information.
          </p>
          <p className="text-muted">
            5. Data Security
          </p>
          <p className="text-muted">
            5.1 We take reasonable precautions to protect your personal information and maintain the security of our
            Website. However, no method of transmission over the internet or electronic storage is completely secure,
            and we cannot guarantee absolute security.
          </p>
          <p className="text-muted">
            6. Children's Privacy
          </p>
          <p className="text-muted">
            6.1 Our Website is not directed towards individuals under the age of 13. We do not knowingly collect
            personal information from children under 13. If you believe that we may have collected personal information
            from a child under 13, please contact us, and we will take appropriate steps to remove such information from
            our records.
          </p>
          <p className="text-muted">
            7. Changes to this Privacy Policy
          </p>
          <p className="text-muted">
            7.1 We may update our Privacy Policy from time to time. Any changes will be posted on this page with an
            updated effective date. We encourage you to review this Privacy Policy periodically for any updates.
          </p>
          <p className="text-muted">
            8. Contact Us
          </p>
          <p className="text-muted">
            8.1 If you have any questions or concerns about this Privacy Policy or our privacy practices, please contact
            us at {siteName}@gmail.com.
          </p>
          <p className="text-muted">
            By using our Website, you signify your acceptance of this Privacy Policy. If you do not agree with this
            Policy, please do not use our Website.
          </p>

        </div>
      </div>
    </div>
  )
}

export async function getStaticProps() {

  const siteName = await StaticContentService.getSiteName();

  return {
    props: {
      siteName: siteName.data
    },
  };
}

export default PrivacyPolicy;
