import React, { Component } from "react";
import SubscriptionService from "../../../../service/SubscriptionService";
import MasterService from "../../../../service/MasterService";
import Link from "next/link";
import Router from "next/router";
import ReactGA from "react-ga4";

class OurPrices extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subscriptions: [],
      isMonth: true,
      isYear: false,
      timeFrame: 1,
      duration: "/в мес",
    };
  }
  componentDidMount() {
    SubscriptionService.retrieveAllSubscriptions().then((response) => {
      this.setState({
        subscriptions: response.data,
      });
    });
  }

  printSubscriptions() {
    return this.state.subscriptions
      .filter((subscription) => subscription.duration == this.state.timeFrame || subscription.subscriptionType == "ZERO")
      .map((subscription) => (
        <div className="col-lg-3 col-md-6 col-12 mt-4 pt-2" key={subscription.id}>
          <div className="card pricing-rates business-rate shadow bg-light border-0 rounded">
            <div className="card-body">
              <h2 className="title text-uppercase mb-4">{subscription.name}</h2>
              <div className="d-flex mb-4">
                <span className="h4 mb-0 mt-2">BYN</span>
                <span className="price h1 mb-0">{subscription.price}</span>
                <span className="h4 align-self-end mb-1">{this.state.duration}</span>
              </div>

              <ul className="feature list-unstyled pl-0">
                {subscription.bulletPoint.map((point) => (
                  <li className="feature-list text-muted" key={point}>
                    <i data-feather="check" className="fea icon-sm text-success mr-2"></i>
                    {point}
                  </li>
                ))}
              </ul>
              {this.printAcceptButton(subscription)}
            </div>
          </div>
        </div>
      ));
  }

  callRedirect(subscription) {
    localStorage.setItem("subscription", JSON.stringify(subscription));
    Router.push({
      pathname: "/new-invoice/",
    });
  }

  printAcceptButton(subscription) {
    if (MasterService.checkIfMaster()) {
      return (
        <a className="btn btn-primary mt-4" onClick={() => this.callRedirect(subscription)}>
          Применить
        </a>
      );
    } else {
      return (
        <Link href="/register">
          <a onClick={() => ReactGA.pageview("/register")} className="btn btn-primary mt-4">
            Регистрация
          </a>
        </Link>
      );
    }
  }

  setMonthActive(e) {
    e.preventDefault();
    this.setState({
      isMonth: true,
      isYear: false,
      timeFrame: 1,
      duration: "/в мес",
    });
  }

  setYearActive(e) {
    e.preventDefault();
    this.setState({
      isMonth: false,
      isYear: true,
      timeFrame: 12,
      duration: "/в год",
    });
  }

  printSubscriptionsBlock() {
    return (
      <div className="row align-items-center">
        <div className="col-12 mt-4 pt-2">
          <div className="d-flex justify-content-center">
            <ul className="nav nav-pills nav-fill justify-content-center w-50 border" id="pills-tab" role="tablist">
              <li className="nav-item d-inline-block w-50">
                <a
                  className={"nav-link px-3 rounded-pill monthly" + (this.state.isMonth ? " active" : "")}
                  id="Monthly"
                  data-toggle="pill"
                  href="#Month"
                  role="tab"
                  aria-controls="Month"
                  aria-selected="true"
                  onClick={(e) => this.setMonthActive(e)}
                >
                  На месяц
                </a>
              </li>
              <li className="nav-item d-inline-block w-50">
                <a
                  className={"nav-link px-3 rounded-pill yearly" + (this.state.isYear ? " active" : "")}
                  id="Yearly"
                  data-toggle="pill"
                  href="#Year"
                  role="tab"
                  aria-controls="Year"
                  aria-selected="false"
                  onClick={(e) => this.setYearActive(e)}
                >
                  На год <span className="badge badge-pill badge-success">15% Скидка </span>
                </a>
              </li>
            </ul>
          </div>

          <div className="tab-content" id="pills-tabContent">
            <div className="tab-pane fade active show" id="Month" role="tabpanel" aria-labelledby="Monthly">
              <div className="row">{this.printSubscriptions()}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="container mt-100 mt-60" id="our-prices">
        <div className="row justify-content-center">
          <div className="col-12 text-center">
            <div className="section-title mb-4 pb-2">
              <h4 className="title mb-4">Наши цены</h4>
              <p className="text-muted para-desc mb-0 mx-auto">
                Мы предлагаем <span className="text-primary font-weight-bold">4 вида подписки </span>
                для компаний. Чем выше уровень подписки, тем больше клиентов сайта будет видеть вашу компанию в результатах поиска при условии
                хорошего рейтинга.
              </p>
            </div>
          </div>
        </div>
        <div className="row">{this.printSubscriptionsBlock()}</div>
        <p className="feature-list text-muted">
          <span className="text-danger">*</span>VIP Отображение в списке компаний - компания отображается выше других компаний в списке компаний.
        </p>
      </div>
    );
  }
}
export default OurPrices;
