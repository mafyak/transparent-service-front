import React from "react";
import dealPic from "../../../../assets/images/deal-hend.png";
import Link from "next/link";
import MasterService from "../../../../service/MasterService";
import ReactGA from "react-ga4";

const OurPrinciples = () => {
  function printButton() {
    if (MasterService.checkIfMaster()) {
      return;
    } else {
      return (
        <Link href="/register">
          <a onClick={() => ReactGA.pageview("/register")} className="btn btn-primary mt-3">
            Зарегистрироваться <i className="mdi mdi-chevron-right"></i>
          </a>
        </Link>
      );
    }
  }

  return (
    <div className="container mt-100 mt-60">
      <div className="row align-items-center">
        <div className="col-lg-6 col-md-6 order-2 order-md-1 mt-4 mt-sm-0 pt-2 pt-sm-0">
          <div className="section-title">
            <h4 className="title mb-4">Предлагайте цены через наш сайт.</h4>
            <p className="text-muted">
              Наш портал предоставляет вам возможность предложить цену заказчику прямо у нас на сайте. Заполните поля работы и клиент сразу сможет их
              увидеть. Для получения большего числа заявок, вам необходимо указать все категории услуг, которые предоставляет ваша компания.
            </p>
            <ul className="feature-list text-muted">
              <p className=" bold_text">Преимущества:</p>
              <li>
                <i data-feather="check-circle" className="fea icon-sm text-success mr-2"></i>
                Упрощённая система выбора задач для работы
              </li>
              <li>
                <i data-feather="check-circle" className="fea icon-sm text-success mr-2"></i>
                Оповещение клиента о поступлении вашего ценового предложения
              </li>
              <li>
                <i data-feather="check-circle" className="fea icon-sm text-success mr-2"></i>
                Получайте заказы только в указанных вами категориях
              </li>
            </ul>
            {printButton()}
          </div>
        </div>

        <div className="col-lg-6 col-md-6 order-1 order-md-2">
          <img src={dealPic} className="img-fluid" alt="" />
        </div>
      </div>
    </div>
  );
};
export default OurPrinciples;
