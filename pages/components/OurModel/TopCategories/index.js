import React  from "react";

const TopCategories =  () => {
    return(
        <section className="section">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-12 text-center">
                <div className="section-title mb-4 pb-2">
                  <h4 className="title mb-4">Самые популярные категории</h4>
                  <p className="text-muted para-desc mx-auto mb-0">
                    Топ самых востребованных услуг на нашем сайте:
                  </p>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-4 col-md-6 mt-4 pt-2">
                <div className="media key-feature align-items-center p-3 rounded shadow">
                  <div className="icon text-center rounded-circle mr-3"></div>
                  <div className="media-body">
                    <h4 className="title mb-0">Услуга 1</h4>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 mt-4 pt-2">
                <div className="media key-feature align-items-center p-3 rounded shadow">
                  <div className="icon text-center rounded-circle mr-3"></div>
                  <div className="media-body">
                    <h4 className="title mb-0">Услуга 2</h4>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 mt-4 pt-2">
                <div className="media key-feature align-items-center p-3 rounded shadow">
                  <div className="icon text-center rounded-circle mr-3"></div>
                  <div className="media-body">
                    <h4 className="title mb-0">Услуга 3</h4>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 mt-4 pt-2">
                <div className="media key-feature align-items-center p-3 rounded shadow">
                  <div className="icon text-center rounded-circle mr-3"></div>
                  <div className="media-body">
                    <h4 className="title mb-0">Услуга 4</h4>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 mt-4 pt-2">
                <div className="media key-feature align-items-center p-3 rounded shadow">
                  <div className="icon text-center rounded-circle mr-3"></div>
                  <div className="media-body">
                    <h4 className="title mb-0">Услуга 5</h4>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 mt-4 pt-2">
                <div className="media key-feature align-items-center p-3 rounded shadow">
                  <div className="icon text-center rounded-circle mr-3"></div>
                  <div className="media-body">
                    <h4 className="title mb-0">Услуга 6</h4>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 mt-4 pt-2">
                <div className="media key-feature align-items-center p-3 rounded shadow">
                  <div className="icon text-center rounded-circle mr-3"></div>
                  <div className="media-body">
                    <h4 className="title mb-0">Услуга 7</h4>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 mt-4 pt-2">
                <div className="media key-feature align-items-center p-3 rounded shadow">
                  <div className="icon text-center rounded-circle mr-3"></div>
                  <div className="media-body">
                    <h4 className="title mb-0">Услуга 8</h4>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 mt-4 pt-2">
                <div className="media key-feature align-items-center p-3 rounded shadow">
                  <div className="icon text-center rounded-circle mr-3"></div>
                  <div className="media-body">
                    <h4 className="title mb-0">Услуга 9</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    )
}
export default TopCategories