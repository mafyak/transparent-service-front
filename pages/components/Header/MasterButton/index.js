import React from "react";
import Link from "next/link";
import ReactGA from "react-ga4";

export default function MasterButton(props) {
  const { isMaster, closeMenu } = props;

  if (isMaster) {
    return (
      <li className="m-0 d-flex">
        <Link href="/myCompany">
          <a
            onClick={() => {
              ReactGA.pageview("/myCompany");
              closeMenu();
            }}
            className="text-center align-self-center"
          >
            Моя компания
          </a>
        </Link>
      </li>
    );
  } else {
    return (
      <>
        <li className="m-0 d-flex">
          <Link href="/our-model">
            <a
              onClick={() => {
                ReactGA.pageview("/our-model");
                closeMenu();
              }}
              className="text-center align-self-center"
            >
              Исполнителям
            </a>
          </Link>
        </li>
        <li className="m-0 d-flex">
          <Link href="/findMaster">
            <a
              onClick={() => {
                ReactGA.pageview("/findMaster");
                closeMenu();
              }}
              className="text-center align-self-center"
            >
              Найти Компанию
            </a>
          </Link>
        </li>
      </>
    );
  }
}
