import React from "react";

export default function TopHeaderForName({ userName }) {
  if (userName) {
    return (
      <div>
        <div className="d-block bg-light shadow">
          <div className="text-monospace d-flex justify-content-end container">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="20"
              height="20"
              viewBox="0 0 24 24"
              fill="none"
              stroke="currentColor"
              strokeWidth="1"
              strokeLinecap="round"
              strokeLinejoin="round"
              className="feather feather-user"
            >
              <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2" />
              <circle cx="12" cy="7" r="4" />
            </svg>
            <span className="d-block px-3">{userName}</span>
          </div>
        </div>
      </div>
    );
  } else {
    return <></>;
  }
}
