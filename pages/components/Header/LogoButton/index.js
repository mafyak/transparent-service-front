import React from "react";
import Link from "next/link";
import ReactGA from "react-ga4";

const LogoButton = () => {

  return (
    <div>
      <Link href="/">
        <a onClick={() => ReactGA.send({hitType: "pageview", page: "/", title: "Home"})} className="logo flexing">
          <img src="/img/default/logo.png" alt="logo" className="company-logo"/>
          <span className="logo-responsive">{process.env.NEXT_PUBLIC_SITE_NAME}<span className="text-primary">.com</span></span>
        </a>
      </Link>
    </div>
  );
}

export default LogoButton;