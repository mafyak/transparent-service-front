import React from "react";

const AdminButton = ({text, path}) => {
    return(
        <div className="buy-button">
          <a href={path} className="btn btn-primary"> {text} </a>
        </div>
    )

}
export default AdminButton;