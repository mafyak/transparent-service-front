import React from "react";

export default function LogInLogOut(props) {
  const { isLoggedIn, isPreload, closeMenu } = props;

  function logOut() {
    if (typeof window !== "undefined") {
      let categoryData = localStorage.getItem("categoryData");
      localStorage.clear();
      localStorage.setItem("categoryData", categoryData);
    }
  }

  if (isLoggedIn) {
    return (
      <div className="mr-3 align-self-center" style={{ lineHeight: "68px", padding: " 3px 0" }}>
        <a
          href="/"
          className="btn btn-outline-primary"
          onClick={() => {
            closeMenu();
            logOut();
          }}
          style={isPreload ? { color: "#ffffff" } : {}}
        >
          Log out
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20"
            height="20"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
            className="feather feather-log-out pl-2 pb-1"
          >
            <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4" />
            <polyline points="16 17 21 12 16 7" />
            <line x1="21" y1="12" x2="9" y2="12" />
          </svg>
        </a>
      </div>
    );
  } else {
    return (
      <div className="mr-3 align-self-center" style={{ lineHeight: "68px", padding: " 3px 0" }}>
        <a href="/login" className="btn btn-outline-primary" style={isPreload ? { color: "#ffffff !important" } : {}}>
          Log in
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20"
            height="20"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
            className="feather feather-log-out pl-2 pb-1"
          >
            <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4" />
            <polyline points="16 17 21 12 16 7" />
            <line x1="21" y1="12" x2="9" y2="12" />
          </svg>
        </a>
      </div>
    );
  }
}
