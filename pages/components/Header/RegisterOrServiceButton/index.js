import React from "react";
import Link from "next/link";
import ReactGA from "react-ga4";

export default function RegisterOrServiceButton(props) {
  const { isLoggedIn, isPreload, isAdmin, closeMenu } = props;

  if (isLoggedIn && isAdmin) {
    return <></>;
  } else if (isLoggedIn) {
    return (
      <div style={{ lineHeight: "68px", padding: "3px 0" }} className="align-self-center">
        <Link href="/jobs">
          <a
            onClick={() => {
              ReactGA.send({ hitType: "pageview", page: "/jobs", title: "Jobs" });
              closeMenu();
            }}
            style={isPreload ? { color: "#2f55d4 !important" } : {}}
            className="btn btn-primary mr-3"
          >
            Мои заявки
          </a>
        </Link>
      </div>
    );
  } else {
    return (
      <div style={{ lineHeight: "68px", padding: "3px 0" }} className="align-self-center">
        <Link href="/register">
          <a
            onClick={() => {
              ReactGA.send({ hitType: "pageview", page: "/register", title: "Register" });
              closeMenu();
            }}
            style={isPreload ? { color: "#2f55d4 !important" } : {}}
            className="btn btn-primary mr-3"
          >
            Регистрация
          </a>
        </Link>
      </div>
    );
  }
}
