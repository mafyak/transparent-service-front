import React, {useState, useEffect} from "react";
import LogInLogOut from "./LogInLogOut";
import LogoButton from "./LogoButton";
import SettingsButton from "./SettingsButton";
import {Search} from "react-feather";
import MasterButton from "./MasterButton";
import RegisterOrServiceButton from "./RegisterOrServiceButton";
import TopHeaderForName from "./TopHeaderForName";
import ReactGA from "react-ga4";
import Link from "next/link";
import Router from "next/router";

export default function Header() {
  const [userName, setUserName] = useState(null);
  const [isPreload, setIsPreload] = useState(true);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [isTop, setIsTop] = useState(true);
  const [type, setType] = useState("");
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  function handleScroll() {
    if (window) {
      const isTopNow = window.scrollY < 100;
      if (isTopNow !== isTop) {
        onScroll(isTop);
      }
    }
  }

  useEffect(() => {
    setType(localStorage.getItem("type"));
    setUserName(localStorage.getItem("fullName"));
    setIsLoggedIn(localStorage.getItem("logged in"));
    setIsPreload(false);
    document.addEventListener("scroll", handleScroll);

    return () => {
      document.removeEventListener("scroll", handleScroll);
    };
  }, []);

  function onScroll(isTop) {
    setIsTop(isTop);
  }

  function handleMenu() {
    setIsMenuOpen(!isMenuOpen);
  }

  function closeMenu() {
    if (isMenuOpen) {
      setIsMenuOpen(false);
    }
  }

  return (
    <header id="topnav" className={isTop ? "bg-white defaultscroll sticky" : "scroll"}>
      {/*<MetaTags>*/}
      {/*  <title>Home DIY portal. Find affordable contractor helper. Home advisor for renovation, remodel. Hardwood flooring services advises.</title>*/}
      {/*  <meta name="description" content="Home DIY portal. Find affordable contractor helper. Home advisor for renovation, remodel. Hardwood flooring services advises." />*/}
      {/*  <meta property="og:description" content="Home DIY portal. Find affordable contractor helper. Home advisor for renovation, remodel. Hardwood flooring services advises." />*/}
      {/*  <meta property="og:title" content="Home DIY portal. Find affordable contractor helper. Home advisor for renovation, remodel. Hardwood flooring services advises." />*/}
      {/*</MetaTags>*/}
      <TopHeaderForName userName={userName}/>
      <div className="container">
        <LogoButton/>
        <div id="navigation" className={isMenuOpen ? "d-inline-block" : "d-none"}
             style={isLoggedIn ? {top: "97px"} : {top: "70px"}}>
          <ul className="navigation-menu" style={{justifyContent: "flex-end"}}>
            <SearchBar/>
            <li className="m-0 d-flex">
              <Link href="/">
                <a
                  onClick={() => {
                    ReactGA.send({hitType: "pageview", page: "/", title: "Home Page"});
                    closeMenu();
                  }}
                  className="text-center align-self-center"
                >
                  Main
                </a>
              </Link>
            </li>
            <li className="m-0 d-flex">
              <Link href={"/blog/category/" + process.env.NEXT_PUBLIC_DEFAULT_BLOG}>
                <a
                  onClick={() => {
                    ReactGA.send({
                      hitType: "pageview",
                      page: "/blog/category/" + process.env.NEXT_PUBLIC_DEFAULT_BLOG,
                      title: "Flooring Blog"
                    });
                    closeMenu();
                  }}
                  className="text-center align-self-center"
                >
                  Blog
                </a>
              </Link>
            </li>
            {/* //            <li className="m-0 d-flex">
//              <Link href="/companies/">
//                <a
//                  onClick={() => {
//                    ReactGA.pageview("/companies");
//                    closeMenu();
//                  }}
//                  className="text-center align-self-center"
//                >
//                  Все компании
//                </a>
//              </Link>
//            </li>
            <MasterButton isMaster={isType("MASTER")} closeMenu={closeMenu} />
            <li className="d-flex pl-4 m-0">
              <SettingsButton isLoggedIn={isLoggedIn} isPreload={isPreload} closeMenu={closeMenu} isAdmin={isType("ADMIN")} />
              <RegisterOrServiceButton isAdmin={isType("ADMIN")} isLoggedIn={isLoggedIn} closeMenu={closeMenu} isPreload={isPreload} />
            </li> */}
            <li className="d-flex pl-4 m-0">
              <SettingsButton isLoggedIn={isLoggedIn} isPreload={isPreload} closeMenu={closeMenu} userType={type}/>
              <LogInLogOut isLoggedIn={isLoggedIn} isPreload={isPreload} closeMenu={closeMenu}/>
            </li>
          </ul>
        </div>
      </div>
      <div className="menu-extras container">
        <div className="menu-item">
          <a className={isMenuOpen ? "navbar-toggle open" : "navbar-toggle close"} onClick={handleMenu}>
            <div className="lines">
              <span></span>
              <span></span>
              <span></span>
            </div>
          </a>
        </div>
      </div>
    </header>
  );
}

const SearchBar = () => {

  const [search, setSearch] = useState("");

  useEffect(() => {
  }, []);

  // todo prevent from re-render

  function updateSearch({target}) {
    setSearch(target.value);
  }

  function clickMe(e) {
    e.preventDefault();
    if (search) {
      Router.push({
        pathname: "/blog/search",
        query: {q: search},
      });
    }
  }

  return (
    <li className="m-0 d-flex">
      <span className="text-center align-self-center">
        <div className="search-bar p-0 d-none d-md-block ms-2">
          <div id="search" className="menu-search mb-0">
            <form role="search" method="get" id="searchform" className="searchform">
              <div>
                <Search className="fea icon-sm icons lens-icon-position"/>
                <input type="text" className="form-control border rounded search-blog-input" name="s" id="s"
                       placeholder="Search Article..." onChange={updateSearch}/>
                <input className="search-blog-lens" onClick={clickMe} type="submit" id="searchsubmit" value="Search"/>
              </div>
            </form>
          </div>
        </div>
      </span>

    </li>
  )
}