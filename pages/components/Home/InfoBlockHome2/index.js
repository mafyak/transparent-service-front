import React from "react";

const InfoBlockHome2 = ({textData}) => {

  if (!textData) {
    return <></>
  }

  return (
    <div className="container mt-100 mb-5">
      <div className="row align-items-center">
        <div className="col-lg-7 col-md-6">
          <img
            src="img/default/home-middle.jpg"
            alt="Be in the known"
            className="up-to-400-height"
          />
        </div>
        <div className="col-lg-5 col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
          <div className="section-title ml-lg-5 mx-4">
            <h4 className="title mb-4">{textData.header}</h4>
            <p className="text-muted">
              {textData.body}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default InfoBlockHome2;