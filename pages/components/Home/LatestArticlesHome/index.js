import React, {useEffect, useState} from "react";
import BlogService from "../../../../service/BlogService";
import UtilService from "../../../../service/UtilService";

const LatestArticlesHome = ({textData}) => {

  const [articles, setArticles] = useState([]);

  useEffect(() => {

    BlogService.getRandomArticlesByCount(9)
    .then(res => {
      setArticles(res.data.content);
    });
  }, []);

  if (!textData) {
    return <></>
  }

  return (

    <div className="container mt-100 mt-60">
      <div className="row justify-content-center">
        <div className="col-12">
          <div className="section-title text-center mb-4 pb-2">
            <h4 className="title mb-4">{textData.header}</h4>
            <p className="text-muted para-desc mb-0 mx-auto">{textData.body}</p>
          </div>
        </div>
      </div>

      <div className="row">
        <ArticlesHome articles={articles}/>
      </div>
    </div>
  );
}

const ArticlesHome = ({articles}) => {
  return articles.map((article, index) => <ArticleHome key={index} article={article}/>)
}

const ArticleHome = ({article}) => {

  return (
    <div className="col-lg-4 col-12 mt-4 pt-2">
      <div className="card blog blog-primary rounded border-0 shadow overflow-hidden">
        <div className="position-relative">
          <img src={article.topicImage} className="card-img-top" alt="..."/>
          <div className="overlay"/>
          <div className="teacher d-flex align-items-center">
            <img src={article.author.avatar} className="avatar avatar-md-sm rounded-circle shadow" alt=""/>
            <div className="ms-2">
              <h6 className="mb-0"><a href={"/article/author/" + article.author.fullName.replace(' ', '_')}
                                      className="user">{article.author.fullName}</a></h6>
              <p className="small my-0 profession">Blogger</p>
            </div>
          </div>
        </div>
        <div className="position-relative">
          <div className="shape overflow-hidden text-color-white">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"/>
            </svg>
          </div>
        </div>
        <div className="card-body content">
          <h6><a href={"article/" + article.url} className="text-primary">Blog</a></h6>
          <a href={"article/" + article.url} className="title text-dark h5">{article.topic}</a>
          <p className="text-muted mt-2">{article.topic}</p>
          <a href={"article/" + article.url} className="text-primary">
            Read More <i className="uil uil-angle-right-b align-middle"/>
          </a>
          <ul className="list-unstyled d-flex justify-content-between border-top mt-3 pt-3 mb-0">
            <li className="text-muted small"> {UtilService.printDate(article.creationDate)}</li>
          </ul>
        </div>
      </div>
    </div>
  )
}

export default LatestArticlesHome;