import React from "react";

const InfoBlockHome1 = ({textData}) => {

  if (!textData) {
    return <></>
  }

  return (
    <div className="container mt-100 mt-60 pb-5">
      <div className="row align-items-center">
        <div className="col-lg-6">
          <div className="section-title">
            <h4 className="title mb-4">
              {textData.topbox.header}<span className="text-primary">.com</span>
            </h4>
            <p className="text-muted para-desc mb-4">
              {textData.topbox.body}
            </p>
            <ul className="list-unstyled feature-list text-muted">
              <BulletPoints points={textData.topbox.points}/>
            </ul>
          </div>
        </div>
        <div className="col-lg-6 align-image-right">
          <img
            src="img/default/home-bottom.jpg"
            className="up-to-400-height"
            alt="Be in the known"
          />
        </div>
      </div>
    </div>
  );
}

const BulletPoints = ({points}) => {

  return points.map((point, index) => (
    <li key={index}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
        className="feather feather-check-circle fea icon-sm text-success mr-2"
      >
        <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"/>
        <polyline points="22 4 12 14.01 9 11.01"/>
      </svg>
      {point}
    </li>
  ))
}

export default InfoBlockHome1;