import React from "react";

const TopBlockHome = ({textData}) => {

  if (!textData) {
    return <></>
  }

  return (
    <>
      <div className="position-relative w-100 main-page-bg">
        <div className="bg-overlay top-main-section"/>
        <div className="container">
          <div className="row align-items-center position-relative mt-3">
            <div className="col-lg-6">
              <div className="title-heading mt-4 text-center text-lg-left text-white">
                <h1 className="heading mb-4">{textData.header}</h1>
                <p className="para-desc mb-5 mw-100">
                  {textData.body}
                </p>
              </div>
            </div>

            <div className="col-lg-6">
              <img
                src="img/default/home-top.jpg"
                alt="Be in the known"
                className="full-width"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="position-relative">
        <div className="shape overflow-hidden text-white">
          <svg viewBox="0 0 2880 130" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="m612,124.456l1669,-124.456l599,0l0,131.00001l-2880,0l0,-65.5l612,58.956z" fill="currentColor"/>
          </svg>
        </div>
      </div>
    </>
  );
}

export default TopBlockHome;