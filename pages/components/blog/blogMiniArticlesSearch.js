import React, {useEffect, useState} from "react";
import BlogService from "../../../service/BlogService";
import BlogMiniArticle from "./blogMiniArticle";

const BlogMiniArticlesSearch = ({blog, prefixUrl, paginator, query}) => {

  const [blogPosts, setBlogPosts] = useState(blog);
  const [secondaryLoad, setSecondaryLoad] = useState(false);

  useEffect(() => {
    setBlogPosts(blog);
    // if (secondaryLoad && paginator) {
    //   // todo add logic here to check for query so search works
    //   BlogService.getBlogByCategoryOrTagNamePageable(tagOrCategory, tagOrCategoryName, paginator.pageNumber - 1, paginator.size)
    //   .then(res => {
    //     setBlogPosts(res.data.content);
    //   });
    // } else {
    //   setSecondaryLoad(true);
    // }
  }, [paginator && paginator.pageNumber, blog, query]);

  if (blogPosts && blogPosts.length > 0) {
    return blogPosts.map((blogPost, index) => (
      <BlogMiniArticle index={index} blogPost={blogPost} prefixUrl={prefixUrl}/>
    ))
  } else {
    return <></>
  }
}

export default BlogMiniArticlesSearch;