import Link from "next/link";
import ReactGA from "react-ga4";
import React from "react";

const Tags = ({tags}) => {

  return <div className="flexing">
    <Tag tags={tags}/>
  </div>

}

const Tag = ({tags}) => {
  if (tags && tags.length > 0) {
    return tags.map((tag) => (
      <h6 className="ml-10">
        <i className="mdi mdi-tag text-primary mr-1"/>
        <Link onClick={() => {
          ReactGA.send({hitType: "pageview", page: "/blog/tag/" + tag, title: "Tag: " + tag});
        }} href={"/blog/tag/" + tag} className="text-primary">
          <a>{tag + " "}</a>
        </Link>
      </h6>
    ));
  } else {
    return (<></>)
  }
}

export default Tags;