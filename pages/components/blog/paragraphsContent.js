import React from "react";
import Link from "next/link";

const ParagraphsContent = ({paragraphs}) => {
  if (paragraphs) {
    return paragraphs
      .sort((a, b) => a.id > b.id ? 1 : -1)
      .map((paragraph, index) => <ParagraphContent key={'p_c_' + index} index={index} paragraph={paragraph}/>)
  } else {
    return <></>;
  }
}

const ParagraphContent = ({index, paragraph}) => {
  if (paragraph.type === "TEXT") {
    return <RenderText text={paragraph.content} index={index}/>
  } else if (paragraph.type === "HTML") {
    return <div dangerouslySetInnerHTML={{__html: paragraph.content}}/>
  } else if (paragraph.type === "QUOTE") {
    return (
      <blockquote key={"quote-" + index} className="blockquote mt-3 p-3">
        <p key={index} className="text-muted mb-0 font-italic">{paragraph.content}</p>
      </blockquote>
    );
  } else {
    return <></>;
  }
}

const RenderText = ({text, index}) => {
  if (text.includes('$|$')) {
    let parts = text.split('$|$');
    return parts.map((part, index) => <SmartString string={part} index={index}/>)
  } else {
    return <p key={index + "_text"} className="text-muted back-end-text">{text}</p>
  }
}

const SmartString = ({string, index}) => {
  if (string.includes('::')) {
    const myLinkPath = string.split('::');
    return (
      <Link href={"/article/" + myLinkPath[1]}>
        <a className="link-styling" key={"span_1_" + index}>{myLinkPath[0]}</a>
      </Link>
    )
  } else {
    return <span className="text-muted" key={"span_1_" + index}>{string}</span>
  }
}

export default ParagraphsContent;