import React, {useEffect, useState} from "react";
import BlogService from "../../../service/BlogService";
import Link from "next/link";
import ReactGA from "react-ga4";
import {UtilService} from "../../../service";
import Image from "next/image";
import AdminService from "../../../service/AdminService";

const BlogSidebar = ({sidebar, tagOrCategoryName, tagOrCategory, isAdmin, blog}) => {

  if (sidebar) {
    return (
      <div className="col-lg-4 col-md-6 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
        <div className="card border-0 sidebar sticky-bar ms-lg-4">
          <div className="card-body p-0">

            {/* <div className="text-center">
                                        <span className="bg-light d-block py-2 rounded shadow text-center h6 mb-0">
                                            Author
                                        </span>

                                        <div className="mt-4">
                                            <img src="/img/avatars/ava_default.jpg" className="img-fluid avatar avatar-medium rounded-pill shadow-md d-block mx-auto" alt="" />

                                            <a href="blog-about.html" className="text-primary h5 mt-4 mb-0 d-block">Cristina Jota</a>
                                            <small className="text-muted d-block">Photographer & Blogger</small>
                                        </div>
                                    </div> */}

            {isAdmin
              ? (<>
                <div className="widget mt-4">
                  <PinterestButton/>
                  <CategoriesBreakDown blog={blog}/>
                  <CategoriesToFilesBreakDown/>
                  <MidjourneyBlock blog={blog}/>
                </div>
              </>)
              : (<></>)}

            <div className="widget mt-4">
              <span className="bg-light d-block py-2 rounded shadow text-center h6 mb-0">
                  Recent Post
              </span>

              <div className="mt-4 flexing-wrapping">
                <RecentPosts articles={sidebar.newArticles}/>
              </div>
            </div>

            <div className="widget mt-4">
              <span className="bg-light d-block py-2 rounded shadow text-center h6 mb-0">
                  Tagclouds
              </span>

              <div className="tagcloud text-center mt-4">
                <TagClods tagOrCategoryName={tagOrCategoryName} tagOrCategory={tagOrCategory}/>
              </div>
            </div>

            {/* <div className="widget mt-4">
                                        <span className="bg-light d-block py-2 rounded shadow text-center h6 mb-0">
                                            Social Media
                                        </span>

                                        <ul className="list-unstyled social-icon social text-center mb-0 mt-4">
                                            <li className="list-inline-item"><a href="javascript:void(0)" className="rounded"><i data-feather="facebook" className="fea icon-sm fea-social"></i></a></li>
                                            <li className="list-inline-item"><a href="javascript:void(0)" className="rounded"><i data-feather="instagram" className="fea icon-sm fea-social"></i></a></li>
                                            <li className="list-inline-item"><a href="javascript:void(0)" className="rounded"><i data-feather="twitter" className="fea icon-sm fea-social"></i></a></li>
                                            <li className="list-inline-item"><a href="javascript:void(0)" className="rounded"><i data-feather="linkedin" className="fea icon-sm fea-social"></i></a></li>
                                            <li className="list-inline-item"><a href="javascript:void(0)" className="rounded"><i data-feather="github" className="fea icon-sm fea-social"></i></a></li>
                                            <li className="list-inline-item"><a href="javascript:void(0)" className="rounded"><i data-feather="youtube" className="fea icon-sm fea-social"></i></a></li>
                                            <li className="list-inline-item"><a href="javascript:void(0)" className="rounded"><i data-feather="gitlab" className="fea icon-sm fea-social"></i></a></li>
                                        </ul>
                                    </div> */}
          </div>
        </div>
      </div>
    )
  } else {
    return <></>
  }
}

const CategoriesBreakDown = ({blog}) => {

  let myMap = new Map();

  blog.map((b) => {
    let url = b.url;
    let category = url.substring(0, url.indexOf("/"));

    if (myMap.has(category)) {
      myMap.set(category, myMap.get(category) + 1);
    } else {
      myMap.set(category, 1);
    }
  });

  let results = Array.from(myMap, ([category, value]) => (category + ": " + value ));

  return (
    <>
      <span className="bg-light d-block py-2 rounded shadow text-center h6 mb-0">
        Categories Breakdown
      </span>

      <div className="mt-4">
        {results.map((result, index) => (
          <div key={"blog_" + index}>{result}</div>
        ))}
      </div>
    </>
  )
}

const CategoriesToFilesBreakDown = () => {

  const [categories, setCategories] = useState([]);

  useEffect(() => {
    function load() {

      AdminService.getCategoryToImageCount()
      .then((result) => {
        setCategories(result.data);
      })
    }

    if (categories.length === 0) {
      load();
    }
  }, []);

  return (
    <>
      <span className="bg-light d-block py-2 rounded shadow text-center h6 mb-0">
        Available images
      </span>

      <div className="mt-4">
        {categories.map((category, index) => (
          <div key={"blog_" + index}>{category.category + ": " + category.imageCount}</div>
        ))}
      </div>
    </>
  )
}

const MidjourneyBlock = ({blog}) => {
  return (
    <>
      <span className="bg-light d-block py-2 rounded shadow text-center h6 mb-0">
        Midjourney requests
      </span>

      <div className="mt-4">
        {blog.map((b, index) => (
          <div key={"blog_" + index}>{b.topic + " --v 5"}</div>
        ))}
      </div>
    </>
  )
}

const PinterestButton = () => {
  return (
    <div className="pointer-cursor content-width">
      <a href="https://www.pinterest.com/oauth/?client_id=1487391&redirect_uri=https://renailedit.com/feoauth/pinterest/&response_type=code&scope=boards:read,boards:write,pins:read,pins:write">
        <Image src="/img/pinterest-icon.jpg" alt="pinterest" width={42} height={42}/>
      </a>
    </div>
  )
}

const TagClods = ({tagOrCategory, tagOrCategoryName}) => {

  const [tagCloud, setTagCloud] = useState([]);
  useEffect(() => {
    async function loadTags() {
      if (tagOrCategoryName) {
        BlogService.getTagCloud(tagOrCategory, tagOrCategoryName).then(({data}) => {
          setTagCloud(data);
        });
      }
    }

    loadTags();
  }, []);

  if (tagCloud) {
    return tagCloud.map((tag, index) => (
      <Link onClick={() => {
        ReactGA.send({hitType: "pageview", page: "/blog/tag/" + tag, title: "Tag: " + tag});
      }} href={"/blog/tag/" + tag} key={"tag-" + index} className="rounded">
        <a>{tag}</a>
      </Link>
    ))
  } else {
    return <></>
  }
}

const RecentPosts = ({articles}) => {

  if (articles) {
    return articles.map((article, index) => (
      <div key={"recent-" + index} className="clearfix post-recent thumb-post-recent">
        <div className="float-left pointer-cursor">
          <Link onClick={() => {
            ReactGA.send({
              hitType: "pageview",
              page: UtilService.buildArticleUrl(article),
              title: "Article: " + UtilService.buildArticleUrl(article)
            });
          }} href={UtilService.buildArticleUrl(article)}>
            <img alt={"image of " + article.topic} src={article.topicImage} className="img-fluid rounded"/>
          </Link>
        </div>
        <div className="post-recent-content float-left">
          <Link onClick={() => {
            ReactGA.send({
              hitType: "pageview",
              page: UtilService.buildArticleUrl(article),
              title: "Article: " + buildArticleUrl(article)
            });
          }} href={UtilService.buildArticleUrl(article)}>
            <a>{article.topic}</a>
          </Link>
          <span className="text-muted mt-2">{UtilService.printDate(article.creationDate)}</span>
        </div>
      </div>
    ));
  } else {
    return <></>
  }
}

export default BlogSidebar;