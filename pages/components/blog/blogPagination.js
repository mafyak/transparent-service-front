import React from "react";

const Pagination = ({totalPages, size, pageNumber, onPageChange}) => {

  let startDotPage;
  if (pageNumber < 4) {
    startDotPage = 2;
  } else if (pageNumber < 6) {
    startDotPage = pageNumber - 2;
  } else if (pageNumber > totalPages - 4) {
    startDotPage = totalPages - 5;
  } else {
    startDotPage = pageNumber - 1;
  }

  function printPaginationThreeDots(start, size, pageNumber, totalPages) {
    let fieldValue = "...";
    let value = "...";
    if (pageNumber < 4 && start) {
      value = 2;
      fieldValue = 2;
    } else if (pageNumber > totalPages - 5 && !start) {
      value = totalPages - 1;
      fieldValue = totalPages - 1;
    }
    if (totalPages > 5) {
      return (
        <li className={"page-item" + ((pageNumber === 2 && start) || (pageNumber === totalPages - 1 && !start) ? " active" : "")} key={"page dots" + start}>
          <button
            className="page-link"
            onClick={() => {
              if (value === "..." && start) {
                onPageChange(Math.round((pageNumber + 1)/2));
              } else if (value === "..." && !start) {
                onPageChange(Math.round((pageNumber + totalPages)/2));
              } else {
                onPageChange(value);
              }
            }}
          >
            {fieldValue}
          </button>
        </li>
      );
    } else {
      return <></>
    }
  }

  function printPaginationMiddlePart(totalPages, number, size, startDotPage) {
    if (totalPages > 5) {
      return Array.from(Array(3), (e, i) => (
        <li className={"page-item" + (number === i + startDotPage + 1 ? " active" : "")} key={"page" + (startDotPage + i)}>
          <button
            className="page-link"
            onClick={() => {
              onPageChange(i + startDotPage + 1);
            }}
          >
            {i + startDotPage + 1}
          </button>
        </li>
      ));
    } else if (totalPages === 2) {
      return;
    } else if (totalPages > 2 && totalPages < 6) {
      let pages = totalPages - 2;
      return Array.from(Array(pages), (e, i) => (
        <li className={"page-item" + (number === i + 2 ? " active" : "")} key={"page" + (i)}>
          <button
            className="page-link"
            onClick={() => {
              onPageChange(i + 2);
            }}
          >
            {i + 2}
          </button>
        </li>
      ));
    }
  }

  if (totalPages && totalPages > 1) {
    return (
      <div className="d-flex p-4 justify-content-center">
        <ul className="pagination mb-0">
          <li className={"page-item" + (pageNumber === 1 ? " active" : "")}>
            <button
              className="page-link"
              key="prev"
              aria-label="Previous"
              onClick={() => {
                onPageChange(1);
              }}
            >
              1
            </button>
          </li>
          {printPaginationThreeDots(true, size, pageNumber, totalPages)}
          {printPaginationMiddlePart(totalPages, pageNumber, size, startDotPage)}
          {printPaginationThreeDots(false, size, pageNumber, totalPages)}
          <li className={"page-item" + (pageNumber === totalPages ? " active" : "")}>
            <button
              className="page-link"
              key="next"
              aria-label="Next"
              onClick={() => {
                onPageChange(totalPages);
              }}
            >
              {totalPages}
            </button>
          </li>
        </ul>
      </div>
    );
  } else {
    return <></>
  }
}

export default Pagination;