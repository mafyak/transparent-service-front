import Link from "next/link";
import ReactGA from "react-ga4";
import React from "react";
import {withAlert} from "react-alert";
import {UtilService} from "../../../service";

const RelatedArticlesBlock = (relatedArticles) => {
  if (relatedArticles && relatedArticles.length > 0) {
    return (
      <div className="card shadow rounded border-0 mt-4">
        <div className="card-body">
          <h5 className="card-title mb-0">Similar articles :</h5>
          <div className="row">
            <RelatedArticles relatedArticles={relatedArticles}/>
          </div>
        </div>
      </div>
    );
  } else {
    return (<></>)
  }
}

const RelatedArticles = (relatedArticles) => {
  if (relatedArticles) {
    return relatedArticles.map((article) => (
      <div className="col-lg-6 mt-4 pt-2">
        <div className="card blog rounded border-0 shadow">
          <div className="position-relative">
            <img src={article.topicImage} className="card-img-top rounded-top" alt="..."/>
            <div className="overlay rounded-top bg-dark"></div>
          </div>
          <div className="card-body content">
            <h5>
              <Link href={"/article/" + article.url}>
                <a onClick={() => {
                  ReactGA.send({ hitType: "pageview", page: "/article/" + article.url, title: "Article: " + article.url });
                }} className="card-title title text-dark">
                  {article.topic}
                </a>
              </Link>
            </h5>
            <div className="post-meta d-flex justify-content-between mt-3">
              {/* <LikesAndCommentsCount article={article}/> */}
              <Link href={"/article/" + article.url}>
                <a onClick={() => {
                  ReactGA.send({ hitType: "pageview", page: "/article/" + article.url, title: "Article: " + article.url });
                }} className="text-muted readmore">
                  Read More <i className="mdi mdi-chevron-right"></i>
                </a>
              </Link>
            </div>
          </div>
          <div className="author">
            <small className="text-light user d-block">
              <i className="mdi mdi-account"></i> {article.author.fullName}
            </small>
            <small className="text-light date">
              <i className="mdi mdi-calendar-check"></i> {UtilService.printDate(article.creationDate)}
            </small>
          </div>
        </div>
      </div>
    ));
  } else {
    return <></>
  }
}

export default withAlert()(RelatedArticlesBlock);