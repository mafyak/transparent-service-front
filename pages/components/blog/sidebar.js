import Link from "next/link";
import ReactGA from "react-ga4";
import React from "react";
import {UtilService} from "../../../service";

const Sidebar = ({sidebar, article}) => {
  if (sidebar) {
    return (
      <div className="col-lg-4 col-md-6 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
        <div className="card border-0 sidebar sticky-bar rounded shadow">
          <div className="card-body">

            <div className="text-center">
              <span className="bg-light d-block py-2 rounded shadow text-center h6 mb-0 large-font">
                Author
              </span>
              <div className="mt-4">
                <Link href={"/article/author/" + article.author.fullName.replace(' ', '_')}>
                  <a onClick={() => {
                    ReactGA.send({
                      hitType: "pageview",
                      page: "/article/author/" + article.author.fullName.replace(' ', '_'),
                      title: "User page: " + article.author.fullName
                    });
                  }}>
                    <img src={article.author.avatar} alt={"Avatar of " + article.author.fullName}
                         className="img-fluid avatar avatar-medium rounded-pill shadow-md d-block mx-auto"/>
                  </a>
                </Link>
                <Link href={"/article/author/" + article.author.fullName.replace(' ', '_')}>
                  <a onClick={() => {
                    ReactGA.send({
                      hitType: "pageview",
                      page: "/article/author/" + article.author.fullName.replace(' ', '_'),
                      title: "User page: " + article.author.fullName
                    });
                  }} className="text-primary h5 mt-4 mb-0 d-block">{article.author.fullName}</a>
                </Link>
                <small className="text-muted d-block">Blogger</small>
              </div>
            </div>

            <div className="widget mb-4 pb-2">
              <span className="bg-light d-block py-2 rounded shadow text-center h6 mb-0 large-font">
                All categories
              </span>
              <ul className="list-unstyled mt-4 mb-0 blog-catagories">
                <Categories categories={sidebar.topCategories}/>
              </ul>
            </div>

            <div className="widget mb-4 pb-2">
              <span className="bg-light d-block py-2 rounded shadow text-center h6 mb-0 large-font">
                New Articles
              </span>
              <div className="mt-4 new-articles-block">
                <ArticlesSidebar articles={sidebar.newArticles}/>
              </div>
            </div>

            <div className="widget mb-4 pb-2">
              <span className="bg-light d-block py-2 rounded shadow text-center h6 mb-0 large-font">
                Related Articles
              </span>
              <div className="mt-4 new-articles-block">
                <ArticlesSidebar articles={article.relatedArticles}/>
              </div>
            </div>

            <div className="widget mb-4 pb-2">
              <span className="bg-light d-block py-2 rounded shadow text-center h6 mb-0 large-font">
                Tags
              </span>
              <div className="tagcloud mt-4">
                <Tags tags={sidebar.topTags}/>
              </div>
            </div>
            {/*{printFollowBlock()}*/}
          </div>
        </div>
      </div>
    );
  } else {
    return <></>
  }
}

const Categories = ({categories}) => {
  if (categories) {
    return categories.map((category, index) => (
      <li key={"categories-" + index}>
        <Link href={"/blog/category/" + category.name}>
          <a onClick={() => {
            ReactGA.send({
              hitType: "pageview",
              page: "/blog/category/" + category.name,
              title: category.name + " blog"
            });
          }}>{category.name}</a>
        </Link>
        <span className="float-right">{category.count}</span>
      </li>
    ));
  } else {
    return <></>
  }
}

const ArticlesSidebar = ({articles}) => {

  // todo-hot check if new articles section shows correct images. If so, remove this function
  // function buildArticleUrl(article) {
  //   if (article.url && article.category) {
  //     return "/article/" + article.url;
  //   } else if (article.url) {
  //     return "/article/construction/" + article.url;
  //   } else {
  //     return "/blog";
  //   }
  // }

  if (articles) {
    return articles.map((article, index) => (
      <div className="clearfix post-recent thumb-post-recent" key={"new-article-" + index}>
        <div className="float-left">
          {" "}
          <Link href={UtilService.buildArticleUrl(article)}>
            <a onClick={() => {
              ReactGA.send({hitType: "pageview", page: article.url, title: "Article: " + article.url});
            }}>
              {" "}
              <img alt={"image of " + article.topic} src={article.topicImage} className="img-fluid rounded"/>
            </a>
          </Link>
        </div>
        <div className="post-recent-content float-left">
          <Link href={UtilService.buildArticleUrl(article)}>
            <a onClick={() => {
              ReactGA.send({hitType: "pageview", page: article.url, title: "Article: " + article.url});
            }}>{article.topic}</a>
          </Link>
          <span className="text-muted mt-2">{UtilService.printDate(article.creationDate)}</span>
        </div>
      </div>
    ));
  } else {
    return <></>
  }
}

const Tags = ({tags}) => {
  if (tags && tags.length > 0) {
    return tags.map((tag, index) => (
      // todo add page here
      <Link href={"/blog/tag/" + tag} key={"top-tag-" + index}>
        <a className="rounded">
          {tag}
        </a>
      </Link>
    ));
  } else {
    return <></>
  }
}


export default Sidebar;