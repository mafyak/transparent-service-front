import Link from "next/link";
import ReactGA from "react-ga4";
import {UtilService} from "../../../service";
import defaultArticleImage from "../../../assets/images/full.jpg";

const BlogMiniArticle = ({blogPost, index, prefixUrl}) => {

  if (blogPost) {
    return (
      <div key={"blog-mini-art-" + index} className="col-lg-6 col-md-12 mb-4 pb-2">
        <div className="card blog blog-primary rounded border-0 shadow">
          <div className="position-relative pointer-cursor">
            <ArticleImage article={blogPost}/>
            <Link onClick={() => {
              ReactGA.send({hitType: "pageview", page: prefixUrl + blogPost.url, title: "Article: " + blogPost.url});
            }} href={prefixUrl + blogPost.url}>
              <div className="overlay rounded-top"/>
            </Link>
          </div>
          <div className="card-body content">
            <h5>
              <Link onClick={() => {
                ReactGA.send({hitType: "pageview", page: prefixUrl + blogPost.url, title: "Article: " + blogPost.url});
              }} href={prefixUrl + blogPost.url} className="card-title title text-dark">
                <a>{blogPost.topic}</a>
              </Link>
            </h5>
            <div className="post-meta d-flex justify-content-between mt-3">
              {/* <ul className="list-unstyled mb-0">
                                <li className="list-inline-item me-2 mb-0"><a href="javascript:void(0)" className="text-muted like"><i className="uil uil-heart me-1"></i>{blogPost.likesCount}</a></li>
                                <li className="list-inline-item"><a href="javascript:void(0)" className="text-muted comments"><i className="uil uil-comment me-1"></i>{blogPost.commentsCount}</a></li>
                            </ul> */}
              <Link onClick={() => {
                ReactGA.send({hitType: "pageview", page: prefixUrl + blogPost.url, title: "Article: " + blogPost.url});
              }} href={prefixUrl + blogPost.url} className="text-muted readmore">
                <a>Read More</a>
              </Link>
              <i className="uil uil-angle-right-b align-middle"/>
            </div>
          </div>
          <div className="author">
            <small className="user d-block"><i className="uil uil-user"/> {blogPost.author.fullName}</small>
            <small className="date"><i className="uil uil-calendar-alt"/>
              {UtilService.printDate(blogPost.creationDate)}
            </small>
          </div>
        </div>
      </div>
    )
  } else {
    return <></>
  }
}

const ArticleImage = ({article}) => {
  let imageUrl = article.topicImage;
  if (imageUrl === "") {
    return <img alt="article image" src={defaultArticleImage} className="card-img-top rounded-top"/>
  } else {
    return <img alt="article image" src={imageUrl} className="card-img-top rounded-top"/>
  }
}

export default BlogMiniArticle;