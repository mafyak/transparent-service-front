import Link from "next/link";
import ReactGA from "react-ga4";
import React from "react";
import {UtilService} from "../../../service";

const PageHead = ({article, category}) => {
  if (article) {
    return (
      <>
        <section className="bg-half-110-20 bg-light d-table w-100">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-12 text-center">
                <div className="page-next-level">
                  <h2> {article.topic} </h2>
                  <ul className="list-unstyled mt-4">
                    <li className="list-inline-item h6 user text-muted mr-2">
                      <i className="mdi mdi-account"/> {article.author ? article.author.fullName : " "}
                    </li>
                    <li className="list-inline-item h6 date text-muted">
                      <i className="mdi mdi-calendar-check"/> {UtilService.printDate(article.creationDate)}
                    </li>
                  </ul>
                  <div className="page-next page-next-level-override">
                    <nav aria-label="breadcrumb" className="d-inline-block">
                      <ul className="breadcrumb bg-white rounded shadow mb-0">
                        <li className="breadcrumb-item">
                          <Link onClick={() => {
                            ReactGA.send({
                              hitType: "pageview",
                              page: "/blog/category/" + process.env.NEXT_PUBLIC_DEFAULT_BLOG,
                              title: "Flooring blog"
                            });
                          }} href={"/blog/category/" + process.env.NEXT_PUBLIC_DEFAULT_BLOG}>
                            <a>Blog</a>
                          </Link>
                        </li>
                        <li className="breadcrumb-item">
                          <Link onClick={() => {
                            ReactGA.send({
                              hitType: "pageview",
                              page: "/blog/category/" + category,
                              title: category.name + " blog"
                            });
                          }} href={"/blog/category/" + category}>
                            <a>{category}</a>
                          </Link>
                        </li>
                        <li className="breadcrumb-item active" aria-current="page">
                          {article.topic}
                        </li>
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div className="position-relative">
          <div className="shape overflow-hidden text-white">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"/>
            </svg>
          </div>
        </div>
      </>
    )
  } else {
    return <></>
  }
}

export default PageHead;