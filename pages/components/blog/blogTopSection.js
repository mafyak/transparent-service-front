import Link from "next/link";
import React from "react";

const BlogTopSection = ({tagOrCategory, tagOrCategoryName}) => {
  return (
    <section className="bg-half-100 bg-light d-table w-100" key="section1">
      <div className="container">
        <div className="row mt-5 justify-content-center">
          <div className="col-lg-12 text-center">
            <div className="pages-heading">
              <h4 className="title mb-0"> Blog </h4>
            </div>
          </div>
        </div>

        <div className="position-breadcrumb">
          <nav aria-label="breadcrumb" className="d-inline-block">
            <ul className="breadcrumb rounded shadow mb-0 px-4 py-2">
              <li className="breadcrumb-item">Blog</li>
              <li className="breadcrumb-item">{tagOrCategory}</li>
              {tagOrCategoryName && (
                <li className="breadcrumb-item">
                  <Link href={"/blog/" + tagOrCategory + "/" + tagOrCategoryName}>
                    <a>{tagOrCategoryName}</a>
                  </Link>
                </li>
              )}
            </ul>
          </nav>
        </div>
      </div>
    </section>
  )
}

export default BlogTopSection;