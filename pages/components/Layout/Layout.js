import React, { useEffect } from "react";
import Footer from "../Footer/Footer";
import Header from "../Header/Header";
import ReactGA from "react-ga4";
import { useRouter } from "next/router";

export default function Layout({ children }) {
  const noNav = ["/login", "/register"];
  const router = useRouter();
  const { pathname } = router;

  useEffect(() => {
    if (window && !window.GA_INITIALIZED) {
      ReactGA.initialize(process.env.NEXT_PUBLIC_ANALYTICS_TAG);
      window.GA_INITIALIZED = true;
    }
    ReactGA.send({ hitType: "pageview", page: window.location.pathname + window.location.search, title: "Page: " + window.location.pathname + window.location.search });
  }, []);

  if (noNav.includes(pathname)) {
    return <>{children}</>;
  } else {
    return (
      <>
        <Header/>
        <>{children}</>
        <Footer />
      </>
    );
  }
}
