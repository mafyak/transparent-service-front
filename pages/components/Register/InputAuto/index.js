import React, { Component } from "react";
import Select from "react-select";
import CityService from "../../../../service/CityService";

class InputAuto extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cities: [],
      selectedCity: null,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    CityService.retrieveAllCities().then((response) => {
      const newArr = [];
      response.data.map((item) => {
        newArr.push({ value: item.id, label: item.name });
      });
      this.setState({ cities: newArr });
    });
  }

  handleChange(thisCity) {
    this.setState({ selectedCity: thisCity });
    this.props.cityChangeParentToChild(thisCity);
  }

  render() {
    const { selectedCity, cities } = this.state;

    return (
      <Select
        defaultValue={selectedCity}
        onChange={this.handleChange}
        options={cities}
        required
        placeholder={"City..."}
        className="react-select-container"
        classNamePrefix="react-select"
      />
    );
  }
}
export default InputAuto;
