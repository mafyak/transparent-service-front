import React from "react";
import { mdiVk } from "@mdi/js";
import Icon from "@mdi/react";

const SocilNetworksBlock = () => {
  return (
    <div className="col-lg-12 mt-4 text-center">
      <h6>Зайти при помощи</h6>
      <ul className="list-unstyled social-icon mb-0 mt-3">
        <li className="list-inline-item">
          <a
            href="https://oauth.vk.com/authorize?client_id=7508029&display=page&redirect_uri=https://RMT.by/socialLogin/&scope=email&response_type=code&v=5.110"
            className="rounded"
          >
            <Icon path={mdiVk} title="Vkontakte" size={1.2} />
          </a>
        </li>
      </ul>
    </div>
  );
};
export default SocilNetworksBlock;
