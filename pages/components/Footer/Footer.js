import React from "react";
import Link from "next/link";
import ReactGA from "react-ga4";

const Footer = () => {

  return (
    <>
      <footer className="footer">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-12 col-12  mb-0 mb-md-4 pb-0 pb-md-2">
              <Link href="/">
                <a onClick={() => ReactGA.send({ hitType: "pageview", page: "/", title: "Home" })} className="logo-footer">
                  {process.env.NEXT_PUBLIC_SITE_NAME}<span className="text-primary">.com</span>
                </a>
              </Link>
              <p className="mt-4">{process.env.NEXT_PUBLIC_SITE_SLOGAN}</p>
            </div>
            <div className="col-lg-2 col-md-3 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
              <h4 className="text-light footer-head">Goodies</h4>
              <ul className="list-unstyled footer-list mt-4">
                <li>
                  <Link href="/our-model">
                    <a onClick={() => ReactGA.send({ hitType: "pageview", page: "/siteTerms", title: "Website terms" })} className="text-foot">
                      <i className="mdi mdi-chevron-right mr-1"/> Site Terms
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/privacy-policy">
                    <a onClick={() => ReactGA.send({ hitType: "pageview", page: "/siteTerms", title: "Website terms" })} className="text-foot">
                      <i className="mdi mdi-chevron-right mr-1"/> Privacy Policy
                    </a>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>

      <footer className="footer footer-bar">
        <div className="container text-center">
          <div className="row align-items-center">
            <div className="col-12 col-md-4">
              <div className="text-sm-left">
                <p className="mb-0">
                  © 2023 {process.env.NEXT_PUBLIC_SITE_NAME}.com <i className="mdi mdi-heart color-white"/>
                  <i className="mdi mdi-heart color-red"/>
                  <i className="mdi mdi-heart color-white"/>
                </p>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
}

export default Footer;