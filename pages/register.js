import React, { Component } from "react";
import { UserService, CityService, ValidationService } from "@services";
// import { InputAuto, SocialNetworksBlock } from "@components";
import InputAuto from "./components/Register/InputAuto";
import SocialNetworksBlock from "./components/Register/SocialNetworksBlock";
import { withAlert } from "react-alert";
import Select from "react-select";
import Link from "next/link";
import { Home, Mail, Key, User, Phone, UserCheck, Eye, EyeOff } from "react-feather";
import Router from "next/router";
import Head from "next/head";

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: "",
      login: "",
      email: "",
      city: {
        id: "",
      },
      phone: "",
      pass: "",
      hidePass: true,
      hideRePass: true,
      repassword: "",
      type: "", // MASTER or USER
      rules: false,
      cities: [],
      roles: [
        { value: "USER", label: "Home Owner" },
        { value: "BLOGGER", label: "Blogger" },
        { value: "MASTER", label: "Contractor" },
      ],
    };
    this.firstNameChange = this.firstNameChange.bind(this);
    this.lastNameChange = this.lastNameChange.bind(this);
    this.emailChange = this.emailChange.bind(this);
    this.cityChange = this.cityChange.bind(this);
    this.phoneChange = this.phoneChange.bind(this);
    this.passChange = this.passChange.bind(this);
    this.repasswordChange = this.repasswordChange.bind(this);
    this.selectRole = this.selectRole.bind(this);
    this.rulesChange = this.rulesChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    CityService.retrieveAllCities().then((response) => this.setState({ cities: response.data }));
  }

  rulesChange() {
    this.setState({ rules: !this.state.rules });
  }

  repasswordChange({ target }) {
    this.setState({ repassword: target.value });
  }

  passChange({ target }) {
    this.setState({ pass: target.value });
  }

  cityChange = (selectedCity) => {
    var curCity = {};
    curCity.id = selectedCity.value;
    curCity.name = selectedCity.label;
    this.setState({ city: curCity });
  };

  emailChange({ target }) {
    this.setState({ login: target.value, email: target.value });
  }

  phoneChange({ target }) {
    this.setState({ phone: target.value });
  }

  lastNameChange({ target }) {
    this.setState({ lastName: target.value });
  }

  firstNameChange({ target }) {
    this.setState({ firstName: target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    if (ValidationService.validateEmail(this.state.email)) {
      if (this.state.pass == this.state.repassword) {
        if (this.state.city.name) {
          if (this.state.roles) {
            if (this.state.rules) {
              var user = {};
              user.email = this.state.email;
              user.login = this.state.login;
              user.type = this.state.type;
              if (user.type == "MASTER") {
                localStorage.setItem("masterFirstRegistration", "Y");
              }
              const bcrypt = require("bcryptjs");
              const salt = bcrypt.genSaltSync(10);
              const encodedPass = bcrypt.hashSync(this.state.pass, salt);
              user.pass = encodedPass;
              user.fullName = this.state.firstName + " " + this.state.lastName;
              user.phone = this.state.phone;
              user.city = this.state.city;
              UserService.registerUser(user).then((response) => {
                if (response != null && response.status == 201) {
                  this.printAlert(response);
                  Router.push({ pathname: "/login/" });
                  // @ add more logic here. For example, check if email is already registered.
                } else {
                  this.callFailureAlert("Error. Seems like this email is in use.");
                }
              });
            } else {
              this.callFailureAlert("Please, accept website usage terms");
            }
          } else {
            this.callFailureAlert("Select your role");
          }
        } else {
          this.callFailureAlert("Select a city");
        }
      } else {
        this.callFailureAlert("Passwords do not match");
      }
    } else {
      this.callFailureAlert("Bad email");
    }
  }

  printCities() {
    return (
      <>
        {this.state.cities.map((city) => (
          <option key={city.id} value={city.id}>
            {city.name}
          </option>
        ))}
      </>
    );
  }

  printAlert(response) {
    if (response != null && response.status == 201) {
      alert = withAlert();
      this.callSuccessAlert("Thank you for registration");
    } else {
      this.callFailureAlert("Error during registration");
    }
  }

  callSuccessAlert(message) {
    alert = withAlert();
    this.props.alert.success(message);
  }

  callFailureAlert(message) {
    alert = withAlert();
    this.props.alert.error(message);
  }

  selectRole(role) {
    this.setState({ type: role.value });
  }

  showPass() {
    this.setState({ hidePass: !this.state.hidePass });
  }

  showRePass() {
    this.setState({ hideRePass: !this.state.hideRePass });
  }

  render() {
    return (
      <div>
        <Head>
          <title>Register page</title>
          <meta name="description" content={"Register page"}/>
          <meta property="og:description" content={"Register page"}/>
          <meta property="og:title" content={"Register page"}/>
        </Head>

        {/* @todo - изображение загрузки страницы. Как связать его с логикой? */}
        {/* <div id="preloader">
          <div id="status">
            <div className="spinner">
              <div className="double-bounce1"></div>
              <div className="double-bounce2"></div>
            </div>
          </div>
        </div> */}
        <div className="back-to-home rounded d-none d-sm-block">
          <a href="/" className="text-white title-dark rounded d-inline-block text-center">
            <Home className="fea icon-sm" />
          </a>
        </div>

        <section className="bg-home bg-circle-gradiant d-flex align-items-center" style={{ padding: "50px" }}>
          <div className="bg-overlay bg-overlay-white"/>
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-8 col-md-8">
                <div className="card login_page shadow rounded border-0">
                  <div className="card-body">
                    <h4 className="card-title text-center">Registration</h4>
                    <form className="login-form mt-4" onSubmit={this.handleSubmit}>
                      <div className="row">
                        <div className="col-md-6">
                          <div className="form-group position-relative">
                            <label>
                              First Name <span className="text-danger">*</span>
                            </label>
                            <User className="fea icon-sm icons" />
                            <input
                              type="text"
                              className="form-control pl-5 pad-left-40"
                              placeholder="First Name"
                              name="s"
                              required
                              value={this.state.firstName}
                              onChange={this.firstNameChange}
                            />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group position-relative">
                            <label>
                              Last name<span className="text-danger">*</span>
                            </label>
                            <UserCheck className="fea icon-sm icons" />
                            <input
                              type="text"
                              className="form-control pl-5 pad-left-40"
                              placeholder="Last name"
                              name="s"
                              required
                              value={this.state.lastName}
                              onChange={this.lastNameChange}
                            />
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="form-group position-relative">
                            <label>
                              E-mail <span className="text-danger">*</span>
                            </label>
                            <Mail className="fea icon-sm icons" />
                            <input
                              type="email"
                              className="form-control pl-5 pad-left-40"
                              placeholder="E-mail"
                              name="email"
                              required
                              value={this.state.email}
                              onChange={this.emailChange}
                            />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group position-relative">
                            <label>
                              City <span className="text-danger">*</span>
                            </label>
                            <InputAuto cityChangeParentToChild={this.cityChange} printCities={this.printCities} />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group position-relative">
                            <label>
                              Phone <span className="text-danger">*</span>
                            </label>
                            <Phone className="fea icon-sm icons" />
                            <input
                              type="text"
                              className="form-control pl-5 pad-left-40"
                              required
                              minLength="10"
                              maxLength="13"
                              placeholder="ххх-xхх-xxхх"
                              value={this.state.phone}
                              onChange={this.phoneChange}
                            />
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="form-group position-relative">
                            <label>
                              Password <span className="text-danger">*</span>
                            </label>
                            <Key className="fea icon-sm icons" />
                            <input
                              type={this.state.hidePass ? "password" : "text"}
                              className="form-control pl-5 pr-5 pad-left-40"
                              placeholder="Password"
                              pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                              required
                              value={this.state.pass}
                              onChange={this.passChange}
                              title="Password has to contain at least one digit, one uppercase letter, one lowercase letter. Total length 8+ symbols."
                            />
                            {this.state.hidePass ? (
                              <Eye className="fea icon-sm icon-right" onClick={() => this.showPass()} />
                            ) : (
                              <EyeOff className="fea icon-sm icon-right" onClick={() => this.showPass()} />
                            )}
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="form-group position-relative">
                            <label>
                              Password again
                              <span className="text-danger">*</span>
                            </label>
                            <Key className="fea icon-sm icons" />
                            <input
                              type={this.state.hideRePass ? "password" : "text"}
                              className="form-control pl-5 pad-left-40"
                              placeholder="Password again"
                              required
                              value={this.state.repassword}
                              onChange={this.repasswordChange}
                            />
                            {this.state.hideRePass ? (
                              <Eye className="fea icon-sm icon-right" onClick={() => this.showRePass()} />
                            ) : (
                              <EyeOff className="fea icon-sm icon-right" onClick={() => this.showRePass()} />
                            )}
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="form-group position-relative">
                            <label>
                              Role<span className="text-danger">*</span>
                            </label>
                            <Select
                              onChange={this.selectRole}
                              options={this.state.roles}
                              required
                              placeholder={"Select role"}
                              className="react-select-container"
                              classNamePrefix="react-select"
                            />
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="form-group">
                            <div className="custom-control custom-checkbox">
                              <input
                                type="checkbox"
                                className="custom-control-input"
                                id="customCheck2"
                                value={this.state.rules}
                                onChange={this.rulesChange}
                              />{" "}
                              <label className="custom-control-label pl-3 d-inline-flex" htmlFor="customCheck2">
                                I accept{" "}
                                <Link href={"/siteTerms"}>
                                  <a className="text-primary px-3" target="_blank">
                                    website terms
                                  </a>
                                </Link>
                              </label>
                            </div>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <button className="btn btn-primary btn-block">Register</button>
                        </div>
                        {/*<SocialNetworksBlock />*/}
                        <div className="mx-auto">
                          <p className="mb-0 mt-3">
                            <small className="text-dark mr-2">Already registered? {" "}</small>
                            <a href="/login" className="text-dark font-weight-bold">
                              Log in
                            </a>
                          </p>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default withAlert()(Register);
