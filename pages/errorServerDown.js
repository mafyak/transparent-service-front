import React from "react";
import image1 from "../assets/images/404.png";
import Link from "next/link";
import ReactGA from "react-ga4";

// @todo this page is not used. Figure out how can we use it.
const ErrorServerDown = () => {
  return (
    <section className="bg-home d-flex align-items-center">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-8 col-md-12 text-center">
            <img src={image1} className="img-fluid" alt="404" />
            <div className="text-uppercase mt-4 display-3">Ой!</div>
            <div className="text-capitalize text-dark mb-4 error-page">Наш сервер решил взять перекур</div>
            <p className="text-muted para-desc mx-auto"> Что-то пошло не так, пожалуйста, свяжитесь с администрацией сайта: </p>
            <div className="text-capitalize text-dark mb-4 error-page">+375(33)307-30-63</div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-12 text-center">
            <Link href="/">

              <a onClick={() => ReactGA.send({ hitType: "pageview", page: "/", title: "Home" })} className="btn btn-primary mt-4 ml-2">
                Home
              </a>
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ErrorServerDown;
