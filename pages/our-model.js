import React, {Component} from "react";
import infoPic from "../assets/images/SEO_SVG.svg";
// import { TopCategories, OurPrinciples, OurPrices } from "@components";
import OurPrinciples from "./components/OurModel/OurPrinciples";
import Head from "next/head";

// import OurPrices from "./components/OurModel/OurPrices";

class OurModel extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
      <Head>
        <title>Our model</title>
        <meta property="og:title" content="Our model"/>
        <meta name="description" content="Our model"/>
        <meta property="og:description" content="Our model"/>
      </Head>
      <section className="section">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-12 text-center">
              <div className="section-title mb-4 pb-2">
                <h4 className="title mb-4">Blog by construction experts</h4>
                {/*<h4 className="title mb-4">Какой принцип работы ?</h4>*/}
                {/*<p className="text-muted para-desc mb-0 mx-auto">*/}
                {/*  Наша площадка помогает упростить процесс общения и оценки работы заказчикам и компаниям-исполнителям. Работа сайта построена на*/}
                {/*  системном отборе лучших специалистов, предоставляющих услуги. Любая компания может зарегистрироваться на сайте бесплатно и получать*/}
                {/*  заявки с сайта.*/}
                {/*</p>*/}
              </div>
            </div>
          </div>

          <BlockOne/>

          {/*<TopCategories />*/}
          {/*<OurPrinciples/>*/}
          {/*<OurPrices />*/}

        </div>
      </section>
      </>
    );
  }
}

const BlockOne = () => {
  return (
    <div className="row align-items-center">
      <div className="col-lg-5 col-md-6 mt-4 pt-2">
        <img src={infoPic} alt=""/>
      </div>

      <div className="col-lg-7 col-md-6 mt-4 pt-2">
        <div className="section-title ml-lg-5">
          <h4 className="title mb-4">We're construction experts sharing knowledge</h4>
          <p className="text-muted">
            For the last 15+ years, our team managed hundreds small and large construction projects,
            covering different construction aspects, such as flooring, siding, roofing, framing, electricity and etc.
            This blog will shed some light on the most commonly asked construction questions. We will try to
            provide the best and valuable explanations, so YOU avoid as many mistakes as possible.
          </p>
          <ul className="feature-list text-muted">
            <p className=" bold_text">What you will find in our blog:</p>
            <li>
              <i data-feather="check-circle" className="fea icon-sm text-success mr-2"/>
              What is the best material/product for your job.
            </li>
            <li>
              <i data-feather="check-circle" className="fea icon-sm text-success mr-2"/>
              What to look for in your estimates and how to compare prices from different contractors.
            </li>
            <li>
              <i data-feather="check-circle" className="fea icon-sm text-success mr-2"/>
              What should you expect from job completion.
            </li>
            <li>
              <i data-feather="check-circle" className="fea icon-sm text-success mr-2"/>
              How do deal with contractors so you don't end up disappointed.
            </li>
          </ul>
        </div>
      </div>
    </div>
  )
}

const BlockOneOriginal = () => {
  return (
    <div className="row align-items-center">
      <div className="col-lg-5 col-md-6 mt-4 pt-2">
        <img src={infoPic} alt=""/>
      </div>

      <div className="col-lg-7 col-md-6 mt-4 pt-2">
        <div className="section-title ml-lg-5">
          <h4 className="title mb-4">Получайте заявки прямо на ваш телефон!</h4>
          <p className="text-muted">
            При поиске компаний, предоставляющих услуги, каждому клиенту предлагаются исполнители,
            зарегистрированные на нашем сайте. Механизм
            подбора выбирает 3 либо 5 исполнителей которые подходят под определённые запросы на сайте. Каждый
            исполнитель, подобранный таким
            образом, получает смс с информацией о заказе. Экономьте своё время и позвольте нам предоставлять вам
            клиентов.
          </p>
          <ul className="feature-list text-muted">
            <p className=" bold_text">Что для этого необходимо:</p>
            <li>
              <i data-feather="check-circle" className="fea icon-sm text-success mr-2"></i>
              Зарегистрировать свою компанию.
            </li>
            <li>
              <i data-feather="check-circle" className="fea icon-sm text-success mr-2"></i>
              Указать все города в которых вы предоставляете свои услуги.
            </li>
            <li>
              <i data-feather="check-circle" className="fea icon-sm text-success mr-2"></i>
              Указать все категории работ в которых вы предоставляете свои услуги.
            </li>
            {/* <li>
                    <i data-feather="check-circle" className="fea icon-sm text-success mr-2"></i>
                    Выбрать тип подписки, который больше всего подходит вам.
                  </li> */}
          </ul>
        </div>
      </div>
    </div>
  )
}

export default OurModel;
