import React, { Component } from "react";
import { TicketService } from "@services";
import { withAlert } from "react-alert";

class Tickets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tickets: "",
      newTicket: "",
      newTopic: "",
    };
    this.handleNewTicketChange = this.handleNewTicketChange.bind(this);
    this.handleNewTopicChange = this.handleNewTopicChange.bind(this);
  }

  componentDidMount() {
    if (typeof window !== "undefined" && localStorage.getItem("logged in")) {
      this.refreshData();
    }
  }

  callError(message) {
    if (typeof window !== "undefined") {
      alert = withAlert();
      this.props.alert.error(message);
    }
  }

  refreshData() {
    TicketService.retrieveMyTickets().then((response) => {
      this.setState({
        tickets: response.data,
      });
    });
  }

  handleNewTicketChange({ target }) {
    this.setState({ newTicket: target.value });
  }

  handleNewTopicChange({ target }) {
    this.setState({ newTopic: target.value });
  }

  printStatus(status) {
    let newStatus = "";
    if (status == "NEW") {
      newStatus = "Новый";
    } else if (status == "PENDING") {
      newStatus = "В обработке";
    } else if (status == "CLOSED") {
      newStatus = "Закрыт";
    }
    return <p className="col-sm-3 col-12 ">{newStatus}</p>;
  }

  printTickets() {
    if (this.state.tickets && this.state.tickets.length > 0) {
      return this.state.tickets.map((ticket) => (
        <div key={ticket.id} className="form-group row mr-5">
          {this.printStatus(ticket.status)}
          <p className="text-primary col-sm-3 col-12 ">{ticket.creationDate}</p>
          <p className="text-primary col-sm-3 col-12 ">{ticket.topic}</p>
          <div className="col-sm-3 col-12">
            <a href={"/ticket/" + ticket.id} target="_blank" className="btn btn-sm btn-outline-primary d-block row mb-4">
              <span>Посмотреть</span>
            </a>
          </div>
        </div>
      ));
    } else {
      return <div className="form-group row mr-5">У вас нету открытых запросов в поддержку</div>;
    }
  }

  validateTicket() {
    if (this.state.newTopic == null || this.state.newTopic.length == 0) {
      this.callError("Пожалуйста, введите тему вопроса");
      return false;
    }

    if (this.state.newTicket == null || this.state.newTicket.length == 0) {
      this.callError("Пожалуйста, введите описание проблемы");
      return false;
    }
    return true;
  }

  addTicket() {
    if (this.validateTicket()) {
      let ticket = { topic: this.state.newTopic, ticket: this.state.newTicket };
      TicketService.createTicket(ticket).then((response) => {
        this.refreshData();
      });
    }
  }

  render() {
    if (typeof window !== "undefined" && localStorage.getItem("logged in")) {
      return (
        <>
          <div className="bg-half bg-light d-table w-100">
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-lg-12 text-center">
                  <div className="page-next-level">
                    <h4 className="title"> Есть проблемы? Мы рады помочь! </h4>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="container pad-50-0-25 fontsize_15rem">
            <div className="row justify-content-center">
              <div className="col-lg-7 col-12">
                <div className="rounded p-4 shadow">
                  <div className="row">
                    <div className="col-12">
                      <div className="d-flex align-items-center pb-4">
                        <label className="font-weight-bold">Создать новый вопрос:</label>
                      </div>
                      <div className="row">
                        <div className="col-md-12">
                          <div className="mb-3">
                            <label className="form-label">Тема</label>
                            <div className="form-icon position-relative">
                              <i data-feather="book" className="fea icon-sm icons"></i>
                              <input
                                name="subject"
                                id="subject"
                                className="form-control ps-5 fontsize_15rem"
                                placeholder="Введите тему сюда"
                                value={this.state.newTopic}
                                onChange={this.handleNewTopicChange}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="mb-3">
                            <label className="form-label">Сообщение</label>
                            <div className="form-icon position-relative">
                              <i data-feather="message-circle" className="fea icon-sm icons"></i>
                              <textarea
                                name="comments"
                                id="comments"
                                rows="4"
                                className="form-control ps-5 fontsize_15rem"
                                placeholder="Введите ваше сообщение сюда"
                                value={this.state.newTicket}
                                onChange={this.handleNewTicketChange}
                              ></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-12">
                          <button className="btn btn-primary" onClick={() => this.addTicket()}>
                            Создать
                          </button>
                          {/* <input type="submit" id="submit" name="send" className="btn btn-primary" value="Send Request" /> */}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="container pad-0-0-50-0 fontsize_15rem">
            <div className="row justify-content-center">
              <div className="col-lg-7 col-12">
                <div className="rounded p-4 shadow">
                  <div className="row">
                    <div className="col-12">
                      <div className="d-flex align-items-center pb-4">
                        <label className="font-weight-bold">Предыдущие вопросы:</label>{" "}
                      </div>
                      {this.printTickets()}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      );
    } else {
      // @todo add redirect to register page
      this.callError("Пожалуйста, зарегистрируйтесь или напишите нам на E-mail: igor.vershyna@gmail.com");
      return <></>;
    }
  }
}
export default withAlert()(Tickets);
