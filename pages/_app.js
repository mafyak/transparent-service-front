import React from "react";
import Layout from "./components/Layout/Layout";
import "../assets/styles/index.scss";
import {transitions, positions, Provider as AlertProvider} from "react-alert";
import AlertTemplate from "react-alert-template-basic";
import Head from "next/head";

const alertOptions = {
  position: positions.BOTTOM_CENTER,
  timeout: 5000,
  offset: "30px",
  transition: transitions.SCALE,
};

const MyApp = ({Component, pageProps}) => {

  return (
    <>
      <Head>
        <link rel="icon" type="image/png" href="/img/default/favicon.png" />
        <meta httpEquiv='content-language' content='en-US'/>
      </Head>
      <AlertProvider template={AlertTemplate} {...alertOptions}>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </AlertProvider>
    </>
  );
}

export default MyApp;