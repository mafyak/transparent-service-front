import React, { useState, useEffect } from "react";
import InputAuto from "./components/Register/InputAuto";
import { Eye } from "react-feather";
import { UserService, FileService } from "@services";
import { withAlert } from "react-alert";
import Router from "next/router";

const MySettings = ({ alert }) => {
  const [userId, setUserId] = useState("");
  const [userLogin, setUserLogin] = useState("");
  const [userFullName, setUserFullName] = useState("");
  const [userPhone, setUserPhone] = useState("");
  const [userEmail, setUserEmail] = useState("");
  const [userCity, setUserCity] = useState("");
  const [userAvatar, setUserAvatar] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");
  const [imageError, setImageError] = useState("");
  const [selectedLogoFile, setSelectedLogoFile] = useState("");
  const [selectedLogoLoaded, setSelectedLogoLoaded] = useState("");
  const [imageIsLoading, setImageIsLoading] = useState(false);

  useEffect(() => {
    async function refreshUserInfo() {
      UserService.getUserInfo().then(({ data }) => {
        setUserId(data.id);
        setUserLogin(data.login);
        setUserFullName(data.fullName);
        setUserPhone(data.phone);
        setUserEmail(data.email);
        setUserCity(data.city);
        setUserAvatar(data.avatar);
        setPassword("");
        setPasswordConfirm("");
        setImageError("");
        setSelectedLogoFile("");
        setSelectedLogoLoaded(0);
      });
    }
    if (!userId) {
      refreshUserInfo();
    }
  }, []);

  function loginChange({ target }) {
    setUserLogin(target.value);
  }

  function passwordChange({ target }) {
    setPassword(target.value);
  }

  function passwordConfirmChange({ target }) {
    setPasswordConfirm(target.value);
  }

  function userPhoneChange({ target }) {
    setUserPhone(target.value);
  }

  function fullNameChange({ target }) {
    setUserFullName(target.value);
  }

  function locationChange(city) {
    let updCity = data.city;
    updCity.name = city.label;
    updCity.id = city.value;
    setUserCity(updCity);
  }

  function emailChange({ target }) {
    setUserEmail(target.value);
  }

  function callSuccessAlert(message) {
    alert.success(message);
  }

  function callInfoAlert(message) {
    alert.info(message);
  }

  function callFailureAlert(message) {
    alert.error(message);
  }

  function saveUser(e) {
    e.preventDefault();
    if (passwordConfirm == password || !password) {
      var encodedPass;
      var user = {};
      var isEmailValid = true;
      if (password) {
        const bcrypt = require("bcryptjs");
        const salt = bcrypt.genSaltSync(10);
        encodedPass = bcrypt.hashSync(password, salt);
        user.pass = encodedPass;
      }
      if (isEmailValid) {
        (user.id = userId),
          (user.login = userLogin),
          (user.fullName = userFullName),
          (user.phone = userPhone),
          (user.email = userEmail),
          (user.city = userCity),
          (user.avatar = userAvatar);
        UserService.updateUser(user).then((response) => {
          UserService.getUserInfo().then(
            (response) => {
              if (response && response.data && response.status == 200) {
                callSuccessAlert("Данные успешно обновлены");
                localStorage.setItem("fullName", response.data.fullName);
                setUserId(response.data.id);
                setUserLogin(response.data.login);
                setUserFullName(response.data.fullName);
                setUserPhone(response.data.phone);
                setUserEmail(response.data.email);
                setUserCity(response.data.city);
                setUserAvatar(response.data.avatar);
                setPassword("");
                setPasswordConfirm("");
              }
            },
            (error) => {
              callFailureAlert("Ошибка во время обновления данных");
            }
          );
        });
      } else {
        callFailureAlert(
          "Пароль должен содержать как минимум одну цифру, одну заглавную и одну строчную латинскую букву. Длина пароля: 8 или более символов"
        );
      }
    } else {
      callFailureAlert("Пароли должны совпадать");
    }
  }

  function saveImage(file) {
    const image = new FormData();
    image.append("image", file);

    if (file.size < 524288000) {
      FileService.saveImage(image).then((res) => {
        if (res && res.data && res.data.status == "OK") {
          const newLogo = res.data.data;
          setUserAvatar(newLogo);
          setImageIsLoading(false);
        } else {
          setImageError(true);
        }
      });
    } else {
      callFailureAlert("Размер файла должен быть не больше 500 мегабайт");
    }
  }

  function selectLogoFile({ target }) {
    setImageError(false);
    setSelectedLogoFile(target.files[0]);
    setSelectedLogoLoaded(0);
    setImageIsLoading(true);
    saveImage(target.files[0]);
  }

  function printCity(userCity) {
    if (userCity) {
      return userCity.name;
    } else return "";
  }

  if (typeof window !== "undefined" && localStorage.getItem("logged in")) {
    return (
      <div>
        <section className="bg-main bg-light d-table w-100 shadow-sm">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-12 text-center">
                <div className="page-next-level">
                  <h4 className="title  text-primary">Ваши настройки</h4>
                  <div className="p-2">
                    <p className="p-2 text-center text-muted">
                      Для сохранения изменений нажмите на кнопку <span className="text-primary">Сохранить </span> внизу формы
                    </p>
                    <a href={"/user/" + userId} target="_blank" className="btn btn-sm d-block row ">
                      <Eye className="fea icon-sm icons" /> <span className="pl-3">Посмотреть ваш профиль</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <div className="container">
          <div className="component-wrapper rounded shadow m-5 p-5">
            <form className="login-form mt-4" onSubmit={saveUser}>
              <div className="row">
                <div className="form-group row col-12 p-4">
                  <div className="p-4 col-12 col-md-4 text-center">
                    <img src={userAvatar} className="img-fluid avatar avatar-medium rounded-circle" />
                  </div>
                  <div className="col-12 col-md-4 d-flex flex-column justify-content-around">
                    <input
                      className="form-control-file mb-4"
                      name="userAvatar"
                      type="file"
                      onChange={selectLogoFile}
                      placeholder="Введите название"
                    />
                    {!imageIsLoading && !imageError ? <span className="text-white">.</span> : <span>Загрузка...</span>}
                    {imageError ? <span className="text-white">Не удалось загрузить файл</span> : <span className="text-white">.</span>}
                  </div>
                </div>

                <div className="form-group col-12">
                  <label>Логин</label>
                  <input type="name" defaultValue={userLogin} className="form-control" onChange={loginChange} />
                </div>

                <div className="form-group col-md-6 col-12">
                  <label>Полное имя</label>
                  <input type="name" defaultValue={userFullName} className="form-control" onChange={fullNameChange} />
                </div>

                <div className="form-group col-md-6 col-12">
                  <label>Телефон</label>
                  <input type="name" defaultValue={userPhone} className="form-control" onChange={userPhoneChange} />
                </div>

                <div className="form-group col-md-6 col-12">
                  <label>
                    Ваш город:<b className="text-primary"> {printCity(userCity)}</b>
                  </label>
                  <InputAuto cityChangeParentToChild={locationChange} />
                </div>

                <div className="form-group col-md-6 col-12">
                  <label>E-mail:</label>
                  <input type="name" defaultValue={userEmail} className="form-control" onChange={emailChange} />
                </div>

                <div className="form-group col-md-6 col-12">
                  <label>Пароль:</label>
                  <input
                    type="password"
                    placeholder="Пароль"
                    autoComplete="new-password"
                    value={password}
                    pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                    className="form-control"
                    title="Пароль должен содержать как минимум одну цифру, одну заглавную и одну строчную латинскую букву. Длина пароля: 8 или более символов"
                    onChange={passwordChange}
                  />
                </div>

                <div className="form-group col-md-6 col-12">
                  <label>Повторите пароль:</label>
                  <input
                    type="password"
                    placeholder="Повторите пароль"
                    autoComplete="new-password"
                    value={passwordConfirm}
                    pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                    className="form-control"
                    title="Пароль должен содержать как минимум одну цифру, одну заглавную и одну строчную латинскую букву. Длина пароля: 8 или более символов"
                    onChange={passwordConfirmChange}
                  />
                </div>
                <button className="btn btn-success mx-4 w-100">Сохранить</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  } else if (process.browser) {
    callInfoAlert("Пожалуйста авторизуйтесь");
    Router.push("/login");
    return (
      <section className="section border-top">
        <div className="container mt-100 mt-60">
          <div className="row justify-content-center"></div>
        </div>
      </section>
    );
  } else {
    return <></>;
  }
};

export default withAlert()(MySettings);
