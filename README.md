# Transparent Service

## Полезные знания

Use Node JS v13.12 to run

### Иерархия категорий работ:
* Топ категории (TopCategory) - стройка\красота\перевозки. На данном этапе игнорируем, работаем только со стройкой.
* Базовые категории (BaseCategory) - стены\потолок\пол. Используем только для простоты выбора нужной категории для заказчика.
* Работа (Job) - поклейка обоев\установка ламината\замена унитаза. Используем у мастеров для определения какие категории услуг они предоставляют.
* Задачи (Task) - непосредственно сами задачи которые выполняются при работе(job), например если работа это поклейка обоев, то задачи буду - срыв имеющихся обоев, подготовка стен, материал, поклейка новых обоев, уборка.


### Планы для исполнителей:
* нулевой: видно только в списке среди обычных
* первый: видно в топе в списке
* второй: видно в топе в списке и среди 5ти компаний
* третий: видно в топе в списке и среди 3х компаний

### Access:
* Jenkins:<br/>
http://81.91.190.37:8080/<br/>
Kto-go<br/>
89h78978G*&(&*Yi9u)<br/>

* DB:<br/>
  UI = Coming... not soon.<br/>
  name = tsDB<br/>
  url = localhost:5432<br/>
  username = postgres<br/>
  password = F*(HY#UU*@(#UFH<br/>
  
* DB commands:<br/>
  access bash from cl: su postgres<br/>
  access postgres from bash: psql -U postgres<br/>
  connect to DB: \c tsdb;<br/>
  show all tabes: \dt<br/>
  quit: \q<br/>
  
* Putty:<br/>
 217.61.126.27:22<br/>
 root<br/>
 80*Y(Dudjfh*Y(FYHJ#JFH*F#
 
* Tomcat:<br/>
 http://217.61.126.27:7070/<br/>
 tomcat<br/>
 s355!!!231!cret
 
* Image Holder:
    * POST TO: http://217.61.126.27:7070/image-holder/uploadImage
    * Body: form-data
        * userId : {your_user_id}
        * image : {file_itself_as_multipart_form_data}
    * Returns : URL to image.
  
* Project:<br/>
  Coming soon.